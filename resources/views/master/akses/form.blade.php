@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                    @csrf
                    <input name="id" id="id" class="form-control" type="hidden" value="{{ $data->id }}"
                        id="id">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong>{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-2 col-form-label-sm">Nama</label>
                                        <div class="col-sm-10">
                                            <input name="nama" id="nama" class="form-control form-control-sm"
                                                type="text" value="{{ $data->nama }}" placeholder="Nama"
                                                id="nama">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @foreach ($akses as $key => $value)
                                    <div class="col-sm-6">
                                        <div>
                                            <table class="table table-hover">
                                                <thead class="thead-theme">
                                                    <th width="5%"></th>
                                                    <th>{{ $value['nama'] }}</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($value['detail'] as $k => $v)
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox"
                                                                    name="{{ $key }}_{{ $k }}"
                                                                    id="{{ $key }}_{{ $k }}"
                                                                    {{ in_array($key . '_' . $k, $data->akses) ? 'checked' : '' }}>
                                                            </td>
                                                            <td>
                                                                <label for="{{ $key }}_{{ $k }}"
                                                                    class="w-100">{{ $v }}</label>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="col-sm-12">
                                    <div class="btn-group btn-group sm float-right">
                                        <a class="btn btn-sm btn-light float-right"
                                            href="{{ route($route . '.index') }}"><i class="fa fa-arrow-left"></i>
                                            Kembali</a>
                                        <button class="btn btn-sm btn-primary float-right" type="submit"><i
                                                class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
