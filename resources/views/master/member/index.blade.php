@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong class="card-ttl">{{ $title }}</strong>
                                <form method="get" action="{{ route($route.'.index') }}" enctype="multipart/form-data">
                                    <div class="row mt-3">
                                        <div class="col-sm-2">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control form-control-sm" name="name" id="name" placeholder="Cari berdasarkan nama" value="{{ !empty($filter->name) ? $filter->name : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control form-control-sm" name="no_hp" id="no_hp" placeholder="Cari berdasarkan No Telp/Hp" value="{{ !empty($filter->no_hp) ? $filter->no_hp : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control form-control-sm" name="no_ktp" id="no_ktp" placeholder="Cari berdasarkan No KTP" value="{{ !empty($filter->no_ktp) ? $filter->no_ktp : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control form-control-sm" name="alamat" id="alamat" placeholder="Cari berdasarkan Alamat" value="{{ !empty($filter->alamat) ? $filter->alamat : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="btn-group btn-group-sm">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="btn-group btn-group-sm float-right">
                                                <a class="btn btn-sm btn-primary" href="{{ route($route . '.create') }}"><i
                                                        class="fa fa-plus"></i> Tambah</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-theme">
                                        <th>Nama</th>
                                        <th>No Hp</th>
                                        <th>No KTP</th>
                                        <th>Alamat</th>
                                        <th>Tipe Member</th>
                                        <th>Kode Refferal</th>
                                        <th>Saldo Terambil</th>
                                        <th>Sisa Saldo</th>
                                        <th width="5%"></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key => $value)
                                            <tr>
                                                <td>{{ $value->name }}</td>
                                                <td>{{ $value->no_hp }}</td>
                                                <td>{{ $value->no_ktp }}</td>
                                                <td>{{ $value->alamat }}</td>
                                                <td>
                                                    @if($value->tipe_member == 1)
                                                       Member
                                                    @elseif($value->tipe_member == 2)
                                                        Member Referral
                                                    @endif
                                                </td>
                                                <td>{{ $value->kode_referral }}</td>
                                                <td>Rp {{ Str::currency($value->saldoterambil) }}</td>
                                                <td>Rp {{ Str::currency($value->saldotersedia) }}</td>
                                                <td>
                                                    <form action="{{ route($route . '.destroy', $value->id) }}"
                                                        method="POST">
                                                        <div class="btn-group btn-group-sm">
                                                            <a class="btn btn-sm btn-primary"
                                                                href="{{ route($route . '.edit', $value->id) }}"><i
                                                                    class="fa fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button  onclick="hapus(this)" class="btn btn-sm btn-danger"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div>
                                    @include('admin.body.pagination')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
