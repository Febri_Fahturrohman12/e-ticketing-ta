@extends('admin.admin_master')
@section('admin')
<style>
    .radio-toolbar input[type="radio"]:focus+label {
        border: 2px dashed #444;
    }

    .radio-toolbar input[type="radio"]:checked+label {
        background-color: #327278;
    }

    .radio-toolbar input[type="radio"] {
        display: none;
    }

    .selectImage {
        color: white;
        font-size: 16px;
    }
    .group-image {
        position: relative;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }
    .display-none {
        display: none;
    }


    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    .group-image :hover .image {
        opacity: 0.3;
    }

    .group-image :hover .middle {
        opacity: 1;
    }
</style>
<div class="page-content">
    <div class="container-fluid">
        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <b>Peringatan</b>
            @foreach ($errors->all() as $error)
            {{ $error }}
            @endforeach
            <button type="button" class="close text-light-gray" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="mdi mdi-close font-16"></i></span></button>
        </div>
        @endif
        <div class="row">
            <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                @csrf
                <input name="id" id="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong>{{ $title }}</strong>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <div class="form-group row ">
                                    <div class="col-sm-12 group-image">
                                        <div class="text-center">
                                            <img id="showImage" class="img-thumbnail rounded-square" alt="200x200" src="{{ (!empty($data->foto))? url($data->foto):url('upload/no_image.jpg') }}">
                                            <div class=" middle">
                                                <button type="button" class="selectImage btn btn-sm btn-primary" onClick="image.click()"> Pilih Foto</button>
                                            </div>
                                        </div>
                                    </div>
                                    <input name="foto_url" class="form-control" style="display: none" type="file" accept="image/png, image/gif, image/jpeg, image/jpg" placeholder="Foto" id="image">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Nama</label>
                                    <div class="col-sm-10">
                                        <input name="name" id="name" class="form-control form-control-sm" type="text" value="{{ $data->name }}" placeholder="Nama" id="name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">No Telp/Hp</label>
                                    <div class="col-sm-10">
                                        <input name="no_hp" id="no_hp" class="form-control form-control-sm" type="number" value="{{ $data->no_hp }}" placeholder="No Telp/Hp" id="no_hp">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">No KTP</label>
                                    <div class="col-sm-10">
                                        <input name="no_ktp" id="no_ktp" class="form-control form-control-sm" type="text" value="{{ $data->no_ktp }}" placeholder="No KTP" id="no_ktp">
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea name="alamat" rows="4" id="alamat" class="form-control form-control-sm" type="text" value="{{ $data->alamat }}" placeholder="Alamat" id="alamat">{{$data->alamat}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="type" class="col-sm-2 col-form-label-sm">Tipe Member</label>
                                    <div class="col-sm-10">
                                        <div class="radio-toolbar">
                                            <input type="radio" id="member" name="tipe_member" value="1" {{ ($data->tipe_member == 1 || $data->tipe_member == null) ? 'checked' : ''}}>
                                            <label onclick="setRefferal(false)" class="btn-info  btn-sm" for="member">
                                                Member
                                            </label>
                                            <input type="radio" id="member_referral" name="tipe_member" value="2" {{ ($data->tipe_member == 2) ? 'checked' : ''}}>
                                            <label  onclick="setRefferal(true)" class="btn-info  btn-sm" for="member_referral">
                                                Member Referral
                                            </label>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row  @if($data->tipe_member == 1 || $data->tipe_member == null) display-none @endif" id="referral">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Kode Referral</label>
                                    <div class="col-sm-4">
                                        <input name="kode_referral" id="kode_referral" class="form-control form-control-sm" type="text" value="{{ $data->kode_referral }}" placeholder="Kode Refferal" id="kode_referral">
                                    </div>
                                </div>
                                <div class="form-group row  @if($data->tipe_member == 1 || $data->tipe_member == null) display-none @endif" id="id_reward_referral">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Reward Referral</label>
                                    <div class="col-sm-4">
                                    <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" name="reward_referral" id="reward_referral " class="form-control form-control-sm" type="text" value="{{ $data->reward_referral  }}" placeholder="Reward Refferal">
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        / Orang
                                                    </span>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="btn-group btn-group sm float-right">
                                    <a class="btn btn-sm btn-light float-right" href="{{ route($route . '.index') }}"><i class="fa fa-arrow-left"></i>
                                        Kembali</a>
                                    <button class="btn btn-sm btn-primary float-right" type="submit"><i class="fa fa-check"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/inputmask@5.0.8/dist/jquery.inputmask.min.js"></script>

<script>
    $(document).ready(function() {
        $(":input").inputmask();
        $('#image').change(function(e) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#showImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });
        $("input[name=type]").change(function() {
            if ($("#operator").is(':checked')) {
                $("#destinasi_select").show();
            } else {
                $("#destinasi_select").hide();
            }
        });
    });
    function setRefferal(isRefferal) {
        if(isRefferal){
            document.getElementById("referral").style.display = "flex";
            document.getElementById("id_reward_referral").style.display = "flex";
        }else{
            document.getElementById("referral").style.display = "none";
            document.getElementById("id_reward_referral").style.display = "none";
        }
    }
</script>
@endsection