@extends('admin.admin_master')
@section('admin')
</style>
<div class="page-content">
    <div class="container-fluid">
        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <b>Peringatan</b>
            @foreach ($errors->all() as $error)
            {{ $error }}
            @endforeach
            <button type="button" class="close text-light-gray" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="mdi mdi-close font-16"></i></span></button>
        </div>
        @endif
        <div class="row">
            <form method="POST" action="{{ route($route . '.store')}}" enctype="multipart/form-data">
                @csrf
                <input name="id" id="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong>{{ $title }}</strong>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Penyetuju Setoran Harian</label>
                                    <div class="col-sm-6">
                                        <select name="m_user_approval_harian_id" id="m_user_approval_harian_id" class="form-control form-control-sm">
                                            <option value=""></option>
                                            @foreach ($listUser as $key => $value)
                                            <option value="{{ $value->id }}" {{ $data->m_user_approval_harian_id == $value->id ? 'selected' : '' }}>
                                                {{ $value->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Penyetuju Setoran Bulanan</label>
                                    <div class="col-sm-6">
                                        <select name="m_user_approval_bulanan_id" id="m_user_approval_bulanan_id" class="form-control form-control-sm">
                                            <option value=""></option>
                                            @foreach ($listUser as $key => $value)
                                            <option value="{{ $value->id }}" {{ $data->m_user_approval_bulanan_id == $value->id ? 'selected' : '' }}>
                                                {{ $value->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mt-3">
                                <div class="btn-group btn-group sm float-right">
                                    <button class="btn btn-sm btn-primary float-right" type="submit"><i class="fa fa-check"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection