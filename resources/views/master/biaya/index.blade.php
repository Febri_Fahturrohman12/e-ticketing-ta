@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong class="card-ttl">{{ $title }}</strong>
                            <div class="btn-group btn-group-sm float-right">
                                {{-- <button class="btn btn-sm btn-light"><i class="fa fa-search"></i> Pencarian</button> --}}
                                <a class="btn btn-sm btn-primary" href="{{ route($route . '.create') }}"><i
                                        class="fa fa-plus"></i> Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-theme">
                                        <th>Periode</th>
                                        <th>Biaya Air / m<sup>3</sup></th>
                                        <th>Biaya Sewa Alat</th>
                                        <th width="5%"></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key => $value)
                                            <tr>
                                                <td>
                                                    {{ $value->tanggal_formatted }}
                                                </td>
                                                <td>{{ $value->biaya_air }}</td>
                                                <td>{{ $value->biaya_alat }}</td>
                                                <td>
                                                    <form action="{{ route($route . '.destroy', $value->id) }}"
                                                        method="POST">
                                                        <div class="btn-group btn-group-sm">
                                                            <a class="btn btn-sm btn-primary"
                                                                href="{{ route($route . '.edit', $value->id) }}"><i
                                                                    class="fa fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="hapus(this)"><i class="fa fa-trash"></i></button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div>
                                    @include('admin.body.pagination')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
