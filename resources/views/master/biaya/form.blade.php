@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                    @csrf
                    <input name="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong>{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-2 col-form-label-sm">Periode
                                            Awal</label>
                                        <div class="col-sm-10">
                                            <input type="month" class="form-control form-control-sm" name="tanggal"
                                                value="{{ $data->tanggal }}" id="tanggal" placeholder="Tanggal">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Biaya Air</label>
                                        <div class="col-sm-9">
                                            <div class="input-group input-group-sm">
                                                <button class="btn btn-sm btn-light input-group-addon" type="button">Rp.
                                                </button>
                                                <input type="number" class="form-control form-control-sm number"
                                                    name="biaya_air" value="{{ $data->biaya_air }}" id="biaya_air"
                                                    placeholder="Biaya Air">
                                                <button class="btn btn-sm btn-light input-group-addon" type="button">/
                                                    m<sup>3</sup>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Biaya Sewa
                                            Alat</label>
                                        <div class="col-sm-9">
                                            <div class="input-group input-group-sm">
                                                <button class="btn btn-sm btn-light input-group-addon" type="button">Rp.
                                                </button>
                                                <input type="number" class="form-control form-control-sm number"
                                                    name="biaya_alat" value="{{ $data->biaya_alat }}" id="biaya_alat"
                                                    placeholder="Biaya Sewa Alat">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="btn-group btn-group sm float-right">
                                        <a class="btn btn-sm btn-light float-right" href="{{ route($route . '.index') }}"><i
                                                class="fa fa-arrow-left"></i> Kembali</a>
                                        <button class="btn btn-sm btn-primary float-right"><i
                                                class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
