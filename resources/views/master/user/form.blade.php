@extends('admin.admin_master')
@section('admin')
<style>
    .radio-toolbar input[type="radio"]:focus+label {
        border: 2px dashed #444;
    }

    .radio-toolbar input[type="radio"]:checked+label {
        background-color: #327278;
    }

    .radio-toolbar input[type="radio"] {
        display: none;
    }
</style>
<div class="page-content">
    <div class="container-fluid">
        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <b>Peringatan</b>
            @foreach ($errors->all() as $error)
            {{ $error }}
            @endforeach
            <button type="button" class="close text-light-gray" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="mdi mdi-close font-16"></i></span></button>
        </div>
        @endif
        <div class="row">
            <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                @csrf
                <input name="id" id="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong>{{ $title }}</strong>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Nama</label>
                                    <div class="col-sm-10">
                                        <input name="name" id="name" class="form-control form-control-sm" type="text" value="{{ $data->name }}" placeholder="Nama" id="name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Akses</label>
                                    <div class="col-sm-10">
                                        <select name="akses_id" id="akses_id" class="form-control form-control-sm">
                                            <option value=""></option>
                                            @foreach ($listAkses as $key => $value)
                                            <option value="{{ $value->id }}" {{ $data->akses_id == $value->id ? 'selected' : '' }}>
                                                {{ $value->nama }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="type" class="col-sm-2">Tipe </label>
                                    <div class="col-sm-10">
                                        <div class="radio-toolbar">
                                            <input type="radio" id="operator" name="type" value="operator" {{ ($data->type == 'operator' || $data->type == null) ? 'checked' : ''}}>
                                            <label class="btn-info  btn-sm" for="operator">
                                                Operator
                                            </label>
                                            <input type="radio" id="finance" name="type" value="finance" {{ ($data->type == 'finance') ? 'checked' : ''}}>
                                            <label class="btn-info  btn-sm" for="finance">
                                                Finance
                                            </label>
                                            <input type="radio" id="admin" name="type" value="admin" {{ ($data->type == 'admin') ? 'checked' : ''}}>
                                            <label class="btn-info  btn-sm" for="admin">
                                                Admin
                                            </label>
                                            

                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row" id="destinasi_select" {{ ($data->type == 'admin' || $data->type == 'finance') ? 'style=display:none' : ''}}>
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Destinasi</label>
                                    <div class="col-sm-10">
                                        <select name="m_destination_id" id="m_destination_id" class="form-control form-control-sm">
                                            <option value=""></option>
                                            @foreach ($listDestinasi as $key => $value)
                                            <option value="{{ $value->id }}" {{ $data->m_destination_id == $value->id ? 'selected' : '' }}>
                                                {{ $value->nama }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-sm-12">
                                <div class="btn-group btn-group sm float-right">
                                    <a class="btn btn-sm btn-light float-right" href="{{ route($route . '.index_master') }}"><i class="fa fa-arrow-left"></i>
                                        Kembali</a>
                                    <button class="btn btn-sm btn-primary float-right" type="submit"><i class="fa fa-check"></i> Simpan</button>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="card-header bg-white pt-0">
                        <strong>{{'Informasi Akun' }}</strong>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Username</label>
                                    <div class="col-sm-10">
                                        <input name="username" id="username" class="form-control form-control-sm" type="text" value="{{ $data->username }}" placeholder="Username">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-2 col-form-label-sm">Password <br> <span style="font-style: italic; font-size:9px">(Kosongi Jika tidak ada perubahan)</span></label>
                                    <div class="col-sm-10">
                                        <input name="password_new" id="password_new" class="form-control form-control-sm" type="password" placeholder="password" id="password">
                                        <input value="{{ $data->password }}" name="password" id="password" class="form-control form-control-sm" type="password" placeholder="password" id="password" style="display: none;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="btn-group btn-group sm float-right">
                                    <a class="btn btn-sm btn-light float-right" href="{{ route($route . '.index_master') }}"><i class="fa fa-arrow-left"></i>
                                        Kembali</a>
                                    <button class="btn btn-sm btn-primary float-right" type="submit"><i class="fa fa-check"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("input[name=type]").change(function() {
            if ($("#operator").is(':checked')) {
                $("#destinasi_select").show();
            } else {
                $("#destinasi_select").hide();
            }
        });
    });
</script>
@endsection