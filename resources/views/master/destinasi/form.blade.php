@extends('admin.admin_master')
@section('admin')

<div class="page-content">
    <div class="container-fluid">
        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <b>Peringatan</b>
            @foreach ($errors->all() as $error)
            {{ $error }}
            @endforeach
            <button type="button" class="close text-light-gray" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="mdi mdi-close font-16"></i></span></button>
        </div>
        @endif
        <div class="row">
            <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                @csrf
                <input name="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong class="card-ttl">{{ $title }}</strong>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Nama</label>
                                    <div class="col-sm-9">
                                        <input name="nama" class="form-control form-control-sm" type="text" value="{{ $data->nama }}" placeholder="Nama" id="nama">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Harga</label>
                                    <div class="col-sm-4">
                                        <div class='input-group input-group-sm'>
                                            <div class="input-group-append input-group-addon">
                                                <span class="input-group-text text-bold input-group-text-sm font-size-12 ">
                                                    Rp
                                                </span>
                                            </div>
                                            <input name="harga" class="form-control form-control-sm" type="number" value="{{ $data->harga }}" placeholder="Harga Tiket Masuk" id="harga">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Alamat</label>
                                    <div class="col-sm-9">
                                        <textarea name="alamat" class="form-control form-control-sm" id="alamat">{{ $data->alamat }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3 mb-2">
                                <strong class="card-ttl">Pembagian Komposisi Pembagian</strong>
                            </div>
                            <hr>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Porporasi</label>
                                    <div class="col-sm-3">
                                        <div class='input-group input-group-sm'>
                                            <input class="form-control-sm  form-control inputnumber" value="{{ $data->perforasi }}" type="number" name="perforasi">
                                            <div class="input-group-append input-group-addon">
                                                <span class="input-group-text text-bold input-group-text-sm font-size-12">
                                                    %
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Asuransi</label>
                                    <div class="col-sm-4">
                                        <div class='input-group input-group-sm'>
                                            <div class="input-group-append input-group-addon">
                                                <span class="input-group-text text-bold input-group-text-sm font-size-12">
                                                    Rp
                                                </span>
                                            </div>
                                            <input class="form-control-sm  form-control inputnumber" value="{{ $data->asuransi }}" type="number" name="asuransi" placeholder="Asuransi">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Biaya Cetak</label>
                                    <div class="col-sm-4">
                                        <div class='input-group input-group-sm'>
                                            <div class="input-group-append input-group-addon">
                                                <span class="input-group-text text-bold input-group-text-sm font-size-12">
                                                    Rp
                                                </span>
                                            </div>
                                            <input class="form-control-sm  form-control inputnumber" value="{{ $data->biaya_cetak }}" type="number" name="biaya_cetak" placeholder="Biaya Cetak">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label-sm" style="font-style: italic;"><span style="color: red">*</span> Harga tiket setelah dikurangi Porporasi, Asuransi, dan Biaya Cetak
                                        dibagi untuk : </label>

                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Perhutani</label>
                                    <div class="col-sm-3">
                                        <div class='input-group input-group-sm'>
                                            <input class="form-control-sm  form-control inputnumber" value="{{ $data->persen_profit_perhutani }}" type="number" name="persen_profit_perhutani">
                                            <div class="input-group-append input-group-addon">
                                                <span class="input-group-text text-bold input-group-text-sm font-size-12">
                                                    %
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">IDN</label>
                                    <div class="col-sm-3">
                                        <div class='input-group input-group-sm'>
                                            <input class="form-control-sm  form-control inputnumber" value="{{ $data->persen_profit_idn }}" type="number" name="persen_profit_idn">
                                            <div class="input-group-append input-group-addon">
                                                <span class="input-group-text text-bold input-group-text-sm font-size-12">
                                                    %
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3 mb-2">
                                <strong class="card-ttl">Jenis Kendaraan</strong>

                            </div>
                            <hr>
                            <div class="col-md-12">
                                <table class="table table-bordered" id="dynamicTable">
                                    <thead class="">
                                        <tr>
                                            <td class="ml-2 text-center" style="width: 40px;">
                                                <button type="button" name="add" id="add" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></button>
                                            </td>
                                            <td style="width: 65%">
                                                <b>Nama</b>
                                            </td>
                                            <td>
                                                <b>Harga</b>
                                            </td>
                                        </tr>
                                    </thead>
                                    @foreach($details as $key => $value)
                                    <tr>
                                        <td><button type="button" class="btn btn-sm btn-danger remove-tr"><i class="fa fa-trash"></i></button></td>
                                        <td>
                                            <input type="text" value="{{ $value['nama'] }}" name="details[{{ $value['id'] }}][nama]" placeholder="Ex : Mobil" class="form-control form-control-sm " />
                                            <input type="hidden" value="{{ $value['id'] }}" name="details[{{ $value['id'] }}][id]" class="form-control form-control-sm" />
                                        </td>
                                        <td>
                                            <input type="number" value="{{ $value['harga'] }}" name="details[{{ $value['id'] }}][harga]" placeholder="Harga" class="form-control form-control-sm" />
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="col-sm-12 mt-3">
                                <div class="btn-group btn-group sm float-right">
                                    <a class="btn btn-sm btn-light float-right" href="{{ route($route . '.index') }}"><i class="fa fa-arrow-left"></i> Kembali</a>
                                    <button class="btn btn-sm btn-primary float-right" type="submit"><i class="fa fa-check"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var i = $('#dynamicTable tr').length - 1;

    $("#add").click(function() {
        ++i;
        $("#dynamicTable").append('<tr><td><button type="button" id="remove' + i + '" name="remove' + i + '" class="btn btn-sm btn-danger remove-tr"><i class="fa fa-trash"></i></button></td><td><input type="text" name="details[' + i + '][nama]" placeholder="Nama" class="form-control form-control-sm" /></td><td><input type="number" name="details[' + i + '][harga]" placeholder="Harga" class="form-control form-control-sm" /></td></tr>');
    });


    $(document).on('click', '.remove-tr', function() {
        $(this).parents('tr').remove();
    });
</script>
@endsection