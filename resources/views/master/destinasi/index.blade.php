@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
       
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white card-ttl">
                            <strong class="card-ttl">{{ $title }}</strong>
                                <form method="get" action="{{ route($route.'.index') }}" enctype="multipart/form-data">
                                    <div class="row mt-3">
                                        <div class="col-sm-3">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control form-control-sm" name="nama" id="nama" placeholder="Cari berdasarkan nama" value="{{ !empty($filter->nama) ? $filter->nama : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="btn-group btn-group-sm">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="btn-group btn-group-sm float-right">
                                                <a class="btn btn-sm btn-primary" href="{{ route($route . '.create') }}"><i
                                                        class="fa fa-plus"></i> Tambah</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-theme">
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Alamat</th>
                                        <th width="5%"></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key => $value)
                                            <tr>
                                                <td>{{ $value->nama }}</td>
                                                <td>{{ $value->harga }}</td>
                                                <td>{{ $value->alamat }}</td>
                                                <td>
                                                    <form action="{{ route($route . '.destroy', $value->id) }}"
                                                        method="POST">
                                                        <div class="btn-group btn-group-sm">
                                                            <a class="btn btn-sm btn-info"
                                                                href="{{ route($route . '.edit', $value->id) }}"><i
                                                                    class="fa fa-edit"></i></a>
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-sm btn-danger" type="button" onclick="hapus(this)"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div>
                                    @include('admin.body.pagination')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
