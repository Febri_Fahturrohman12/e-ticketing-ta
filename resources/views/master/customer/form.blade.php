@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                    @csrf
                    <input name="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong class="card-ttl">{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Nama</label>
                                        <div class="col-sm-9">
                                            <input name="nama" class="form-control form-control-sm" type="text"
                                                value="{{ $data->nama }}" placeholder="Nama" id="nama">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Nomer PAM</label>
                                        <div class="col-sm-9">
                                            <input name="nomer" class="form-control form-control-sm" type="text"
                                                value="{{ $data->nomer }}" placeholder="Nomer PAM" id="nomer">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Telp</label>
                                        <div class="col-sm-9">
                                            <input name="telp" class="form-control form-control-sm" type="text"
                                                value="{{ $data->telp }}" placeholder="Telp" id="telp">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea name="alamat" class="form-control form-control-sm" id="alamat">{{ $data->alamat }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-3">
                                    <div class="btn-group btn-group sm float-right">
                                        <a class="btn btn-sm btn-light float-right" href="{{ route($route . '.index') }}"><i
                                                class="fa fa-arrow-left"></i> Kembali</a>
                                        <button class="btn btn-sm btn-primary float-right" type="submit"><i
                                                class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
