@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white card-ttl">
                            <strong class="card-ttl">{{ $title }}</strong>
                            <form method="get" action="{{ route($route.'.index') }}" enctype="multipart/form-data">
                                <div class="row mt-3">
                                    <div class="col-sm-3">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control form-control-sm" name="nama" id="nama" placeholder="Cari nama Pembuat" value="{{ !empty($filter->nama) ? $filter->nama : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control form-control-sm" name="aktivitas" id="aktivitas" placeholder="Cari Aktivitas" value="{{ !empty($filter->aktivitas) ? $filter->aktivitas : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="btn-group btn-group-sm">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-theme">
                                        <th>Nama Pembuat</th>
                                        <th>Ip Address</th>
                                        <th>Tanggal</th>
                                        <th>Aktivitas</th>
                                      
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key => $value)
                                            <tr>
                                                <td>{{ $value->users_name }}</td>
                                                <td>{{ $value->ip_address }}</td>
                                                <td>{{ $value->created_at }}</td>
                                                <td>{{ $value->aktivitas }}</td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div>
                                    @include('admin.body.pagination')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
