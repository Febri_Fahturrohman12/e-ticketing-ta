<!DOCTYPE html>
<html>

<head>
    <title>Laporan Ticketing</title>
</head>

<body>
    <style type="text/css">
        table {
            border-collapse: collapse;
            width: 100%;
        }

        h4,
        h5 {
            font-family: arial;
        }

        td,
        th {
            font-size: 15px;
            font-family: arial;
            padding: 2px
        }

        .table td,
        .table th {
            padding: 2px;
            font-size: 10px;
            border: 1px solid black;
        }

        .table thead {
            background: #006342;
            color: #fff
        }

        .sub-header {
            width: 40px;
        }
        .background-green {
            background-color: #006342f0;
            color: white;
        }
        @media print {
            @page {
                size: landscape
            }

            body {
                -webkit-print-color-adjust: exact !important;
            }
        }
        
    </style>
    <div >
        <table>
            <thead style="background: #fff; color: black;">
                <tr>
                    <th colspan="12" style="width: 100%;">
                        <div style="vertical-align: middle; text-align: center; font-weight:bold; ">
                            <h4 style="margin-bottom: 0px; margin-top: 0;">{{ $title }}  Periode {{ $periode }}</h4>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
        <div>
            <table class="table table-hover">
                <thead class="thead-theme table-bordered"  >
                    <tr>
                        <th class="align-middle background-green" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2"> NO</th>
                        <th class="align-middle" style="background-color: #06773d;color: white; vertical-align:center; min-width: 200px;" rowspan="2">NAMA WISATA </th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">KODE</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">PERIODE</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">TERJUAL</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">HARGA</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">HASIL BRUTO <br> (5 X 6)</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" colspan="3">KEWAJIBAN</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">
                            <div>
                                Pendapatan Setelah Dikurangi Kewajiban
                            </div>
                            <span> (7-8-9-10) </span>
                        </th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" colspan="2">PARA PIHAK</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">PPH <br> (12+13x4%)</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">PHT <br> (11X%)</th>
                        <th class="align-middle text-center" style="background-color: #06773d;color: white; vertical-align:center" rowspan="2">Setoran yang diterima KPH <br> (8+9+10+14+15)</th>
                    </tr>
                    <tr>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center" >ASURANSI <br> (5 X Rp)</td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center" >PORPORASI <br> (7-8x%)</td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center" >CETAK <br> (5 X Rp)</td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center" >LMDH <br> (5 X %)</td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center" >PENGELOLA <br> (11 X %)</td>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">1</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">2</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">3</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">4</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">5</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">6</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">7</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">8</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">9</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">10</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">11</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">12</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">13</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">14</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">15</td>
                        <td class="text-center text-super-bold" style="font-weight: bold; text-center">16</td>
                    </tr>
                    @foreach ($data as $key => $value)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $value->nama_destinasi }}</td>
                        <td>{{ $value->kode }}</td>
                        <td>{{ $value->tanggal_mulai }} - {{ $value->tanggal_selesai }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::number($value->total_pengunjung) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->harga_tiket) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_pendapatan) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_asuransi) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_perforasi) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_biaya_cetak) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_pendapatan_bersih) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_profit_perhutani) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_profit_idn) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_pph) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_pht) }}</td>
                        <td class="text-center" style="text-align: center;">{{ Str::currency($value->total_setoran_kph) }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot class="table-bordered">
                    <tr style="background: #006342;color: #fff;">
                        <td colspan="4" style="background-color: #06773d;color: white; vertical-align:center; font-weight: bold">Jumlah</td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::number($total['terjual'])}}
                        </td>
                        <td style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">

                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['bruto'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['asuransi'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['porporasi'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['cetak'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['pendapatan_bersih'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['lmdh'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['pengelola'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['pph'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['pht'])}}
                        </td>
                        <td class="text-center" style="background-color: #06773d;color: white; vertical-align:center; text-align: center;">
                            {{Str::currency($total['setoran_kph'])}}
                        </td>
                    </tr>
                </tfoot>
            </table>
            <table>
                <tr>
                    <td></td>
                    <td style="text-align: left" colspan="2">Mengetahui</td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: left" colspan="2">{{$penyetuju->type}}</td>
                </tr>
                <tr >
                    
                </tr>
                <tr >
                    
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold;text-align: left" colspan="2">{{$penyetuju->nama_penyetuju}}</td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>