@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white card-ttl">
                            <strong class="card-ttl">{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <form method="get" action="{{ route($route) }}" enctype="multipart/form-data">
                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <select name="destinasi_id" id="destinasi_id"
                                                    class="form-control form-control-sm">
                                                    <option value="">Semua Destinasi</option>
                                                    @foreach ($listDestinasi as $key => $value)
                                                        <option value="{{ $value->id }}"
                                                            {{ !empty($filter->destinasi_id) && $filter->destinasi_id == $value->id ? 'selected' : '' }}>
                                                            {{ $value->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input class="form-control-sm dateapicker form-control" value="{{ $filter->tanggal_mulai }}" type="text" name="tanggal_mulai" placeholder="Tanggal Mulai" id="pickerdate">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="px-1 mt-1" style="text-align: center; width: fit-content;">s/d</span>
                                    <div class="col-sm-2">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input class="form-control-sm dateapicker form-control" value="{{ $filter->tanggal_selesai }}" type="text" name="tanggal_selesai" placeholder="Tanggal Selesai" id="pickerdate">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 text-left">
                                        <div class="">
                                            <button class="btn  btn-sm btn-primary" type="submit"><i class="fa fa-search"></i>
                                                Tampilkan</button>
                                            <a class="btn btn-sm btn-info mr-3" href="{{route('generate-laporan',['download'=>'excel','destinasi_id' => $filter->destinasi_id, 'tanggal_mulai' => $filter->tanggal_mulai, 'tanggal_selesai' => $filter->tanggal_selesai])}}"> <i class="fas fa-file-excel mr-1"></i><span class="ml-2"> Export Excel</span></a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="col-md-12">
                                <div class="div-b-table">
                                    <table class="table table-hover">
                                        <thead class="thead-theme table-bordered">
                                            <tr>
                                                <th class="align-middle" rowspan="2"> NO</th>
                                                <th class="align-middle" style="min-width: 200px;" rowspan="2">NAMA WISATA </th>
                                                <th class="align-middle text-center" rowspan="2">KODE</th>
                                                <th class="align-middle text-center" rowspan="2">PERIODE</th>
                                                <th class="align-middle text-center" rowspan="2">TERJUAL</th>
                                                <th class="align-middle text-center" rowspan="2">HARGA</th>
                                                <th class="align-middle text-center" rowspan="2">HASIL BRUTO  <br> (5 X 6)</th>
                                                <th class="align-middle text-center" colspan="3">KEWAJIBAN</th>
                                                <th class="align-middle text-center" rowspan="2">
                                                    <div>
                                                        Pendapatan Setelah Dikurangi Kewajiban
                                                    </div>
                                                    <span > (7-8-9-10) </span>
                                                </th>
                                                <th class="align-middle text-center" colspan="2">PARA PIHAK</th>
                                                <th class="align-middle text-center" rowspan="2">PPH <br> (12+13x4%)</th>
                                                <th class="align-middle text-center" rowspan="2">PHT <br> (11X%)</th>
                                                <th class="align-middle text-center" rowspan="2">Setoran yang diterima KPH <br> (8+9+10+14+15)</th>
                                            </tr>
                                            <tr>
                                                <td class="text-center">ASURANSI  <br> (5 X Rp)</td>
                                                <td class="text-center">PORPORASI  <br> (7-8x%)</td>
                                                <td class="text-center">CETAK <br> (5 X Rp)</td>
                                                <td class="text-center">LMDH <br>  (5 X %)</td>
                                                <td class="text-center">PENGELOLA <br>  (11 X %)</td>
                                            </tr>
                                        </thead>
                                       
                                        <!-- <thead class="thead-theme">
                                            <td>ASURANSI  <br> (4 X Rp)</td>
                                            <td>PORPORASI  <br> (6-7x%)</td>
                                            <td>CETAK <br> (4 X Rp)</td>
                                            <td>LMDH <br>  (10 X %)</td>
                                            <td>PENGELOLA <br>  (10 X %)</td>
                                        </thead> -->
                                        <tbody>
                                        <tr>
                                                <td class="text-center text-super-bold">1</td>
                                                <td class="text-center text-super-bold">2</td>
                                                <td class="text-center text-super-bold">3</td>
                                                <td class="text-center text-super-bold">4</td>
                                                <td class="text-center text-super-bold">5</td>
                                                <td class="text-center text-super-bold">6</td>
                                                <td class="text-center text-super-bold">7</td>
                                                <td class="text-center text-super-bold">8</td>
                                                <td class="text-center text-super-bold">9</td>
                                                <td class="text-center text-super-bold">10</td>
                                                <td class="text-center text-super-bold">11</td>
                                                <td class="text-center text-super-bold">12</td>
                                                <td class="text-center text-super-bold">13</td>
                                                <td class="text-center text-super-bold">14</td>
                                                <td class="text-center text-super-bold">15</td>
                                                <td class="text-center text-super-bold">16</td>
                                        </tr>
                                            @foreach ($data as $key => $value)
                                                <tr>
                                                    <td class="text-center">{{ $loop->iteration }}</td>
                                                    <td>{{ $value->nama_destinasi }}</td>
                                                    <td>{{ $value->kode }}</td>
                                                    <td>{{ $value->tanggal_mulai }} - {{ $value->tanggal_selesai }}</td>
                                                    <td class="text-center">{{ Str::number($value->total_pengunjung) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->harga_tiket) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_pendapatan) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_asuransi) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_perforasi) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_biaya_cetak) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_pendapatan_bersih) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_profit_perhutani) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_profit_idn) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_pph) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_pht) }}</td>
                                                    <td class="text-center">{{ Str::currency($value->total_setoran_kph) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot class="table-bordered">
                                            <tr style="background: #006342;color: #fff;">
                                                <td colspan="4" style="font-weight: bold">Jumlah</td>
                                                <td class="text-center" >
                                                    {{Str::number($total['terjual'])}}
                                                </td>
                                                <td>

                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['bruto'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['asuransi'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['porporasi'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['cetak'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['pendapatan_bersih'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['lmdh'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['pengelola'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['pph'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['pht'])}}
                                                </td>
                                                <td class="text-center" >
                                                    {{Str::currency($total['setoran_kph'])}}
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
