<!DOCTYPE html>
<html>

<head>
    <title>Laporan Ticketing</title>
</head>

<body>
    <style type="text/css">
    table {
        border-collapse: collapse;
        width: 100%;
    }

    h4,
    h5 {
        font-family: arial;
    }

    td,
    th {
        font-size: 15px;
        font-family: arial;
        padding: 2px
    }

    .table td,
    .table th {
        padding: 2px;
        font-size: 10px;
        border: 1px solid black;
    }

    .table thead {
        background: #006342;
        color: #fff
    }

    .sub-header {
        width: 40px;
    }

    @media print {
        @page {
            size: landscape
        }

        body {
            -webkit-print-color-adjust: exact !important;
        }
    }
    </style>
    <div align="left">
        <table>
            <thead style="background: #fff; color: black;">
                <tr>
                  <th colspan="5" style="width: 100%;">
                    <div style="vertical-align: middle; text-align: center; font-weight: bold;">
                        <h4 style="margin-bottom: 0px; margin-top: 0;">{{ $title }}</h5>
                        <h5 style="margin-top: 0px;">Periode {{ $periode }}</h5>
                    </div>
                  </th>
                </tr>
            </thead>
        </table>
        <div>
          <table class="table">
            <thead>
                <tr style="background: #006342;color: #fff;">
                  <th>Kode</th>
                  <th>Tanggal</th>
                  <th>Nama Destinasi</th>
                  <th width="20">Jumlah Pengunjung</th>
                  <th style="text-align: right">Total Harga</th>
                </tr>
            </thead>
            <!-- UNTUK PRINT -->
            <tbody>
              @foreach ($data as $key => $value)
              <tr>
                  <td>{{ $value->kode }}</td>
                  <td>{{ $value->tanggal_formatted }}</td>
                  <td>{{ $value->nama_destinasi }}</td>
                  <td style="text-align: center">{{ $value->jumlah_pengunjung }}</td>
                  <td style="text-align: right">Rp. {{ Str::currency($value->total_harga) }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr style="background: #006342;color: #fff;">
                <td colspan="3" style="font-weight: bold">Total</td>
                <td style="text-align: center;font-weight: bold">{{$totalpengunjung}}</td>
                <td style="text-align: right;font-weight: bold">Rp. {{ Str::currency($totalharga) }}</td>
              </tr>
            </tfoot>
        </table>
        </div>
    </div>
</body>

</html>