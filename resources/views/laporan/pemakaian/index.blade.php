@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white card-ttl">
                            <strong class="card-ttl">{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <form method="get" action="{{ route($route) }}" enctype="multipart/form-data">
                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <select name="destinasi_id" id="destinasi_id"
                                                    class="form-control form-control-sm">
                                                    <option value="">Semua Destinasi</option>
                                                    @foreach ($listDestinasi as $key => $value)
                                                        <option value="{{ $value->id }}"
                                                            {{ !empty($filter->destinasi_id) && $filter->destinasi_id == $value->id ? 'selected' : '' }}>
                                                            {{ $value->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input class="form-control-sm dateapicker form-control" value="{{ $filter->tanggal_mulai }}" type="text" name="tanggal_mulai" placeholder="Tanggal Mulai" id="pickerdate">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="col-sm-1 mt-1" style="text-align: center;"> s/d</span>
                                    <div class="col-sm-2">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input class="form-control-sm dateapicker form-control" value="{{ $filter->tanggal_selesai }}" type="text" name="tanggal_selesai" placeholder="Tanggal Selesai" id="pickerdate">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 text-left">
                                        <div class="btn-group btn-group-sm">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i>
                                                Tampilkan</button>
                                            <a class="btn btn-primary" href="{{route('generate-laporan',['download'=>'pdf','destinasi_id' => $filter->destinasi_id, 'tanggal_mulai' => $filter->tanggal_mulai, 'tanggal_selesai' => $filter->tanggal_selesai])}}">Download PDF</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-theme">
                                        <th>Kode</th>
                                        <th>Tanggal</th>
                                        <th>Nama Destinasi</th>
                                        <th style="text-align: center">Jumlah Pengunjung</th>
                                        <th class="text-right" style="text-align: right">Total Harga</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key => $value)
                                            <tr>
                                                <td>{{ $value->kode }}</td>
                                                <td>{{ $value->tanggal_formatted }}</td>
                                                <td>{{ $value->nama_destinasi }}</td>
                                                <td style="text-align: center">{{ $value->jumlah_pengunjung }}</td>
                                                <td class="text-right" style="text-align: right">Rp. {{ Str::currency($value->total_harga) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr style="background: #006342;color: #fff;">
                                          <td colspan="3" style="font-weight: bold">Total</td>
                                          <td style="text-align: center;font-weight: bold">{{$totalpengunjung}}</td>
                                          <td style="text-align: right;font-weight: bold">Rp. {{ Str::currency($totalharga) }}</td>
                                        </tr>
                                      </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
