<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Demo Two Page</title>
</head>
<body>
    <h1>This is a Demo Page Two from Controller</h1>
    <a href="{{ url('/demo') }}">Go to Demo</a>
</body>
</html>