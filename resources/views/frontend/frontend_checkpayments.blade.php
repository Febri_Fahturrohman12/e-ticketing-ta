@extends('frontend.frontend_master')
@section('frontend')
    <main id="main" style="margin-top: 75px;">
        <section id="about" class="about">
            <div class="container">

                <div class="row content">
                    <div class="col-9">
                        <div class="footer-newsletter">
                            <h4 style="font-weight: bold;">Bayar Tagihan Anda Sekarang</h4>
                            <p style="font-size: 14px;">Untuk melihat tagihan meter air, Pelanggan dapat mengisi form cek
                                tagihan meteran di bawah ini dengan mengisi nomor meteran dibawah ini</p>
                            <hr>
                        </div>
                        <div class="col-8 col-offset-4" style="margin-top: 50px;">
                            <div class="footer-newsletter">
                                <h6 style="font-weight: bold">Cek Nomor Meter</h6>
                                <p style="font-size: 12px;">Silahkan cek nomor meter anda</p>
                            </div>
                            <form action="{{ route('cektagihan') }}" method="get"
                                style="margin-top: 15px;background: #fff;padding: 6px 10px;position: relative;border-radius: 50px;text-align: left;border: 1px solid #b9b9fa;">
                                <input type="text" name="nomer" value="{{ $nomer }}"
                                    style="border: 0;padding: 4px 8px;width: calc(100% - 100px);">
                                <input type="submit" value="Cek Tagihan"
                                    style="position: absolute;top: -1px;right: -1px;bottom: -1px;border: 0;background: none;font-size: 16px;padding: 0 20px;background: #5a5af3;color: #fff;transition: 0.3s;border-radius: 50px;box-shadow: 0px 2px 15px rgb(0 0 0 / 10%);">
                            </form>
                        </div>
                        @if (!empty($nomer) && count($listPemakaian) > 0)
                            <div class="col-8 col-offset-4" style="margin-top: 40px;">
                                <div class="footer-newsletter">
                                    <h6 style="font-weight: bold">Pilih Metode Pembayaran</h6>
                                    <p style="font-size: 12px;">Pilih salah satu dari metode pembayaran yang tersedia, kami
                                        merekomendasikan melalui Transfer Virtual Account untuk kemudahan transaksi.</p>
                                </div>
                                <div style="margin-top: 20px;">
                                    <select class="form-select"
                                        style="margin-top: 15px;background: #fff;padding: 6px 10px;position: relative;border-radius: 50px;text-align: left;border: 1px solid #b9b9fa;" onchange="setbank(this)">
                                        <option value="">Pilih metode bayar</option>
                                        <option value="BCA">Bank BCA</option>
                                        <option value="Mandiri">Bank Mandiri</option>
                                        <option value="BRI">Bank BRI</option>
                                        <option value="BNI">Bank BNI</option>
                                        <option value="Gopay">Gopay</option>
                                        <option value="Ovo">Ovo</option>
                                        <option value="Link Aja">Link Aja</option>
                                        <option value="Dana">Dana</option>
                                        <option value="Shopee Pay">ShopeePay</option>
                                        <option value="Indomart">Indomart</option>
                                        <option value="Alfamart">Alfamart</option>
                                        <option value="Visa">Visa</option>
                                        <option value="Master Card">Master Card</option>
                                        <option value="Pay Pal">PayPal</option>
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>
                    @if (!empty($nomer) && count($listPemakaian) > 0)
                        <div class="col-3">
                            <div class="col-12"
                                style="background-color: #fff;border-radius: 1rem;box-shadow: 0 6px 20px rgb(17 26 104 / 10%);display: inline-block;margin-bottom: 3rem;padding: 32px 18px 24px;font-family: 'Poppins', sans-serif">
                                <div class="row">
                                    <div class="col-12">
                                        <p style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 14px;margin-bottom: 0px;">
                                            Detail Tagihan</p>
                                            <span style="font-size: 13px;">
                                                {{$listPemakaian[0]->nama_customer}} - {{$nomer}}
                                            </span>
                                        <hr style="margin-top: 10px;">
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            @foreach ($listPemakaian as $key => $value)
                                                <div class="col-8">
                                                    <p
                                                        style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                        {{ $value->tanggal_formatted }}</p>
                                                </div>
                                                <div class="col-4" style="text-align: right;">
                                                    <p
                                                        style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                        Rp {{ $value->total }}</p>
                                                </div>
                                            @endforeach
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-8">
                                                <p
                                                    style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 12px;margin-bottom: 0px;">
                                                    Metode Pembayaran</p>
                                            </div>
                                            <div class="col-4" style="text-align: right;">
                                                <p id="namabayar"
                                                    style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 12px;margin-bottom: 0px;">
                                                    </p>
                                            </div>
                                        </div>
                                        <hr style="margin-top: 10px;">
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <p
                                                    style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 13px;margin-bottom: 0px;">
                                                    Total</p>
                                            </div>
                                            <div class="col-6" style="text-align: right;">
                                                <p
                                                    style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 13px;margin-bottom: 0px;">
                                                    Rp {{ $total }}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-12 mt-3" style="text-align: center;position: relative;bottom:0;">
                                    <form action="{{ route('bayartagihan', $nomer) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn-get-started scrollto"
                                            style="font-family: 'Poppins', sans-serif;font-weight: 400;font-size: 12px;letter-spacing: 0.5px;display: inline-block;padding: 8px 30px 9px 30px;margin-bottom: 12px;border-radius: 3px;transition: 0.5s;border-radius: 50px;background: #5a5af3;color: #fff;border: 2px solid #5a5af3;margin-right: 10px;">Bayar
                                            Sekarang</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-12">
                                <p style="font-size: 15px;">Transaksi makin mudah dengan metode pembayaran terlengkap!</p>
                                <div class="summary-cart mt-3">
                                    <div class="d-flex mb-3">
                                        <img src="{{ asset('frontend/assets/img/payment/bca.svg') }}" alt="bca"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/mandiri.svg') }}" alt="mandiri"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/bri.svg') }}" alt="bri"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/bni.svg') }}" alt="bni"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/qris.svg') }}" alt="qris"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/gopay.svg') }}" alt="gopay"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/ovo.svg') }}" alt="ovo"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/linkaja.svg') }}" alt="linkaja"
                                            width="36" height="16" class="lazyload m-auto">
                                    </div>
                                    <div class="d-flex">
                                        <img src="{{ asset('frontend/assets/img/payment/dana.svg') }}" alt="dana"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/shopeepay.svg') }}"
                                            alt="shopeepay" width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/indomaret.svg') }}"
                                            alt="indomaret" width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/alfamart.svg') }}" alt="alfamart"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/visa.svg') }}" alt="visa"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/master-card.svg') }}"
                                            alt="master-card" width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/jcb.svg') }}" alt="jcb"
                                            width="36" height="16" class="lazyload m-auto">
                                        <img src="{{ asset('frontend/assets/img/payment/paypal.svg') }}" alt="paypal"
                                            width="36" height="16" class="lazyload m-auto">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </section>
        <!-- ======= Services Section ======= -->
        <section id="services" class="services" style="background: #fff;">
            <div class="container">

                <div class="section-title">
                    <h2>Sistem Layanan</h2>
                    <p>Our Tirta merupakan media untuk membantu anda dalam melunasi tagihan air. Dengan sistem yang sangat
                        mudah dioperasikan dan tentunya mudah dimengerti sehingga dapat membantu anda dalam melakukan
                        pembayaran tagihan secara cepat dan tepat.</p>
                </div>

                <div class="row">
                    <div class="content col-xl-5 d-flex flex-column justify-content-center">
                        <img src="{{ asset('frontend/assets/img/hero-img.png') }}" class="img-fluid" alt="">
                    </div>
                    <div class="col-xl-7">
                        <div class="icon-boxes d-flex flex-column justify-content-center">
                            <div class="row">
                                <div class="col-lg-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                                    <div class="icon-box iconbox-blue">
                                        <div class="icon">
                                            <svg width="100" height="100" viewBox="0 0 600 600"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path stroke="none" stroke-width="0" fill="#f5f5f5"
                                                    d="M300,521.0016835830174C376.1290562159157,517.8887921683347,466.0731472004068,529.7835943286574,510.70327084640275,468.03025145048787C554.3714126377745,407.6079735673963,508.03601936045806,328.9844924480964,491.2728898941984,256.3432110539036C474.5976632858925,184.082847569629,479.9380746630129,96.60480741107993,416.23090153303,58.64404602377083C348.86323505073057,18.502131276798302,261.93793281208167,40.57373210992963,193.5410806939664,78.93577620505333C130.42746243093433,114.334589627462,98.30271207620316,179.96522072025542,76.75703585869454,249.04625023123273C51.97151888228291,328.5150500222984,13.704378332031375,421.85034740162234,66.52175969318436,486.19268352777647C119.04800174914682,550.1803526380478,217.28368757567262,524.383925680826,300,521.0016835830174">
                                                </path>
                                            </svg>
                                            <i class="bx bxl-dribbble"></i>
                                        </div>
                                        <h4><a>Mudah</a></h4>
                                        <p>Mempermudah anda untuk melunasi tagihan air yang belum terbayar</p>
                                    </div>
                                </div>

                                <div class="col-lg-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in"
                                    data-aos-delay="200">
                                    <div class="icon-box iconbox-orange ">
                                        <div class="icon">
                                            <svg width="100" height="100" viewBox="0 0 600 600"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path stroke="none" stroke-width="0" fill="#f5f5f5"
                                                    d="M300,582.0697525312426C382.5290701553225,586.8405444964366,449.9789794690241,525.3245884688669,502.5850820975895,461.55621195738473C556.606425686781,396.0723002908107,615.8543463187945,314.28637112970534,586.6730223649479,234.56875336149918C558.9533121215079,158.8439757836574,454.9685369536778,164.00468322053177,381.49747125262974,130.76875717737553C312.15926192815925,99.40240125094834,248.97055460311594,18.661163978235184,179.8680185752513,50.54337015887873C110.5421016452524,82.52863877960104,119.82277516462835,180.83849132639028,109.12597500060166,256.43424936330496C100.08760227029461,320.3096726198365,92.17705696193138,384.0621239912766,124.79988738764834,439.7174275375508C164.83382741302287,508.01625554203684,220.96474134820875,577.5009287672846,300,582.0697525312426">
                                                </path>
                                            </svg>
                                            <i class="bx bx-file"></i>
                                        </div>
                                        <h4><a>Akurat</a></h4>
                                        <p>Data tagihan yang diperoleh sangat akurat dan perhitungan sesuai dengan pemakaian
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-6 d-flex align-items-stretch mt-4" data-aos="zoom-in"
                                    data-aos-delay="300">
                                    <div class="icon-box iconbox-pink">
                                        <div class="icon">
                                            <svg width="100" height="100" viewBox="0 0 600 600"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path stroke="none" stroke-width="0" fill="#f5f5f5"
                                                    d="M300,541.5067337569781C382.14930387511276,545.0595476570109,479.8736841581634,548.3450877840088,526.4010558755058,480.5488172755941C571.5218469581645,414.80211281144784,517.5187510058486,332.0715597781072,496.52539010469104,255.14436215662573C477.37192572678356,184.95920475031193,473.57363656557914,105.61284051026155,413.0603344069578,65.22779650032875C343.27470386102294,18.654635553484475,251.2091493199835,5.337323636656869,175.0934190732945,40.62881213300186C97.87086631185822,76.43348514350839,51.98124368387456,156.15599469081315,36.44837278890362,239.84606092416172C21.716077023791087,319.22268207091537,43.775223500013084,401.1760424656574,96.891909868211,461.97329694683043C147.22146801428983,519.5804099606455,223.5754009179313,538.201503339737,300,541.5067337569781">
                                                </path>
                                            </svg>
                                            <i class="bx bx-tachometer"></i>
                                        </div>
                                        <h4><a>Cepat</a></h4>
                                        <p>Dapat mengestimasi waktu anda dan melakukan pembayaran tanpa harus ribet</p>
                                    </div>
                                </div>

                                <div class="col-lg-6 d-flex align-items-stretch mt-4" data-aos="zoom-in"
                                    data-aos-delay="100">
                                    <div class="icon-box iconbox-teal">
                                        <div class="icon">
                                            <svg width="100" height="100" viewBox="0 0 600 600"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path stroke="none" stroke-width="0" fill="#f5f5f5"
                                                    d="M300,503.46388370962813C374.79870501325706,506.71871716319447,464.8034551963731,527.1746412648533,510.4981551193396,467.86667711651364C555.9287308511215,408.9015244558933,512.6030010748507,327.5744911775523,490.211057578863,256.5855673507754C471.097692560561,195.9906835881958,447.69079081568157,138.11976852964426,395.19560036434837,102.3242989838813C329.3053358748298,57.3949838291264,248.02791733380457,8.279543830951368,175.87071277845988,42.242879143198664C103.41431057327972,76.34704239035025,93.79494320519305,170.9812938413882,81.28167332365135,250.07896920659033C70.17666984294237,320.27484674793965,64.84698225790005,396.69656628748305,111.28512138212992,450.4950937839243C156.20124167950087,502.5303643271138,231.32542653798444,500.4755392045468,300,503.46388370962813">
                                                </path>
                                            </svg>
                                            <i class="bx bx-layer"></i>
                                        </div>
                                        <h4><a>Tersusun</a></h4>
                                        <p>Tagihan yang anda peroleh tersusun rapi oleh sistem</p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End .content-->
                    </div>
                </div>

            </div>
        </section><!-- End Services Section -->
    </main>
@endsection
<script>
    function setbank(params) {
        var a = params.value;
        document.getElementById("namabayar").innerHTML = a;
    }
</script>