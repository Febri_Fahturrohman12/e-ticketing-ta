@extends('frontend.frontend_master')
@section('frontend')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 pt-2 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
          <p style="margin-bottom: 0px;font-size: 26px;color: #434175;font-family: "Poppins", sans-serif">Gak Jadi Mandi Karena Air Mati?</p>
          <h1 style="font-weight: 600;">Bayar PDAM di Our Tirta.</h1>
          <div class="mt-3">
            <a href="{{ route('cektagihan')}}" class="btn-get-started scrollto">Cek Tagihan</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img">
          <img src="{{ asset('frontend/assets/img/services.png') }}" class="img-fluid" alt="">
        </div>
      </div>
    </div>

</section><!-- End Hero -->
<main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-6">
            <h2>Our Tirta Solusi Bayar Air Gak Pake Lama</h2>
            <h3>Bayar air sekarang gak pake lama, Tanpa harus datang ke gerai ataupun kantor pembayaran</h3>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <p>
              Our Tirta menyediakan layanan tentunya untuk memudahkan anda dalam melakukan pembayaran tagihan air. Hanya perlu 3 langkah tagihan anda langsung terbayar. Our Tirta pastinya :
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Mudah</li>
              <li><i class="ri-check-double-line"></i> Cepat</li>
              <li><i class="ri-check-double-line"></i> Perhitungan Akurat</li>
            </ul>
            <p class="fst-italic">
              Ayo segera cek tagihan air anda. Gak Jadi Mandi Karena Air Mati? Bayar PDAM di Our Tirta Solusinya.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">

          <div class="col-lg-4">
            <div class="box">
              <span>01</span>
              <h4>Cek Tagihan</h4>
              <p>Tekan tombol cek tagihan untuk memulai pembayaran air</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box">
              <span>02</span>
              <h4>Masukkan Nomor Meter</h4>
              <p>Masukkan nomor meteran air anda pada isian yang telah disediakan dan klik tombol cari</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box">
              <span>03</span>
              <h4> Bayar Sekarang</h4>
              <p>Klik tombol bayar sekarang untuk menyelesaikan pembayaran anda</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Why Us Section -->

  </main><!-- End #main -->
@endsection