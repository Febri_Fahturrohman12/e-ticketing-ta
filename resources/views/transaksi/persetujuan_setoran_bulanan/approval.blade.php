@extends('admin.admin_master')
@section('admin')
<style>
    .radio-toolbar input[type="radio"]:focus+label {
        border: 2px dashed #444;
    }

    .radio-toolbar input[type="radio"]:checked+label {
        background-color: #327278;
    }

    .radio-toolbar input[type="radio"] {
        display: none;
    }

    .selectImage {
        color: white;
        font-size: 16px;
    }
    .group-image {
        position: relative;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }


    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    .group-image :hover .image {
        opacity: 0.3;
    }

    .group-image :hover .middle {
        opacity: 1;
    }
</style>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <form method="POST" action="{{ route($route . '.approve_pengajuan', $data->id) }}" enctype="multipart/form-data">
                @csrf
                <input name="id" id="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                <input name="m_user_approval_id" id="m_user_approval_id" class="form-control" type="hidden" value="{{ $approval->m_user_approval_id }}" id="m_user_approval_id">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong>{{ $title }}</strong>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                @if($data->id != null)
                                <div class="form-group row" style="display: none">
                                    <label for="example-text-input" class="col-sm-4 col-form-label-sm">Kode</label>
                                    <div class="col-sm-5">
                                        <input name="kode" id="kode" class="form-control form-control-sm" type="text" value="{{ $data->kode }}" placeholder="Kode" id="Kode">
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">Destinasi</label>
                                    <div class="col-lg-8 col-sm-9">
                                        <select disabled name="m_destination_id" id="destinasi" class="form-control form-control-sm" onchange="kalkulasiSetoran()">
                                            <option value="">Pilih Destinasi</option>
                                            @foreach ($listDestinasi as $key => $value)
                                            <option value="{{ $value->id }}" {{$data->m_destination_id == $value->id ? 'selected' : ''}}>{{ $value->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-4 col-form-label-sm">Tanggal</label>
                                    <div class="col-sm-4">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input disabled id="tanggal_mulai" class="form-control-sm dateapicker form-control" onchange="kalkulasiSetoran()" value="{{ $data->tanggal_mulai }}" type="text" name="tanggal_mulai" placeholder="Tanggal Mulai">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input disabled id="tanggal_selesai" class="form-control-sm dateapicker form-control" onchange="kalkulasiSetoran()" value="{{ $data->tanggal_selesai }}" type="text" name="tanggal_selesai" placeholder="Tanggal Selesai">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mt-1">
                                    <label for="example-text-input" class="col-sm-4 col-form-label-sm">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea disabled rows="4" name="keterangan" id="keterangan" class="form-control form-control-sm" type="text" placeholder="keterangan" id="Keterangan">{{ $data->keterangan }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row ">
                                    <label for="example-text-input" class="col-sm-3 col-form-label-sm">Bukti Pendukung</label>
                                    <div class="col-sm-9 group-image">
                                        <div class="text-center">
                                            <img id="showImage" class="img-thumbnail rounded-square" alt="200x200" src="{{ (!empty($data->foto))? url($data->foto):url('upload/no_image.jpg') }}">
                                            
                                        </div>
                                    </div>
                                    <input name="foto_url" class="form-control" style="display: none" type="file" accept="image/png, image/gif, image/jpeg, image/jpg" placeholder="Foto" id="image">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <b class="text-bold">Kalkulasi Persetujuan</b>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-4 col-form-label-sm">Total Pendapatan <span id="total_tiket"> @if($data->total_tiket != null) ({{$data->total_tiket}} Tiket) @endif</span></label>
                                        <div class="col-sm-8">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_pendapatan" disabled data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_pendapatan }}" type="text" name="total_pendapatan" placeholder="Total Pendapatan">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-4 col-form-label-sm">Perforasi <span id="perforasi"> @if($data->perforasi != null) ({{$data->perforasi}}%) @endif</span></label>
                                        <div class="col-sm-8">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_perforasi" disabled data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_perforasi }}" type="text" name="total_perforasi" placeholder="Total Perforasi">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-4 col-form-label-sm">Asuransi <span id="asuransi"> @if($data->asuransi != null) (Rp. {{$data->asuransi}}) @endif</span></label>
                                        <div class="col-sm-8">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_asuransi" disabled data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_asuransi }}" type="text" name="total_asuransi" placeholder="Total Asuransi">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-4 col-form-label-sm">Biaya Cetak <span id="biaya_cetak"> @if($data->biaya_cetak != null) (Rp. {{$data->biaya_cetak}}) @endif</span></label>
                                        <div class="col-sm-8">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_biaya_cetak" disabled data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_biaya_cetak }}" type="text" name="total_biaya_cetak" placeholder="Total Biaya Cetak">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="example-text-input " class="col-sm-4 col-form-label-sm text-bold">Pendapatan Bersih</label>
                                        <div class="col-sm-8">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_pendapatan_bersih" disabled data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_pendapatan_bersih }}" type="text" name="total_pendapatan_bersih" placeholder="Total Pendapatan Bersih">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <b class="text-bold">Pembagian Pendapatan</b>
                                    <hr>
                                    <div class="form-group row mb-3">
                                        <div style="font-style: italic;">

                                            <span style="color: red">* </span>Pembagian pendapatan dibagi setelah dikurangi Perforasi, Asuransi,
                                            dan Biaya Cetak <span id="keterangan_pendapatan_bersih"></span> 
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-4 col-form-label-sm">Perhutani<span id="persen_profit_perhutani"> @if($data->persen_profit_perhutani != null) ({{$data->persen_profit_perhutani}}%) @endif</span></label>
                                        <div class="col-sm-8">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_profit_perhutani" disabled data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_profit_perhutani }}" type="text" name="total_profit_perhutani" placeholder="Total Pendapatan Perhutani">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-4 col-form-label-sm">IDN <span id="persen_profit_idn"> @if($data->persen_profit_idn != null) ({{$data->persen_profit_idn}}%) @endif</span></label>
                                        <div class="col-sm-8">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_profit_idn" disabled data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_profit_idn }}" type="text" name="total_profit_idn" placeholder="Total Pendapatan IDN">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 mt-3">
                            <div class="row">
                                <div class="col-md-6">
                                    @if($approval->m_user_approval_id)
                                    <div>
                                        <b class="text-bold">Persetujuan</b>
                                        <hr>
                                        <div class="btn-group btn-group sm float-left">
                                            <div class=" mr-2" style="margin-right: 10px;">
                                                <img class="rounded-circle header-foto-persetujuan" src="{{ (!empty($approval->m_user_approval_foto))? url($approval->m_user_approval_foto):url('upload/no_image.jpg') }}">
                                            </div>
                                            <div style="width: fit-content">
                                                <span class="text-bold">{{$approval->m_user_approval_nama}} </span> <br>
                                                {{$approval->m_user_approval_akses_nama}}
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group btn-group sm float-right" style="padding-top: 60px;">
                                        <a class="btn btn-sm btn-light float-right" href="{{ route($route . '.index') }}"><i class="fa fa-arrow-left"></i> Kembali</a>
                                        <button class="btn btn-sm btn-success float-right" type="submit"><i class="fa fa-check"></i> Approve</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/inputmask@5.0.8/dist/jquery.inputmask.min.js"></script>

<script>
    $(document).ready(function() {
        $(":input").inputmask();
        $("input[name=type]").change(function() {
            if ($("#operator").is(':checked')) {
                $("#destinasi_select").show();
            } else {
                $("#destinasi_select").hide();
            }
        });
        $('#image').change(function(e) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#showImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });

    });
</script>
@endsection