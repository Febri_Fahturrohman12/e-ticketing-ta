@extends('admin.admin_master')
@section('admin')<div class="page-content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
            <div class="row" style="padding-top: 20px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="footer-newsletter">
                        <strong class="card-ttl">Konfirmasi Kode Tiket</strong>
                        {{-- <p style="font-size: 14px;">Untuk melihat detail tiket, Operator dapat mengisi form cek
                            kode tiket di bawah ini.</p> --}}
                        <hr>
                    </div>
                    <div class="col-lg-6 col-12 col-offset-4" style="margin-top: 30px;">
                        <div class="footer-newsletter">
                            
                            <h6 style="font-weight: bold;color: #505D69">Cek Kode Tiket</h6>
                            <p style="font-size: 12px;">Silahkan isi menggunakan kode tiket pengunjung</p>
                        </div>
                        <form action="{{ route('noscangettiket') }}" method="get"
                            style="margin-top: 15px;background: #fff;padding: 6px 10px;position: relative;border-radius: 50px;text-align: left;border: 1px solid #b9b9fa;">
                            <input type="text" id="kode" name="kode" value="{{ $data->kode }}"
                                style="border: 0;padding: 4px 8px;width: calc(100% - 100px);">
                            <input type="submit" value="Cek Tiket"
                                style="position: absolute;top: -1px;right: -1px;bottom: -1px;border: 0;background: none;font-size: 16px;padding: 0 20px;background: #006342;color: #fff;transition: 0.3s;border-radius: 50px;box-shadow: 0px 2px 15px rgb(0 0 0 / 10%);">
                        </form>
                    </div>
                </div>
                @if($data->ada_data == 0)
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="col-12" style="background-color: #fff;border-radius: 1rem;display: inline-block;margin-bottom: 3rem;padding: 32px 18px 24px;font-family: 'Poppins', sans-serif">
                        <div class="row">
                            <div class="col-12" style="text-align: center">
                                <img src="{{ asset('backend/assets/images/scan.png') }}" width="30%" alt="logo-sm">
                                <br>
                                <p style="font-weight: bold;">ISI KODE TIKET TERLEBIH DAHULU</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($data->ada_data == 1)
                    @if($data->status == 0)
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="col-12" style="background-color: #fff;border-radius: 1rem;display: inline-block;margin-bottom: 3rem;padding: 32px 18px 24px;font-family: 'Poppins', sans-serif">
                                <div class="row">
                                    <div class="col-12">
                                        <p style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 14px;margin-bottom: 0px;">
                                            Scan QR Code</p>
                                            <span style="font-size: 13px;margin-top:5px">
                                                {{$data->nama_destinasi}} - <b>{{$data->kode}}</b>
                                            </span>
                                        <hr style="margin-top: 10px;">
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-8">
                                                <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                    {{$data->jumlah_pengunjung}} Pengunjung</p>
                                            </div>
                                            <div class="col-4" style="text-align: right;">
                                                <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                    Rp. {{ Str::currency($data->harga_pengunjung) }}</p>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-8">
                                                <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                    Parkir {{$data->nama_kendaraan}}</p>
                                            </div>
                                            <div class="col-4" style="text-align: right;">
                                                <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                    Rp. {{ Str::currency($data->biaya_kendaraan) }}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <p
                                                    style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 13px;margin-bottom: 0px;">
                                                    Total</p>
                                            </div>
                                            <div class="col-6" style="text-align: right;">
                                                <p
                                                    style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 13px;margin-bottom: 0px;">
                                                    Rp. {{ Str::currency($data->total_harga) }}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-12 mt-3" style="text-align: center;position: relative;bottom:0;">
                                    <a class="btn-get-started" href="{{ route("transaksi-cetak" . '.noscanconfirm', $data->id) }}" style="font-family: 'Poppins', sans-serif;font-weight: 400;font-size: 12px;letter-spacing: 0.5px;display: inline-block;padding: 8px 30px 9px 30px;margin-bottom: 12px;border-radius: 3px;transition: 0.5s;border-radius: 50px;background: #006342;color: #fff;border: 2px solid #006342;margin-right: 10px;">
                                        Konfirmasi
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($data->status == 1)
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="col-12" style="background-color: #fff;border-radius: 1rem;display: inline-block;margin-bottom: 3rem;padding: 32px 18px 24px;font-family: 'Poppins', sans-serif">
                                <div class="row">
                                    <div class="col-12" style="text-align: center">
                                        <img src="{{ asset('backend/assets/images/scandone.png') }}" width="30%" alt="logo-sm">
                                        <br>
                                        <p style="font-weight: bold;">KODE TIKET SUDAH DI KONFIRMASI SEBELUMNYA</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
        </div>
        </div>
    </div>
@endsection