<style>
    @media print {
        body {
            zoom: 67%;
        }
    }
</style>
<div style="width: 265px; margin: 0 auto; border: 1px solid #f2f2f2; box-shadow; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); padding-top: 0px;">
    <div>
      <h3 style="text-align: center; margin: 0px;text-transform: uppercase;">{{$data->nama_destinasi}}</h3>
    </div>
    
    <h6 style="text-align: center; font-size: 15px; margin: 5px 0px; font-weight: normal">{{$data->alamat}}</h6>
    <hr>
    <div style="text-align: center;margin-top:10px;">
        <?php
            // take a string in a variable
            $string = $data->uniq_ticket;

            // add the string in the Google Chart API URL
            // $google_chart_api_url = "https://chart.googleapis.com/chart?chs=150x150&chld=L|0&cht=qr&chl=".$string."&choe=UTF-8";
            $google_chart_api_url = "https://quickchart.io/qr?size=200&text=".$string;

            // let's display the generated QR code
            echo "<img src='".$google_chart_api_url."' alt='".$string."'>";
        ?>
    </div>
    <h6 style="text-align: center; font-size: 17px; margin:10px 0px 0px 0px;">{{$data->kode}}</h6>
    <h6 style="text-align: center; font-size: 14px; margin:0px 0px;font-weight: normal;font-style: italic;">{{$data->updated_at}}</h6>
      
        =============================
        <table>
            <tr>
                <td style="width: 50px"><b>Qty</b></td>
                <td style="width: 120px">Ket</td>
                <td style="width: 120px">Sub Total (Rp.)</td>
            </tr>
        </table>
        =============================
        <table style="text-align: center">
            <tbody>
                <tr>
                    <td style="width: 50px">{{$data->jumlah_pengunjung}}</td>
                    <td style="width: 120px;text-align: left;">Pengunjung</td>
                    <td style="width: 120px;  text-align: right">{{ Str::currency($data->harga_pengunjung) }}</td>
                </tr>
                <tr>
                    <td style="width: 50px">1</td>
                    <td style="width: 120px;text-align: left;">Park ({{$data->nama_kendaraan}})</td>
                    <td style="width: 120px;  text-align: right">{{ Str::currency($data->biaya_kendaraan) }}</td>
                </tr>
            </tbody>
        </table>
        =============================
        <table>
            <tr>
                <td style="width: 50px"></td>
                    <td style="width: 120px"><b>Total</b></td>
                <td style="width: 120px;  text-align: right">{{ Str::currency($data->total_harga) }}</td>
            </tr>
        </table>
        =============================
        <div style="text-align: center;">[TERIMA KASIH]</div>
        <div style="text-align: center;">atas kunjunganya, Selamat menikmati liburan dan semoga hari anda menyenangkan</div>
        <hr style="border-style: dotted 1px solid #000;">
    </div>
    {{-- <script type="text/javascript">
        window.onafterprint = window.close;
        window.print();
     </script> --}}