@extends('admin.admin_master')
@section('admin')<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                <div class="card bg-white shadow rounded-3 p-3 border-0">
                    <video id="preview" style="border-radius: 5px;"></video>
                    <div class="btn-group btn-group-toggle mb-5" data-toggle="buttons">
                        <label class="btn btn-primary">
                          <input type="radio" name="options" value="2" autocomplete="off"> Back Camera
                        </label>
                        <label class="btn btn-secondary">
                          <input type="radio" name="options" value="1" autocomplete="off"> Front Camera
                        </label>
                      </div>
                    <form action="{{ route("transaksi-cetak.gettiket") }}" method="GET" id="formgettiket" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="kodetiket" id="kodetiket" value="{{$data->kodetiket}}">
                    </form>
                </div>
            </div>
            @if($data->ada_data == 0)
            <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                <div class="col-12" style="background-color: #fff;border-radius: 1rem;box-shadow: 0 6px 20px rgb(17 26 104 / 10%);display: inline-block;margin-bottom: 3rem;padding: 32px 18px 24px;font-family: 'Poppins', sans-serif">
                    <div class="row">
                        <div class="col-12" style="text-align: center">
                            <img src="{{ asset('backend/assets/images/scan.png') }}" width="60%" alt="logo-sm">
                            <br>
                            <p style="font-weight: bold;">SCAN QR CODE TERLEBIH DAHULU</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if($data->ada_data == 1)
                @if($data->status == 0)
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                        <div class="col-12" style="background-color: #fff;border-radius: 1rem;box-shadow: 0 6px 20px rgb(17 26 104 / 10%);display: inline-block;margin-bottom: 3rem;padding: 32px 18px 24px;font-family: 'Poppins', sans-serif">
                            <div class="row">
                                <div class="col-12">
                                    <p style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 14px;margin-bottom: 0px;">
                                        Scan QR Code</p>
                                        <span style="font-size: 13px;margin-top:5px">
                                            {{$data->nama_destinasi}} - <b>{{$data->kode}}</b>
                                        </span>
                                    <hr style="margin-top: 10px;">
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-8">
                                            <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                {{$data->jumlah_pengunjung}} Pengunjung</p>
                                        </div>
                                        <div class="col-4" style="text-align: right;">
                                            <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                Rp. {{ Str::currency($data->harga_pengunjung) }}</p>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-8">
                                            <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                Parkir {{$data->nama_kendaraan}}</p>
                                        </div>
                                        <div class="col-4" style="text-align: right;">
                                            <p style="font-family: 'Open Sans', sans-serif;font-size: 12px;margin-bottom: 0px;">
                                                Rp. {{ Str::currency($data->biaya_kendaraan) }}</p>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <p
                                                style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 13px;margin-bottom: 0px;">
                                                Total</p>
                                        </div>
                                        <div class="col-6" style="text-align: right;">
                                            <p
                                                style="font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 13px;margin-bottom: 0px;">
                                                Rp. {{ Str::currency($data->total_harga) }}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12 mt-3" style="text-align: center;position: relative;bottom:0;">
                                <a class="btn-get-started" href="{{ route("transaksi-cetak" . '.confirm', $data->id) }}" style="font-family: 'Poppins', sans-serif;font-weight: 400;font-size: 12px;letter-spacing: 0.5px;display: inline-block;padding: 8px 30px 9px 30px;margin-bottom: 12px;border-radius: 3px;transition: 0.5s;border-radius: 50px;background: #5a5af3;color: #fff;border: 2px solid #5a5af3;margin-right: 10px;">
                                    Konfirmasi
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                @if($data->status == 1)
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                        <div class="col-12" style="background-color: #fff;border-radius: 1rem;box-shadow: 0 6px 20px rgb(17 26 104 / 10%);display: inline-block;margin-bottom: 3rem;padding: 32px 18px 24px;font-family: 'Poppins', sans-serif">
                            <div class="row">
                                <div class="col-12" style="text-align: center">
                                    <img src="{{ asset('backend/assets/images/scandone.png') }}" width="60%" alt="logo-sm">
                                    <br>
                                    <p style="font-weight: bold;">QR CODE SUDAH DI SCAN SEBELUMNYA</p>
                                    <br>
                                    <a class="btn-get-started" href="{{ route("transaksi-cetak" . '.scan') }}" style="font-family: 'Poppins', sans-serif;font-weight: 400;font-size: 12px;letter-spacing: 0.5px;display: inline-block;padding: 8px 30px 9px 30px;margin-bottom: 12px;border-radius: 3px;transition: 0.5s;border-radius: 50px;background: #5a5af3;color: #fff;border: 2px solid #5a5af3;margin-right: 10px;">
                                        KEMBALI SCAN
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="{{ asset('backend/assets/js/instascan.min.js') }}"></script>
    <script type="text/javascript">
        let scanner = new Instascan.Scanner({ video: document.getElementById('preview'), mirror: false });
        
        scanner.addListener('scan', function (content) {
          console.log(content);
        });
        Instascan.Camera.getCameras().then(function (cameras) {
            if(cameras.length>0){
                const tampilKamera = cameras[1] ? cameras[1] : cameras[0];
                scanner.start(tampilKamera);
                $('[name="options"]').on('change',function(){
                    if($(this).val()==1){
                        if(cameras[0]!=""){
                            scanner.start(cameras[0]);
                        }else{
                            alert('No Front camera found!');
                        }
                    }else if($(this).val()==2){
                        if(cameras[1]!=""){
                            scanner.start(cameras[1]);
                        }else{
                            alert('No Back camera found!');
                        }
                    }
                });
            }else{
                console.error('No cameras found.');
            }
        }).catch(function (e) {
          console.error(e);
        });

        scanner.addListener('scan', function(c){
            document.getElementById('kodetiket').value = c;
            document.getElementById('formgettiket').submit();
        })
    </script>
@endsection