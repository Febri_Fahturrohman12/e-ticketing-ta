@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                    @csrf
                    <input name="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong class="card-ttl">{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-12 mb-3">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">Destinasi</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <select name="m_destination_id" id="destinasi" class="form-control form-control-sm">
                                                <option value="">Pilih Destinasi</option>
                                                @foreach ($listDestinasi as $key => $value)
                                                    <option value="{{ $value->id }}" {{$data->m_destination_id == $value->id ? 'selected' : ''}}>{{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">Kendaraan</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <select name="m_destination_detail_id" id="destinasi_detail" class="form-control form-control-sm">
                                                @foreach ($listDestinasiDetail as $key => $value)
                                                    <option value="{{ $value->id }}" {{$data->m_destination_detail_id == $value->id ? 'selected' : ''}}>{{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm required">Jumlah Pengunjung</label>
                                        <div class="col-lg-8 col-sm-3">
                                            <div class="input-group input-group-sm">
                                                <input type="number" class="form-control form-control-sm number"
                                                    name="jumlah_pengunjung" value="{{ $data->jumlah_pengunjung }}" id="jumlah_pengunjung"
                                                    placeholder="Jumlah Pengunjung">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">No Telepon</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <input name="no_telepon" class="form-control form-control-sm" value="{{ $data->no_telepon }}" type="text" placeholder="Masukan no telepon" id="no_telepon">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">Kode Referral</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <input name="kd_referral" class="form-control form-control-sm" value="{{ $data->kd_referral }}" type="text" placeholder="Masukan kode Referral" id="kd_referral">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">&nbsp;</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <button class="btn btn-sm btn-info" type="button" onclick="setBiaya({{$listDestinasi}})">Hitung</button>    
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-12 " style="background: #faf1f1;
                                border-radius: 8px;
                                padding: 20px;">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6 col-12">
                                            <p>Total Pembayaran</p>
                                        </div>
                                        <div class="col-sm-6 col-xs-6 col-12">
                                            <p style="font-weight: bold;text-align: right;margin:0px;padding:0px;">Rp. <span id="total_bayar"></span></p>
                                            <hr>
                                            <p style="text-align: right;margin:0px;padding:0px;">Tiket : Rp. <span id="harga_tiket"></span></p>
                                            <p style="text-align: right;margin:0px;padding:0px;">Kendaraan : Rp. <span id="harga_kendaraan"></span></p>
                                            <input style="display: none;" name="tiket_price" class="form-control form-control-sm" type="text" placeholder="tiket_price" id="tiket_price">
                                            <input style="display: none;" name="kendaraan_price" class="form-control form-control-sm" type="text" placeholder="kendaraan_price" id="kendaraan_price">
                                        </div>
                                    </div>                                        
                                </div>
                                <div class="col-lg-8 col-sm-12 mt-5" style="text-align: center">
                                    <div class="btn-group btn-group md">
                                        <a class="btn btn-sm btn-light "
                                            href="{{ route($route . '.index') }}"><i class="fa fa-arrow-left"></i>
                                            Kembali</a>
                                        <button class="btn btn-sm btn-primary " type="submit"><i
                                                class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('#destinasi').click(function () {
            var idCountry = this.value;
            $("#destinasi_detail").html('');
            $.ajax({
                url: "{{url('api/fetch-states')}}",
                type: "POST",
                data: {
                    country_id: idCountry,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function (result) {
                    $('#destinasi_detail').html('<option value="">Pilih Kendaraan</option>');
                    $.each(result.states, function (key, value) {
                        $("#destinasi_detail").append('<option value="' + value
                            .id + '">' + value.nama + '</option>');
                    });
                }
            });
        });
    });
    function setBiaya(list) {
        var destinasi_id = document.getElementById('destinasi').value;
        var destinasi_detail_id = document.getElementById('destinasi_detail').value;
        var jumlah_pengunjung = document.getElementById('jumlah_pengunjung').value;

        let tiket_price = 0;
        let kendaraan_price = 0;
        let stepone = 0;
        let steptwo = 0;

        list.forEach(element => {
            if(element.id == destinasi_id) {
                tiket_price = element.harga;
                document.getElementById('harga_tiket').innerHTML = "( "+element.harga + " x " + jumlah_pengunjung +" )";
                document.getElementById('tiket_price').value = element.harga;
                return;
            }
        });
        $.ajax({
            url: "{{url('api/fetch-states')}}",
            type: "POST",
            data: {
                country_id: destinasi_id,
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function (result) {
                $.each(result.states, function (key, value) {
                    if(value.id == destinasi_detail_id){
                        kendaraan_price = value.harga;
                        document.getElementById('harga_kendaraan').innerHTML = value.harga;
                        document.getElementById('kendaraan_price').value = value.harga;
                        hitungTotal();
                        return;
                    }
                });
            }
        });
        
    }
    function hitungTotal() {
        var tiket_price_total = document.getElementById('tiket_price').value;
        var kendaraan_price_total = document.getElementById('kendaraan_price').value;
        var jumlah_pengunjung_total = document.getElementById('jumlah_pengunjung').value;

        var total_bayar = parseInt(tiket_price_total * jumlah_pengunjung_total) + parseInt(kendaraan_price_total);
        document.getElementById('total_bayar').innerHTML = total_bayar;
    }
</script>