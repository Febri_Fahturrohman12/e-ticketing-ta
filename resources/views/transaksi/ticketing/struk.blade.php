@extends('admin.admin_master')
@section('admin')
<style>
    .tab {
  width: 90%;
  margin: 0 auto;
  height: 15px;
  margin-bottom: -5px;
  background-color: #fff;
  box-shadow: inset 0 0 35px #ddd;
  border-radius: 50px
}

.pseudo {
  position: relative;
}

.pseudo:before {
  content: "";
  position: absolute;
  width: 96%;
  top: 5px;
  left: 2%;
  border-bottom: 1px dashed #000;
}

.panel-dark {
  background-color: #f7f7f7;
  border-color: #f7f7f7;
  box-shadow: 0 5px 15px #ccc;
}

.pseudo:after {
  content: '';
  position: absolute;
  display: block;
  width: 100%;
  height: 10px;
  bottom: -10px;
  left: 0;
  background-image: linear-gradient(45deg, rgba(0, 0, 0, 0) 33.333%, #f7f7f7 33.333%, #f7f7f7 66.667%, rgba(0, 0, 0, 0) 66.667%), linear-gradient(-45deg, rgba(0, 0, 0, 0) 33.333%, #f7f7f7 33.333%, #f7f7f7 66.667%, rgba(0, 0, 0, 0) 66.667%);
  background-size: 20px 40px;
  background-position: 50% -30px;
  background-repeat: repeat-x;
  z-index: 1;
}

.container-print {
  padding: 0;
}

@-webkit-keyframes shake-it-baby {
  0% {
    height: 0px;
    transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
  }
  10% {
    height: 10px;
    transform: translate(0.5px, -0.5px) rotate(0.1deg);
  }
  20% {
    height: 20px;
    transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
  }
  30% {
    height: 35px;
    transform: translate(0.5px, -0.5px) rotate(0.1deg);
  }
  40% {
    height: 50px;
    transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
  }
  50% {
    height: 70px;
    transform: translate(0.5px, -0.5px) rotate(0.1deg);
  }
  60% {
    height: 75px;
    transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
  }
  70% {
    height: 95px;
    transform: translate(0.5px, -0.5px) rotate(0.1deg);
  }
  80% {
    height: 105px;
    transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
  }
  90% {
    height: 125px;
    transform: translate(0.5px, -0.5px) rotate(0.1deg);
  }
  100% {
    height: 140px;
    transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
  }
}

.container-print .panel-body {
  animation: shake-it-baby 1s;
  overflow: hidden;
}
</style>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12" style="text-align: center">
                <div style="margin-top: 20px;">
                    <div class="row">
                      <div class="col-sm-6 offset-sm-3">
                        <div class="tab"></div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6 offset-sm-3 container-print">
                        <div class="panel panel-dark panel-default pseudo">
                          <div class="panel-body">
                            <div class="row" style="margin-top: 25px;">
                                <div class="col-sm-12">
                                   <h5 style="font-weight: bold">{{$data->nama_destinasi}}</h5>
                                   <p style="margin: 3px;padding-left: 20px;padding-right: 20px;font-style: italic;">{{$data->alamat}}</p>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 7px;padding-top: 0px;">
                                <div class="col-sm-10 offset-sm-1" style="padding:0px;">
                                    <?php
                                        // take a string in a variable
                                        $string = $data->uniq_ticket;

                                        // add the string in the Google Chart API URL
                                        // $google_chart_api_url = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=".$string."&choe=UTF-8";
                                        $google_chart_api_url = "https://quickchart.io/qr?size=200&text=".$string;

                                        // let's display the generated QR code
                                        echo "<img src='".$google_chart_api_url."' alt='".$string."'>";
                                    ?>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-12">
                                   <h4 style="font-weight: bold">{{$data->kode}}</h4>
                                   <p style="margin: 0px;padding-left: 20px;padding-right: 20px;font-style: normal;">{{$data->updated_at}}</p>
                                </div>
                            </div>
                            <hr />
                            <div class="row" style="margin: 7px auto;">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-6" style="text-align: left;">Pengunjung ({{$data->jumlah_pengunjung}})</div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-6" style="text-align: right;">
                                Rp. {{ Str::currency($data->harga_pengunjung) }}
                              </div>
                            </div>
                            <div class="row" style="margin: 7px auto;">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6" style="text-align: left;">Parkir ({{$data->nama_kendaraan}})</div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6" style="text-align: right;">
                                  Rp. {{ Str::currency($data->biaya_kendaraan) }}
                                </div>
                            </div>
                            <hr />
                            <div class="row" style="margin: 7px auto;">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6" style="text-align: left;"><span style="font-weight: bold;">TOTAL</span></div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6" style="text-align: right;">
                                  Rp. {{ Str::currency($data->total_harga) }}
                                </div>
                            </div>
                            <br><br>
                            <div class="row" style="margin: 7px auto;">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-12" style="margin-top: 10px;">
                                <a class="btn btn-primary" href="{{ route("transaksi-cetak" . '.index') }}" style="width: 100%;">
                                  <i class="fa fa-check">&nbsp;</i>Selesai
                                </a>
                              </div>
                              <div class="col-lg-4 col-md-4 col-sm-4 col-12" style="margin-top: 10px;">
                                <a class="btn btn-success" href="{{ route("transaksi-cetak" . '.confirmlangsung',$data->id) }}" style="width: 100%;">
                                  <i class="fa fa-check-circle">&nbsp;</i>Selesai & Konfirmasi
                                </a>
                              </div>
                              <div class="col-lg-4 col-md-4 col-sm-4 col-12" style="margin-top: 10px;">
                                <a class="btn btn-info" href="{{ route("transaksi-cetak" . '.print',$data->id) }}" target="_blank" style="width: 100%;">
                                  <i class="fa fa-print">&nbsp;</i>Cetak Bukti
                                </a>
                              </div>
                            </div>
                            <br><br> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>
@endsection