@extends('admin.admin_master')
@section('admin')
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-white">
                        <strong class="card-ttl">{{ $title }}</strong>
                        <form method="get" action="{{ route($route.'.index') }}" enctype="multipart/form-data">
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-sm" name="kode" id="kode" placeholder="Cari berdasarkan Kode" value="{{ !empty($filter->kode) ? $filter->kode : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="btn-group btn-group-sm">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="btn-group btn-group-sm float-right">
                                        <a class="btn btn-sm btn-primary" href="{{ route($route . '.create') }}"><i class="fa fa-plus"></i> Tambah</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="thead-theme">
                                    <th>Kode</th>
                                    <th style="width: 15%">Nama Pembuat</th>
                                    <th style="width: 15%">Nama Destinasi</th>
                                    <th class="text-center">Tanggal</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Total Pendapatan</th>
                                    <th width="5%"></th>    
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $value)
                                    <tr>
                                        <td>{{ $value->kode }}</td>
                                        <td>{{ $value->nama_pengaju }}</td>
                                        <td>{{ $value->destination_name }}</td>
                                        <td class="text-center">{{ $value->tanggal_mulai }} - {{$value->tanggal_selesai}}</td>
                                        <td class="text-center">
                                            @if($value->status == 'menunggu')
                                            <span class="badge badge-warning">
                                                {{ $value->status }}
                                            </span>
                                            @endif
                                            @if($value->status == 'disetujui')
                                            <span class="badge badge-success">
                                                {{ $value->status }}
                                            </span>
                                            @endif
                                        </td>
                                        <td class="text-center">Rp {{ Str::currency($value->total_pendapatan) }}</td>
                                        <td class="text-right" style="text-align: end;">
                                            <form action="{{ route($route . '.destroy', $value->id) }}" method="POST">
                                                <div class="btn-group btn-group-sm">
                                                    @if(Session::get('user')->id == $value->m_user_approval_id && $value->status == 'menunggu')
                                                        <a class="btn btn-sm btn-success" href="{{ route($route . '.approval', $value->id) }}"><i class="fa fa-check"></i></a>
                                                    @endif
                                                    <a class="btn btn-sm btn-primary" href="{{ route($route . '.edit', $value->id) }}"><i class="fa fa-edit"></i></a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-sm btn-danger" type="button" onclick="hapus(this)"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div>
                                @include('admin.body.pagination')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection