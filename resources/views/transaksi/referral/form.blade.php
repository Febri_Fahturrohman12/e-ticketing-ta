@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                    @csrf
                    <input name="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong class="card-ttl">{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-12 mb-3">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">Kode Ticket</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <input name="kode_ticket" class="form-control form-control-sm" value="{{ $data->kode_ticket }}" type="text" disabled id="kode_ticket">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">Kode Referral</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <input name="kode_reff" class="form-control form-control-sm" value="{{ $data->kode_reff }}" type="text" disabled id="kode_reff">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm">Nama</label>
                                        <div class="col-lg-8 col-sm-9">
                                            <input name="nama" class="form-control form-control-sm" value="{{ $data->nama }}" type="text" disabled id="nama">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-4 col-sm-3 col-form-label-sm required">Jumlah Pengunjung</label>
                                        <div class="col-lg-8 col-sm-3">
                                            <div class="input-group input-group-sm">
                                                <input type="number" class="form-control form-control-sm number"
                                                    name="reff_pengunjung" value="{{ $data->reff_pengunjung }}" id="reff_pengunjung"
                                                    placeholder="Jumlah Pengunjung">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-4 col-form-label-sm">Total Reward </label>
                                        <div class="col-lg-8 col-sm-3">
                                            <div class='input-group input-group-sm'>
                                                <div class="input-group-append input-group-addon">
                                                    <span class="input-group-text text-bold input-group-text-sm">
                                                        Rp
                                                    </span>
                                                </div>
                                                <input id="total_reward" data-inputmask="'alias': 'numeric', 'digits':0, 'groupSeparator' : '.', 'removeMaskOnSubmit': true, 'autoUmask': true" class="form-control-sm dateapicker form-control" value="{{ $data->total_reward }}" type="text" name="total_reward" placeholder="Total Pendapatan">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="col-lg-12 col-sm-12">
                                        <div class="btn-group btn-group md float-right">
                                            <a class="btn btn-sm btn-light "
                                                href="{{ route($route . '.index') }}"><i class="fa fa-arrow-left"></i>
                                                Kembali</a>
                                            <button class="btn btn-sm btn-primary " type="submit"><i
                                                    class="fa fa-check"></i> Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection