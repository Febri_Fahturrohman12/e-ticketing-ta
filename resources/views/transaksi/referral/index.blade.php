@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-white card-ttl">
                            <strong class="card-ttl">Transaksi {{ $title }}</strong>
                            <form method="get" action="{{ route($route.'.index') }}" enctype="multipart/form-data">
                                <div class="row mt-3">
                                    <div class="col-sm-3">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control form-control-sm" name="kode" id="kode" placeholder="Cari berdasarkan kode referral" value="{{ !empty($filter->kode) ? $filter->kode : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="btn-group btn-group-sm">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-6">
                                        <div class="btn-group btn-group-sm float-right">
                                            <a class="btn btn-sm btn-primary" href="{{ route($route . '.create') }}"><i
                                                    class="fa fa-plus"></i> Tambah</a>
                                        </div>
                                    </div> --}}
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-theme">
                                        <th>Kode Tiket</th>
                                        <th>Kode Referral</th>
                                        <th>Tanggal</th>
                                        <th>Nama</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-right">Reward</th>
                                        <th class="text-left">Pengunjung</th>
                                        <th class="text-right">Total Reward</th>
                                        <th width="5%"></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key => $value)
                                            <tr>
                                                <td class="text-capitalize">{{ $value->kd_tiket }}</td>
                                                <td class="text-left">{{ $value->kode_reff }}</td>
                                                <td>{{ $value->tanggal }}</td>
                                                <td>{{ $value->nm_member }}</td>
                                                <td class="text-center">
                                                    @if($value->tipe == 1)
                                                    <span class="badge badge-success">Pemasukan</span>
                                                    @endif
                                                    @if($value->tipe == 2)
                                                    <span class="badge badge-warning">Pengeluaran</span>
                                                    @endif
                                                </td>
                                                <td class="text-right"> Rp {{ Str::currency($value->reward) }}</td>
                                                <td class="text-left">{{ $value->reff_pengunjung }}</td>
                                                <td class="text-right"> Rp {{ Str::currency($value->total_reward) }}</td>
                                                <td>
                                                    {{-- @if ($value->status_pembayaran == 'pending') --}}
                                                    <form action="{{ route($route . '.destroy', $value->id) }}"
                                                    method="POST">
                                                            <div class="btn-group btn-group-sm">
                                                                <a class="btn btn-sm btn-info"
                                                                    href="{{ route($route . '.edit', $value->id) }}"><i
                                                                        class="fa fa-edit"></i></a>
                                                                @csrf
                                                                @method('DELETE')
                                                                <button class="btn btn-sm btn-danger" type="button"
                                                                    onclick="hapus(this)"><i class="fa fa-trash"></i></button>
                                                            </div>
                                                        </form>
                                                    {{-- @endif --}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div>
                                    @include('admin.body.pagination')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
