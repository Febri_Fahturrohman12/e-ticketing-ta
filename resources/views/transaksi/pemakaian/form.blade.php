@extends('admin.admin_master')
@section('admin')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <form method="POST" action="{{ route($route . '.store', $data->id) }}" enctype="multipart/form-data">
                    @csrf
                    <input name="id" class="form-control" type="hidden" value="{{ $data->id }}" id="id">
                    <div class="card">
                        <div class="card-header bg-white">
                            <strong class="card-ttl">{{ $title }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Customer</label>
                                        <div class="col-sm-9">
                                            <select name="customer_id" id="customer_id"
                                                class="form-control form-control-sm">
                                                @foreach ($listCustomer as $key => $value)
                                                    <option value="{{ $value->id }}" {{$data->customer_id == $value->id ? 'selected' : ''}}>{{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row"> 
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Biaya</label>
                                        <div class="col-sm-9">
                                            <select name="biaya_id" id="biaya_id"
                                                class="form-control form-control-sm" onchange="setBiaya({{$listBiaya}})">
                                                @foreach ($listBiaya as $key => $value)
                                                    <option value="{{ $value->id }}" {{$data->biaya_id == $value->id ? 'selected' : ''}}>{{ $value->tanggal }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Biaya</label>
                                        <div class="col-sm-9">
                                            <div class="input-group input-group-sm">
                                                <input type="number" class="form-control form-control-sm number"
                                                    name="biaya_air" value="{{ $data->biaya_air }}" id="biaya_air"
                                                    placeholder="Biaya" readonly>
                                                <button class="btn btn-sm btn-light input-group-addon" type="button">/
                                                    m<sup>3</sup>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Pemakaian</label>
                                        <div class="col-sm-9">
                                            <div class="input-group input-group-sm">
                                                <input type="number" class="form-control form-control-sm number"
                                                    name="pemakaian" value="{{ $data->pemakaian }}" id="pemakaian"
                                                    placeholder="Pemakaian" onchange="hitungTotal()">
                                                <button class="btn btn-sm btn-light input-group-addon" type="button">
                                                    m<sup>3</sup>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Total</label>
                                        <div class="col-sm-9">
                                            <input name="total" class="form-control form-control-sm" type="text"
                                                value="{{ $data->total }}" placeholder="Total" id="total" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-3 col-form-label-sm">Biaya
                                            Alat</label>
                                        <div class="col-sm-9">
                                            <input name="biaya_alat" class="form-control form-control-sm" type="text"
                                                value="{{ $data->biaya_alat }}" placeholder="Biaya Alat" id="biaya_alat"
                                                readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-3">
                                    <div class="btn-group btn-group sm float-right">
                                        <a class="btn btn-sm btn-light float-right"
                                            href="{{ route($route . '.index') }}"><i class="fa fa-arrow-left"></i>
                                            Kembali</a>
                                        <button class="btn btn-sm btn-primary float-right" type="submit"><i
                                                class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
    function hitungTotal() {
        var biaya = document.getElementById('biaya_air').value;
        var pemakaian = document.getElementById('pemakaian').value;
        var biaya_alat = document.getElementById('biaya_alat').value;
        var total = parseInt(biaya * pemakaian) + parseInt(biaya_alat);
        document.getElementById('total').value = total;
    }

    function setBiaya(list) {
        var biaya_id = document.getElementById('biaya_id').value;
        console.log(list)
        list.forEach(element => {
            if(element.id == biaya_id) {
                document.getElementById('biaya_air').value = element.biaya_air;
                document.getElementById('biaya_alat').value = element.biaya_alat;
                hitungTotal();
                return;
            }
        });
    }
</script>
