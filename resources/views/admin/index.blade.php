@extends('admin.admin_master')
@section('admin')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <div class="row">
                        <h4 class="mb-sm-0 col-md-3 mt-2">Dashboard</h4>
                        <div class="col-md-9 ">
                            <form method="get" action="{{ route($route.'.index') }}" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input class="form-control-sm dateapicker form-control" value="{{ $filter->tanggal_mulai }}" type="text" name="tanggal_mulai" placeholder="Tanggal Mulai">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="col-sm-1 mt-1"> s/d</span>
                                    <div class="col-sm-4">
                                        <div class='input-group date input-group-sm clockpicker' id='datetimepicker'>
                                            <input class="form-control-sm dateapicker form-control" value="{{ $filter->tanggal_selesai }}" type="text" name="tanggal_selesai" placeholder="Tanggal Selesai">
                                            <div class="input-group-append input-group-addon ">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="btn-group btn-group-sm">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">FS Creative</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-truncate font-size-14 mb-2 mt-2">Total Pengunjung</p>
                                <h4 class="mb-2">{{ $total_pengunjung }}</h4>
                            </div>
                            <div class="avatar-sm">
                                <span class="avatar-title bg-light text-primary rounded-3">
                                    <i class="ri-user-3-line font-size-24"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-truncate font-size-14 mb-2 mt-2">Total Pendapatan</p>
                                <h4 class="mb-2">Rp {{ Str::currency($total_pendapatan) }}</h4>
                            </div>
                            <div class="avatar-sm">
                                <span class="avatar-title bg-light text-primary rounded-3">
                                    <i class="mdi mdi-currency-usd font-size-24"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 mt-2">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-3 card-title ">
                            Total Pengunjung
                        </h4>
                        <div class="col-md-12">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 mt-2">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4 mt-2">Aktivitas Pengguna</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="thead-theme">
                                    <th>Nama Pembuat</th>
                                    <th>Ip Address</th>
                                    <th>Tanggal</th>
                                    <th>Aktivitas</th>

                                </thead>
                                <tbody>
                                    @foreach ($user_log as $key => $value)
                                    <tr>
                                        <td>{{ $value->users_name }}</td>
                                        <td>{{ $value->ip_address }}</td>
                                        <td>{{ $value->created_at }}</td>
                                        <td>{{ $value->aktivitas }}</td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                         
                            <div>
                                @include('admin.body.pagination')
                            </div>
                        </div>
                    </div><!-- end card -->
                </div><!-- end card -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });
    var arrMonth = <?php echo $chart['month'] ?>;
    var arrData = <?php echo $chart['data'] ?>;
    var data = {
        labels: arrMonth,
        datasets: [{
            label: "Total Pengunjung",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: arrData
        }, ]
    };

    var option = {
        responsive: true,
    };

    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("myChart").getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: option
    })
    // var myLineChart = new Chart(ctx).Line(data, option); //'Line' defines type of the chart.
</script>
@endsection