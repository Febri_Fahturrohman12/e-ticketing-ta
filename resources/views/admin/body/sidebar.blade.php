<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">E-TICKETING MENU</li>
                <li>
                    <a href="{{ route('dashboard') }}" class="waves-effect">
                        <!-- <i class="ri-dashboard-line"></i><span class="badge rounded-pill bg-success float-end">3</span> -->
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-list-settings-fill"></i>
                        <span>Master Data</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @can('master_akses')<li><a href="{{ route('akses.index') }}">Hak Akses</a></li>@endcan
                        @can('master_destinasi')<li><a href="{{ route('destinasi.index') }}">Destinasi</a></li>@endcan
                        @can('master_user')<li><a href="{{ route('user.index_master') }}">Pengguna</a></li>@endcan
                        @can('master_member')<li><a href="{{ route('member.index') }}">Member</a></li>@endcan
                        @can('master_setting_persetujuan')<li><a href="{{ route('setting_persetujuan.index') }}">Setting Persetujuan</a></li>@endcan

                        <!-- <li><a href="{{ route('customer.index') }}">Customer</a></li>
                        <li><a href="{{ route('biaya.index') }}">Biaya</a></li> -->
                    </ul>
                </li>
                @canany('transaksi_ticketing')
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-briefcase-4-line"></i>
                        <span>Transaksi</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @can('transaksi_ticketing')<li><a href="{{ route('transaksi-cetak.index') }}">Cetak Tiket</a></li> @endcan
                        @can('transaksi_ticketing')<li><a href="{{ route('transaksi-cetak.scan') }}">Scan Tiket</a></li> @endcan
                        @can('transaksi_ticketing')<li><a href="{{ route('transaksi-cetak.noscan') }}">Konfirmasi Tiket</a></li> @endcan
                        @can('transaksi_ticketing')<li><a href="{{ route('transaksi-referral.index') }}">Referral</a></li> @endcan
                    </ul>
                </li>
                @endcan
                @canany('transaksi_ticketing')
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-scales-line"></i>
                        <span>Finance</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @can('transaksi_persetujuan_setoran_harian')<li><a href="{{ route('persetujuan_setoran_harian.index') }}">Setoran Harian</a></li> @endcan
                        @can('transaksi_persetujuan_setoran_bulanan')<li><a href="{{ route('persetujuan_setoran_bulanan.index') }}">Setoran Bulanan</a></li> @endcan
                    </ul>
                </li>
                @endcan
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-printer-line"></i>
                        <span>Laporan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        @can('laporan_ticketing')<li><a href="{{ route('laporan-pemakaian') }}">Tiket</a></li>@endcan
                        @can('laporan_keuangan')<li><a href="{{ route('laporan-keuangan') }}">Keuangan</a></li>@endcan
                        @can('laporan_user_log')<li><a href="{{ route('laporan-user-log') }}">Aktivitas Pengguna</a></li>@endcan
                    </ul>
                </li>


            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
