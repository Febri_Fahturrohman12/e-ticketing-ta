@if ($pagination > 1)
    <form method="get" action="{{ route($route . '.index') }}" enctype="multipart/form-data">
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                <li class="page-item">
                    <button class="page-link" type="submit" aria-label="Previous" onclick="setvalue(-1)">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </button>
                </li>
                <li class="page-item"><a class="page-link">
                        <input type="hidden" class="text-center" style="height: 21px; width: 50px;"
                            value="{{ $page }}" name="page" id="page">{{ $page }}</a></li>
                <li class="page-item"><a class="page-link" href="#">/</a></li>
                <li class="page-item"><a class="page-link"><input type="hidden" class="text-center"
                            style="height: 21px; width: 50px;" value="{{ $pagination }}"
                            id="pagination">{{ $pagination }}</a></li>
                <li class="page-item">
                    <button class="page-link" type="submit" aria-label="Next" onclick="setvalue(1)">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </button>
                </li>
            </ul>
        </nav>
        <script>
            function setvalue(value) {
                if ((value == 1 && document.getElementById('page').value < document.getElementById('pagination').value) || (
                        value == -1 && document.getElementById('page').value > 1)) {
                    document.getElementById('page').value = parseInt(document.getElementById('page').value) + value;
                }
            }
        </script>
    </form>
@endif
