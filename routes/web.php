<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AksesController;
use App\Http\Controllers\BiayaController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Demo\DemoController;
use App\Http\Controllers\DestinasiController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\DropdownController;
use App\Http\Controllers\LaporanPemakaianController;
use App\Http\Controllers\LaporanKeuanganController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\PersetujuanSetoranBulananController;
use App\Http\Controllers\PersetujuanSetoranHarianController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SettingPersetujuanController;
use App\Http\Controllers\TransaksiPemakaianController;
use App\Http\Controllers\TransaksiCetakController;
use App\Http\Controllers\TransaksiReferralController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserLogController;
use App\Models\PersetujuanSetoranHarian;
use App\Models\TransaksiReferral;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// EXAMPLE PENGELOMPOKAN ROUTE
Route::controller(DemoController::class)->group(function () {
    Route::get('/demo', 'Index')->name('demo.page');
    Route::get('/demotwo', 'DemoTwo')->name('demotwo.page');
});

// PROJECT
// FRONTEND
// Route::get('/', function () {
//     Route::get('/dashboard', 'index')->middleware(['auth', 'verified'])->name('dashboard');
// });
// Route::get('/', function () {
//     Route::get('/dashboard', 'index')->middleware(['auth', 'verified'])->name('dashboard');
// })->name('index_frontend');
// Route::get('/cektagihan', function () {
//     return view('frontend.frontend_checkpayments');
// })->name('cektagihan');

// Route::controller(FrontendController::class)->group(function () {
//     Route::get('/cektagihan', 'cekTagihan')->name('cektagihan');
//     Route::post('/bayartagihan/{nomer}', 'bayarTagihan')->name('bayartagihan');
// });

// BACKEND
// Admin All Route
Route::controller(DashboardController::class)->group(function () {
    Route::get('/', 'index')->middleware(['auth', 'verified'])->name('dashboard');
});
Route::controller(DashboardController::class)->group(function () {
    Route::get('/dashboard', 'index')->middleware(['auth', 'verified'])->name('dashboard');
    Route::get('/dashboard', 'index')->middleware(['auth', 'verified'])->name('dashboard.index');
});

Route::controller(AdminController::class)->group(function () {
    Route::get('/admin/logout', 'destroy')->name('admin.logout');
    Route::get('/admin/profile', 'Profile')->name('admin.profile');
    Route::get('/edit/profile', 'EditProfile')->name('edit.profile');
    Route::post('/store/profile', 'StoreProfile')->name('store.profile');
    Route::get('/change/password', 'ChangePassword')->name('change.password');
    Route::post('/update/password', 'UpdatePassword')->name('update.password');
});

// Master
Route::middleware('auth','can:master_customer')->resource('customer', CustomerController::class);
Route::middleware('auth','can:master_biaya')->resource('biaya', BiayaController::class);
Route::middleware('auth','can:master_akses')->resource('akses', AksesController::class);
Route::middleware('auth','can:master_destinasi')->resource('destinasi', DestinasiController::class);
Route::middleware('auth','can:master_setting_persetujuan')->resource('setting_persetujuan', SettingPersetujuanController::class);
// Route::middleware('auth')->resource('persetujuan_setoran_harian', PersetujuanSetoranHarianController::class);
// Route::middleware('auth')->resource('persetujuan_setoran_bulanan', PersetujuanSetoranBulananController::class);


Route::middleware('auth','can:transaksi_persetujuan_setoran_harian')->controller(PersetujuanSetoranHarianController::class)->group(function () {
    Route::get('/persetujuan_setoran_harian', 'index')->name('persetujuan_setoran_harian.index');
    Route::get('/persetujuan_setoran_harian/create', 'create')->name('persetujuan_setoran_harian.create');
    Route::get('/persetujuan_setoran_harian/edit/{id}', 'edit')->name('persetujuan_setoran_harian.edit');
    Route::post('/persetujuan_setoran_harian/store', 'store')->name('persetujuan_setoran_harian.store');
    Route::delete('/persetujuan_setoran_harian/destroy/{id}', 'destroy')->name('persetujuan_setoran_harian.destroy');
    Route::get('/persetujuan_setoran_harian/approval/{id}', 'approval')->name('persetujuan_setoran_harian.approval');
    Route::post('/persetujuan_setoran_harian/approve_pengajuan', 'approvePengajuan')->name('persetujuan_setoran_harian.approve_pengajuan');
});

Route::middleware('auth','can:transaksi_persetujuan_setoran_bulanan')->controller(PersetujuanSetoranBulananController::class)->group(function () {
    Route::get('/persetujuan_setoran_bulanan', 'index')->name('persetujuan_setoran_bulanan.index');
    Route::get('/persetujuan_setoran_bulanan/create', 'create')->name('persetujuan_setoran_bulanan.create');
    Route::get('/persetujuan_setoran_bulanan/edit/{id}', 'edit')->name('persetujuan_setoran_bulanan.edit');
    Route::post('/persetujuan_setoran_bulanan/store', 'store')->name('persetujuan_setoran_bulanan.store');
    Route::delete('/persetujuan_setoran_bulanan/destroy/{id}', 'destroy')->name('persetujuan_setoran_bulanan.destroy');
    Route::get('/persetujuan_setoran_bulanan/approval/{id}', 'approval')->name('persetujuan_setoran_bulanan.approval');
    Route::post('/persetujuan_setoran_bulanan/approve_pengajuan', 'approvePengajuan')->name('persetujuan_setoran_bulanan.approve_pengajuan');
    Route::get('/persetujuan_setoran_bulanan/kalkulasiSetoran', 'kalkulasiSetoran');

});

Route::get('/user', function () {
    return view('master.user.index');
})->name('user');

Route::controller(TransaksiPemakaianController::class)->group(function () {
    Route::get('/transaksi-pemakaian/index', 'index')->name('transaksi-pemakaian.index');
    Route::get('/transaksi-pemakaian/create', 'create')->name('transaksi-pemakaian.create');
    Route::get('/transaksi-pemakaian/edit/{id}', 'edit')->name('transaksi-pemakaian.edit');
    Route::post('/transaksi-pemakaian/store', 'store')->name('transaksi-pemakaian.store');
    Route::delete('/transaksi-pemakaian/destroy/{id}', 'destroy')->name('transaksi-pemakaian.destroy');
});

Route::middleware('auth','can:transaksi_ticketing')->controller(TransaksiCetakController::class)->group(function () {
    Route::get('/transaksi-cetak/index', 'index')->name('transaksi-cetak.index');
    Route::get('/transaksi-cetak/create', 'create')->name('transaksi-cetak.create');
    Route::get('/transaksi-cetak/edit/{id}', 'edit')->name('transaksi-cetak.edit');
    Route::post('/transaksi-cetak/store', 'store')->name('transaksi-cetak.store');
    Route::get('/transaksi-cetak/cetak', 'cetak')->name('transaksi-cetak.cetak');
    Route::get('/transaksi-cetak/print/{id}', 'print')->name('transaksi-cetak.print');
    Route::delete('/transaksi-cetak/destroy/{id}', 'destroy')->name('transaksi-cetak.destroy'); 
    Route::get('/transaksi-cetak/scan', 'scan')->name('transaksi-cetak.scan');
    Route::get('/transaksi-cetak/gettiket', 'gettiket')->name('transaksi-cetak.gettiket');
    Route::get('/transaksi-cetak/confirm/{id}', 'confirm')->name('transaksi-cetak.confirm');
    Route::get('/transaksi-cetak/confirmlangsung/{id}', 'confirmlangsung')->name('transaksi-cetak.confirmlangsung');
    Route::get('/transaksi-cetak/noscan', 'noscan')->name('transaksi-cetak.noscan');
    Route::get('/transaksi-cetak/noscangettiket', 'noscangettiket')->name('noscangettiket');
    Route::get('/transaksi-cetak/noscanconfirm/{id}', 'noscanconfirm')->name('transaksi-cetak.noscanconfirm');
    Route::post('api/fetch-states', 'fetchState');
});

Route::middleware('auth','can:transaksi_ticketing')->controller(TransaksiReferralController::class)->group(function () {
    Route::get('/transaksi-referral/index', 'index')->name('transaksi-referral.index');
    Route::get('/transaksi-referral/create', 'create')->name('transaksi-referral.create');
    Route::get('/transaksi-referral/edit/{id}', 'edit')->name('transaksi-referral.edit');
    Route::post('/transaksi-referral/store', 'store')->name('transaksi-referral.store');
    Route::delete('/transaksi-referral/destroy/{id}', 'destroy')->name('transaksi-referral.destroy'); 
});

Route::controller(LaporanPemakaianController::class)->group(function () {
    Route::get('/laporan-pemakaian', 'index')->name('laporan-pemakaian');
    Route::get('/generate-laporan', 'index')->name('generate-laporan');
});
Route::controller(LaporanKeuanganController::class)->group(function () {
    Route::get('/laporan-keuangan', 'index')->name('laporan-keuangan');
    Route::get('/generate-laporan', 'index')->name('generate-laporan');
});
// Route::controller(UserLogController::class)->group(function () {
//     Route::get('/laporan-user-log/index', 'index')->name('laporan-user-log.index');
// });
Route::middleware('auth','can:laporan_user_log')->controller(UserLogController::class)->group(function () {
    Route::get('/laporan-user-log/index', 'index')->name('laporan-user-log.index');
    Route::get('/laporan-user-log', 'index')->name('laporan-user-log');
});

Route::middleware('auth','can:master_user')->controller(UserController::class)->group(function () {
    Route::get('/user/index', 'index_master')->name('user.index');
    Route::get('/user/index_master', 'index_master')->name('user.index_master');
    Route::get('/user/create', 'create')->name('user.create');
    Route::get('/user/edit/{id}', 'edit')->name('user.edit');
    Route::post('/user/store', 'store')->name('user.store');
    Route::delete('/user/destroy/{id}', 'destroy')->name('user.destroy');
});
Route::middleware('auth','can:master_member')->controller(MemberController::class)->group(function () {
    Route::get('/member/index', 'index')->name('member.index');
    Route::get('/member/create', 'create')->name('member.create');
    Route::get('/member/edit/{id}', 'edit')->name('member.edit');
    Route::post('/member/store', 'store')->name('member.store');
    Route::delete('/member/destroy/{id}', 'destroy')->name('member.destroy');
});

// Route::middleware('auth')->controller(DestinasiController::class)->group(function () {
//     Route::get('/destinasi', 'index')->name('destinasi.index');
//     Route::get('/destinasi/edit/{id}', 'edit')->name('destinasi.edit');
//     Route::post('/destinasi/store', 'store')->name('destinasi.store');
//     Route::delete('/destinasi/destroy', 'destroy')->name('destinasi.destroy');
// });

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
