<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_setting_persetujuan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('m_user_approval_harian_id')->nullable();
            $table->string('m_user_approval_bulanan_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('t_approval_setoran_harian', function (Blueprint $table) {
            $table->increments('id');
            $table->char('kode',255)->nullable();
            $table->date('tanggal')->nullable();
            $table->bigInteger('total_pendapatan')->nullable();
            $table->text('keterangan')->nullable();
            $table->text('foto')->nullable();
            $table->char('status',50)->nullable();
            $table->bigInteger('m_user_pengajuan_id')->nullable();
            $table->bigInteger('m_user_approval_id')->nullable();
            $table->char('m_user_approval_nama', 255)->nullable();
            $table->dateTime('approved_at')->nullable();      
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('t_approval_setoran_bulanan', function (Blueprint $table) {
            $table->increments('id');
            $table->char('kode',255)->nullable();
            $table->date('tanggal_mulai')->nullable();
            $table->date('tanggal_selesai')->nullable();
            $table->bigInteger('total_pendapatan')->nullable();
            $table->text('keterangan')->nullable();
            $table->text('foto')->nullable();
            $table->char('status',50)->nullable();
            $table->bigInteger('m_user_pengajuan_id')->nullable();
            $table->bigInteger('m_user_approval_id')->nullable();
            $table->char('m_user_approval_nama', 255)->nullable();      
            $table->dateTime('approved_at')->nullable();      
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_setting_persetujuan');
        Schema::dropIfExists('t_approval_setoran_harian');
        Schema::dropIfExists('t_approval_setoran_bulanan');
    }
};
