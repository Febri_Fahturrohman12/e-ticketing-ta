<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_approval_setoran_bulanan', function($table) {
            $table->integer('m_destination_id')->after('id')->nullable();
            $table->bigInteger('total_perforasi')->after('m_user_pengajuan_id')->nullable();
            $table->bigInteger('total_asuransi')->after('total_perforasi')->nullable();
            $table->bigInteger('total_biaya_cetak')->after('total_asuransi')->nullable();
            $table->bigInteger('total_pendapatan_bersih')->after('total_biaya_cetak')->nullable();
            $table->bigInteger('total_profit_perhutani')->after('total_pendapatan_bersih')->nullable();
            $table->bigInteger('total_profit_idn')->after('total_profit_perhutani')->nullable();
            $table->bigInteger('total_tiket')->after('total_profit_idn')->nullable();
            $table->bigInteger('perforasi')->after('total_tiket')->nullable();
            $table->bigInteger('asuransi')->after('perforasi')->nullable();
            $table->bigInteger('biaya_cetak')->after('asuransi')->nullable();
            $table->bigInteger('persen_profit_perhutani')->after('biaya_cetak')->nullable();
            $table->bigInteger('persen_profit_idn')->after('persen_profit_perhutani')->nullable();


        });
        Schema::table('t_approval_setoran_harian', function($table) {
            $table->integer('m_destination_id')->after('id')->nullable();
            $table->date('tanggal_mulai')->after('tanggal')->nullable();
            $table->date('tanggal_selesai')->after('tanggal_mulai')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_approval_setoran_bulanan', function($table) {
            $table->dropColumn('m_destination_id');
            $table->dropColumn('total_perforasi');
            $table->dropColumn('total_asuransi');
            $table->dropColumn('total_biaya_cetak');
            $table->dropColumn('total_pendapatan_bersih');
            $table->dropColumn('total_profit_perhutani');
            $table->dropColumn('total_profit_idn');
            $table->dropColumn('total_tiket');
            $table->dropColumn('perforasi');
            $table->dropColumn('asuransi');
            $table->dropColumn('biaya_cetak');
            $table->dropColumn('persen_profit_perhutani');
            $table->dropColumn('persen_profit_idn');


        });
        Schema::table('t_approval_setoran_harian', function($table) {
            $table->dropColumn('m_destination_id');
            $table->dropColumn('tanggal_mulai');
            $table->dropColumn('tanggal_selesai');
        });
    }
};
