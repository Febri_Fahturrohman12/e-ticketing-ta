<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_referral', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_member')->nullable();
            $table->integer('id_ticket')->nullable();
            $table->integer('reward')->nullable();
            $table->integer('reff_pengunjung')->nullable();
            $table->integer('total_reward')->nullable();
            $table->text('kode_reff')->nullable();
            $table->integer('tipe')->nullable();
            $table->tinyInteger('is_deleted')->default('0');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_referral');
    }
};