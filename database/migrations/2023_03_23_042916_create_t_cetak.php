<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_cetak', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode')->nullable();
            $table->integer('m_destination_id')->nullable();
            $table->integer('m_destination_detail_id')->nullable();
            $table->integer('jumlah_pengunjung')->nullable();
            $table->integer('total_harga')->nullable();
            $table->string('no_telepon')->nullable();
            $table->string('uniq_ticket')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->tinyInteger('is_deleted')->default('0');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_cetak');
    }
};
