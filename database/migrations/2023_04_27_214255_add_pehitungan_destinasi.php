<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_destination', function($table) {
            $table->integer('perforasi')->after('alamat')->nullable();
            $table->bigInteger('asuransi')->after('perforasi')->nullable();
            $table->bigInteger('biaya_cetak')->after('asuransi')->nullable();
            $table->integer('persen_profit_perhutani')->after('biaya_cetak')->nullable();
            $table->integer('persen_profit_idn')->after('persen_profit_perhutani')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_destination', function($table) {
            $table->dropColumn('perforasi');
            $table->dropColumn('asuransi');
            $table->dropColumn('biaya_cetak');
            $table->dropColumn('persen_profit_perhutani');
            $table->dropColumn('persen_profit_idn');

        });
    }
};
