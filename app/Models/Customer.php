<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'm_customer';

    protected $fillable = [
        'nama',
        'nomer',
        'alamat',
        'telp',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    protected $attributes = [
        'created_by' => 1,
        'updated_by' => 1,
    ];
}
