<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettingPersetujuan extends Model
{
    use HasFactory;
    use SoftDeletes; // Use SoftDeletes library
    public $timestamps = true;
    protected $fillable = [
        'm_user_approval_harian_id',
        'm_user_approval_bulanan_id',
    ];
    protected $table = 'm_setting_persetujuan';

    public $title = 'Setting Persetujuan';
    public $route = 'setting_persetujuan';

    public function getAll()
    {
        return $this->first();
    }
    public function edit(array $payload, int $id)
    {
        return $this->find($id)->update($payload);
    }
    public function store(array $payload)
    {
        return $this->create($payload);
    }
}
