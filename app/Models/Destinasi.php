<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Repository\CrudInterface;
use Illuminate\Database\Eloquent\SoftDeletes;


class Destinasi extends Model implements CrudInterface
{
    use HasFactory;
    use SoftDeletes; // Use SoftDeletes library
    public $timestamps = true;
    protected $fillable = [
        'nama',
        'harga',
        'alamat',
        'perforasi',
        'asuransi',
        'biaya_cetak',
        'persen_profit_perhutani',
        'persen_profit_idn'
    ];
    protected $table = 'm_destination';

    public $title = 'Destinasi';
    public $route = 'destinasi';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;
    private $destinasiDetail;

    public function __construct()
    {
        $this->destinasiDetail = new DestinasiDetail();
    }
    /**
     * Relasi ke DestinasiDetail 
     *
     * @return void
     */
    public function details()
    {
        return $this->hasMany(DestinasiDetail::class, 'm_destination_id', 'id');
    }

    public function drop(int $id)
    {   
        
        return $this->find($id)->delete();
    }

    public function edit(array $payload, int $id)
    {
        return $this->find($id)->update($payload);
    }

    public function getAll(array $filter, int $page = null, string $sort = '')
    {
        // query
        $model = $this->selectRaw('m_destination.*')
            ->whereNull('m_destination.deleted_at')
            ->orderBy('m_destination.id', 'ASC');

        if (!empty($filter['nama'])) {
            $model->where($this->table.'.nama', 'LIKE','%'.$filter['nama'].'%');
        }
        // pagination
        $this->pagination = ceil($model->count() /$this->limit);
        if ($this->pagination > 1) {
            if (!empty($page) ) {
                $this->page = $page;
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }
        
        // $response['filter'] = (object) $filter;

        // get index
        $model = $model->get();
        $response = [
            'filter' => (object)$filter,
            'data' => $model,
            'title' => $this->title,
            'page' => $this->page,
            'pagination' => $this->pagination,
            'route' => $this->route,

        ];
        return $response;
    }

    public function getById(int $id)
    {
        return $this->find($id);
    }

    public function store(array $payload)
    {   
        $save = $this->create($payload);
       
        return $save;
    }
    public function tambah(){
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;
        $response['data'] = getFieldTable($this->table);

        return $response;
    }
    public function getData($id){
        $response['title'] = $this->title . " | Edit Data";
        $response['route'] = $this->route;
        $response['data'] = $this->find($id);
        return $response;
    }

    public function insertUpdateDetail(array $details, int $destinasi_id)
    {
        if (empty($details)) {
            return false;
        }

        $deleteDestinasiDetail = DestinasiDetail::where('m_destination_id', $destinasi_id)->forcedelete(); 
        foreach ($details as $val) {
            $val['m_destination_id'] = $destinasi_id;
            $val['id'] = isset($vals['id']) ? $vals['id'] : null;
            $this->destinasiDetail->store($val);
        }
    }
}
