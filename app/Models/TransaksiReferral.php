<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiReferral extends Model
{
    use HasFactory;

    protected $table = 'transaksi_referral';

    protected $fillable = [
        'id',
        'id_member',
        'id_ticket',
        'reward',
        'reff_pengunjung',
        'total_reward',
        'kode_reff',
        'tipe',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'is_deleted',
    ];

    protected $attributes = [
        'created_by' => 1,
        'updated_by' => 1,
    ];
}
