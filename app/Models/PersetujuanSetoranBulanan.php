<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersetujuanSetoranBulanan extends Model
{
    use HasFactory;
    use SoftDeletes; // Use SoftDeletes library
    public $timestamps = true;
    protected $fillable = [
        'tanggal_mulai',
        'tanggal_selesai',
        'total_pendapatan',
        'keterangan',
        'foto',
        'status',
        'kode',
        'm_user_pengajuan_id',
        'm_user_approval_id',
        'm_user_approval_nama',
        'approved_at',
        'total_perforasi',
        'total_asuransi',
        'total_biaya_cetak',
        'total_pengunjung',
        'total_pendapatan_bersih',
        'total_profit_perhutani',
        'total_profit_idn',
        'm_destination_id',
        'total_tiket',
        'perforasi',
        'asuransi',
        'biaya_cetak',
        'persen_profit_perhutani',
        'persen_profit_idn',



    ];
    protected $table = 't_approval_setoran_bulanan';

    public $title = 'Persetujuan Setoran Bulanan';
    public $route = 'persetujuan_setoran_bulanan';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;
    private $destinasiDetail;

    public function __construct()
    {

    }

    public function drop(int $id)
    {
       
        return $this->find($id)->delete();
    }

    public function edit(array $payload, int $id)
    {
       
        return $this->find($id)->update($payload);
    }

    public function getAll(array $filter, int $page = null, string $sort = '')
    {
        // query
        $model = $this->selectRaw($this->table.'.*, pengaju.name as nama_pengaju, m_destination.nama as destination_name')
            ->whereNull($this->table.'.deleted_at')
            ->join('users as pengaju', $this->table.'.m_user_pengajuan_id', '=', 'pengaju.id')
            ->leftJoin('m_destination', $this->table.'.m_destination_id', '=', 'm_destination.id')
            ->orderBy( $this->table.'.id', 'DESC')
            ;
        if (!empty($filter['kode'])) {
            $model->where($this->table.'.kode', 'LIKE','%'.$filter['kode'].'%');
        }
        // pagination
        $this->pagination = ceil($model->count() /$this->limit);
        if ($this->pagination > 1) {
            if (!empty($page) ) {
                $this->page = $page;
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }

        // get index
        $model = $model->get();
      
        $response = [
            'filter' => (object)$filter,
            'data' => $model,
            'title' => $this->title,
            'page' => $this->page,
            'pagination' => $this->pagination,
            'route' => $this->route,

        ];
        return $response;
    }

    public function getById(int $id)
    {
        return $this->find($id);
    }

    public function store(array $payload)
    {       
        $generateKode  = generateKode($this->table);
        if($generateKode['status']){
            $payload['kode'] = $generateKode['data'];
        }
        $save = $this->create($payload);
        return $save;
    }
    public function tambah(){
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;
        $response['data'] = getFieldTable($this->table);

        return $response;
    }
    public function getData($id, $type = null){
        if($type == null) {
            $type = 'Edit Data';
        }
        $response['route'] = $this->route;
        $response['data'] = $this->find($id);
        $response['title'] = $this->title . " | ".$type." | ". $response['data']->kode;
        return $response;
    }
    
}
