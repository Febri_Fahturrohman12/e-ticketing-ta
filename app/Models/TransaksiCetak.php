<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiCetak extends Model
{
    use HasFactory;

    protected $table = 't_cetak';

    protected $fillable = [
        'id',
        'kode',
        'm_destination_id',
        'm_destination_detail_id',
        'jumlah_pengunjung',
        'total_harga',
        'no_telepon',
        'uniq_ticket',
        'kd_referral',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'is_deleted',
    ];

    protected $attributes = [
        'created_by' => 1,
        'updated_by' => 1,
    ];
}
