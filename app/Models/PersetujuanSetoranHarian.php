<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersetujuanSetoranHarian extends Model
{
    use HasFactory;
    use SoftDeletes; // Use SoftDeletes library
    public $timestamps = true;
    protected $fillable = [
        'tanggal',
        'total_pendapatan',
        'keterangan',
        'foto',
        'status',
        'kode',
        'm_user_pengajuan_id',
        'm_user_approval_id',
        'm_user_approval_nama',
        'approved_at',
        'tanggal_mulai',
        'tanggal_selesai',
        'm_destination_id'

    ];
    protected $table = 't_approval_setoran_harian';

    public $title = 'Persetujuan Setoran Harian';
    public $route = 'persetujuan_setoran_harian';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;
    private $destinasiDetail;

    public function __construct()
    {

    }

    public function drop(int $id)
    {
        return $this->find($id)->delete();
    }

    public function edit(array $payload, int $id)
    {
        if(isset($payload['total_pendapatan'])){
            $payload['total_pendapatan'] = str_replace(".", "", $payload['total_pendapatan']);
        }
        
        return $this->find($id)->update($payload);
    }

    public function getAll(array $filter, int $page = null, string $sort = '')
    {
        // query
        $model = $this->selectRaw($this->table.'.*, pengaju.name as nama_pengaju, m_destination.nama as destination_name')
            ->join('users as pengaju', $this->table.'.m_user_pengajuan_id', '=', 'pengaju.id')
            ->leftJoin('m_destination', $this->table.'.m_destination_id', '=', 'm_destination.id')
            ->whereNull($this->table.'.deleted_at')
            ->orderBy( $this->table.'.id', 'DESC')
            ;
        if (!empty($filter['kode'])) {
            $model->where($this->table.'.kode', 'LIKE','%'.$filter['kode'].'%');
        }
        // pagination
        $this->pagination = ceil($model->count() /$this->limit);
        if ($this->pagination > 1) {
            if (!empty($page) ) {
                $this->page = $page;
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }

        // get index
        $model = $model->get();
        
        $response = [
            'filter' => (object)$filter,
            'data' => $model,
            'title' => $this->title,
            'page' => $this->page,
            'pagination' => $this->pagination,
            'route' => $this->route,

        ];
        return $response;
    }

    public function getById(int $id)
    {
        return $this->find($id);
    }

    public function store(array $payload)
    {       
        $generateKode  = generateKode($this->table);
        if($generateKode['status']){
            $payload['kode'] = $generateKode['data'];
        }

        $save = $this->create($payload);
       
        return $save;
    }
    public function tambah(){
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;
        $response['data'] = getFieldTable($this->table);

        return $response;
    }
    public function getData($id, $type = null){
        if($type == null) {
            $type = 'Edit Data';
        }
        $response['data'] = $this->find($id);
        $response['title'] = $this->title . " | ".$type." | ". $response['data']->kode;
        $response['route'] = $this->route;
        return $response;
    }
}
