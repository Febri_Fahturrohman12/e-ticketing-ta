<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLog extends Model
{
    use HasFactory;
    use SoftDeletes; // Use SoftDeletes library
    public $timestamps = true;
    protected $fillable = [
        'ip_address',
        'users_id',
        'users_name',
        'reff_type',
        'reff_id',
        'aktivitas',

    ];
    protected $table = 'user_log';

    public $title = 'Aktivitas Pengguna';
    public $route = 'laporan-user-log';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;
    private $destinasiDetail;

    public function __construct()
    {

    }

    public function drop(int $id)
    {
        return $this->find($id)->delete();
    }


    public function getAll(array $filter, int $page = null, string $sort = '')
    {
        // query
        $model = $this->selectRaw($this->table.'.*')
            ->whereNull($this->table.'.deleted_at')
            ->orderBy( $this->table.'.id', 'DESC')
            ;
        if (!empty($filter['nama'])) {
            $model->where($this->table.'.users_name', 'LIKE','%'.$filter['nama'].'%');
        }
        if (!empty($filter['aktivitas'])) {
            $model->where($this->table.'.aktivitas', 'LIKE','%'.$filter['aktivitas'].'%');
        }
        // pagination
        $this->pagination = ceil($model->count() /$this->limit);
        if ($this->pagination > 1) {
            if (!empty($page) ) {
                $this->page = $page;
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }

        // get index
        $model = $model->get();
      
        $response = [
            'filter' => (object)$filter,
            'data' => $model,
            'title' => $this->title,
            'page' => $this->page,
            'pagination' => $this->pagination,
            'route' => $this->route,

        ];
        return $response;
    }

    public function store(array $payload)
    {       
        $save = $this->create($payload);
        return $save;
    }
   
}
