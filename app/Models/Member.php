<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Repository\CrudInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model implements CrudInterface{

    use HasFactory;
    use SoftDeletes; // Use SoftDeletes library
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'foto',
        'no_hp',
        'no_ktp',
        'alamat',
        'tipe_member',
        'kode_referral',
        'reward_referral'
    ];
    protected $table = 'm_member';

    public $title = 'Member';
    public $route = 'member';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;


    public function drop(int $id)
    {   
        return $this->find($id)->delete();
    }

    public function edit(array $payload, int $id)
    {
        return $this->find($id)->update($payload);
    }
    public function getAll(array $filter, int $page = null, string $sort = '')
    {
        // query
        $model = $this->selectRaw('m_member.*')
            ->whereNull('m_member.deleted_at')
            ->orderBy('m_member.id', 'ASC');

        if (!empty($filter['name'])) {
            $model->where($this->table.'.name', 'LIKE','%'.$filter['name'].'%');
        }
        if (!empty($filter['no_hp'])) {
            $model->where($this->table.'.no_hp', 'LIKE','%'.$filter['no_hp'].'%');
        }
        if (!empty($filter['no_ktp'])) {
            $model->where($this->table.'.no_ktp', 'LIKE','%'.$filter['no_ktp'].'%');
        }
        if (!empty($filter['alamat'])) {
            $model->where($this->table.'.alamat', 'LIKE','%'.$filter['alamat'].'%');
        }
        // pagination
        $this->pagination = ceil($model->count() /$this->limit);
        if ($this->pagination > 1) {
            if (!empty($page) ) {
                $this->page = $page;
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }
        
        // $response['filter'] = (object) $filter;

        // get index
        $model = $model->get();

        // GET SISA SALDO REFERRAL
        $saldoref = TransaksiReferral::select('transaksi_referral.*')
            ->where('transaksi_referral.is_deleted', '=', 0)
            ->orderBy('transaksi_referral.id', 'ASC');
        $saldo = $saldoref->get();

        $datasaldo = [];
        foreach ($saldo as $key => $value) {
            $datasaldo[$value->id_member][] = $value;
        }
        // Kalkulasi Saldo
        $arr_saldo = [];
        foreach ($datasaldo as $key => $value) {
            $nominal = 0;
            $nominalterambil = 0;
            foreach ($value as $keys => $vals) {
                if($vals->tipe == 1){
                    $nominal += $vals->total_reward;
                }

                if($vals->tipe == 2){
                    $nominalterambil += $vals->total_reward;
                }
                
                $arr_saldo[$key]['pendapatan'] = $nominal;
                $arr_saldo[$key]['potongan'] = $nominalterambil; 
            }
        }
        
        foreach ($model as $key => $value) {
            $value->saldotersedia = isset($arr_saldo[$value->id]['pendapatan']) ? $arr_saldo[$value->id]['pendapatan'] : 0;
            $value->saldoterambil = isset($arr_saldo[$value->id]['potongan']) ? $arr_saldo[$value->id]['potongan'] : 0;
        }
        
        $response = [
            'filter' => (object)$filter,
            'data' => $model,
            'title' => $this->title,
            'page' => $this->page,
            'pagination' => $this->pagination,
            'route' => $this->route,

        ];
        return $response;
    }
    public function getById(int $id)
    {
        return $this->find($id);
    }

    public function store(array $payload)
    {   
        $save = $this->create($payload);
       
        return $save;
    }
    public function tambah(){
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;
        $response['data'] = getFieldTable($this->table);

        return $response;
    }
    public function getData($id){
        $response['title'] = $this->title . " | Edit Data";
        $response['route'] = $this->route;
        $data = $this->find($id);
        $response['data'] = $data;
        return $response;
    }


}
