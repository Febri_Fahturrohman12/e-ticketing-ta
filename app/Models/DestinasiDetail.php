<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DestinasiDetail extends Model
{
    use HasFactory;
    use SoftDeletes; // Use SoftDeletes library

    protected $table = 'm_destination_detail';
    protected $fillable = [
        'nama',
        'harga',
        'm_destination_id'
    ];
    public $timestamps = true;


    public function store(array $payload)
    {
        return $this->create($payload);
    }
    public function getAll(array $filter)
    {
        $destinasi_details = $this->query();

        if (!empty($filter['m_destination_id'])) {
            $destinasi_details->where('m_destination_id', '=',  $filter['m_destination_id'] );
        }

        return $destinasi_details->get();
    }
    public function dropByDestinasiId(int $destinasi_id)
    {
        return $this->where('m_destination_id', $destinasi_id)->delete();
    }
}
