<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPemakaian extends Model
{
    use HasFactory;

    protected $table = 't_pemakaian';

    protected $fillable = [
        'customer_id',
        'biaya_id',
        'biaya_air',
        'biaya_alat',
        'pemakaian',
        'total',
        'status_pembayaran',
        'tgl_pembayaran',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    protected $attributes = [
        'created_by' => 1,
        'updated_by' => 1,
    ];
}
