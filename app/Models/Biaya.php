<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Biaya extends Model
{
    use HasFactory;

    protected $table = 'm_biaya';

    protected $fillable = [
        'tanggal',
        'biaya_air',
        'biaya_alat',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    protected $attributes = [
        'created_by' => 1,
        'updated_by' => 1,
    ];
}
