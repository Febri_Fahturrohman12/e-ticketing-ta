<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\TransaksiCetak;
use App\Models\TransaksiPemakaian;
use App\Models\UserLog;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $userLog;
    public $route = 'dashboard';

    public function __construct()
    {
        $this->userLog = new UserLog();
    }
    public function index(Request $request)
    {
        $response['customer'] = Customer::count();
        $filter = $request->all();

        $filter['tanggal_mulai'] = isset($filter['tanggal_mulai']) ? $filter['tanggal_mulai'] : date('Y') . '-01-01';
        $filter['tanggal_selesai'] =  isset($filter['tanggal_selesai']) ? $filter['tanggal_selesai'] : date('Y') . '-12-31';


        $monthStart = date('m', strtotime($filter['tanggal_mulai']));
        $monthEnd = date('m', strtotime($filter['tanggal_selesai']));
        $yearStart = date('Y', strtotime($filter['tanggal_mulai']));
        $yearEnd = date('Y', strtotime($filter['tanggal_selesai']));

        if ($yearEnd != $yearStart) {
            $filter['tanggal_mulai'] = date('Y') . '-01-01';
            $filter['tanggal_selesai'] =  date('Y') . '-12-31';
            $notification = array(
                'message' => 'Mohon pilih range tanggal ditahun yang sama',
                'alert-type' => 'error',
            );
            return redirect()->route($this->route)->with($notification);
        }
        $dataPerbulan = [];

        $arrMonth = [];
        for ($i = intval($monthStart); $i <= intval($monthEnd); $i++) {
            $arrMonth[] = (getMonthName($i));
            $dataPerbulan[$i] = [
                'monthName' => getMonthName($i),
                'monthDate' => $i,
                'total_pendapatan' => 0,
                'total_pengunjung' => 0
            ];
        }

        $getUserLog = $this->userLog->getAll($filter, $request->page ?? 1, $request->sort ?? '');

        $response['user_log'] = $getUserLog['data'];
        $response['page'] = isset($request->page) ? $request->page : 1;
        $response['pagination'] =  $getUserLog['pagination'];
        $response['route'] = $this->route;

        $response['total_pengunjung'] = 0;
        $response['total_pendapatan'] = 0;


        $getTiket = TransaksiCetak::selectRaw("t_cetak.*, DATE_FORMAT(created_at, '%Y-%m') AS new_date, year(created_at) year, month(created_at) month")
            ->where('t_cetak.is_deleted', '=', 0)
            ->where('created_at', '>=',  $filter['tanggal_mulai'])
            ->where('created_at', '<=',  $filter['tanggal_selesai'])
            ->get();
        foreach ($getTiket as $key => $value) {
            $dataPerbulan[$value->month]['total_pendapatan'] +=  $value->total_harga;
            $dataPerbulan[$value->month]['total_pengunjung'] +=  $value->jumlah_pengunjung;
            $response['total_pendapatan'] += $value->total_harga;
            $response['total_pengunjung'] +=  $value->jumlah_pengunjung;
            // $arrPerbulan[$value->    ]
        }
        $arrData = [];
        foreach ($dataPerbulan as $key => $value) {
            $arrData[] = $value['total_pengunjung'];
        }
        $response['chart'] = [
            'month' => json_encode($arrMonth),
            'data' => json_encode($arrData),
        ];

        $response['filter'] = (object)[
            'tanggal_mulai' => $filter['tanggal_mulai'],
            'tanggal_selesai' => $filter['tanggal_selesai'],
        ];
        // dd($response);
        return view('admin.index', $response);
    }
}
