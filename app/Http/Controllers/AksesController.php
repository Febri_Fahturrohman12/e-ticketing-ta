<?php

namespace App\Http\Controllers;

use App\Models\Akses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AksesController extends Controller
{
    public $title = 'Akses';
    public $route = 'akses';
    public $table = 'm_akses';
    public $view = 'master.akses';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;

    public $akses = [
        'master' => [
            'nama' => 'Master',
            'detail' => [
                'akses' => 'Akses',
                'user' => 'User',
                'member' => 'Member',
                'destinasi' => 'Destinasi',
                'setting_persetujuan' => 'Setting Persetujuan',
            ],
        ],
        'transaksi' => [
            'nama' => 'Transaksi',
            'detail' => [
                'ticketing' => 'Ticketing',
                'persetujuan_setoran_harian' => 'Setoran Harian',
                'persetujuan_setoran_bulanan' => 'Setoran Bulanan'
            ],
        ],
        'laporan' => [
            'nama' => 'Laporan',
            'detail' => [
                'ticketing' => 'Ticketing',
                'user_log' => 'Aktivitas Pengguna',
                'keuangan' => 'Keuangan',
            ],
        ],
    ];

    public function index(Request $request)
    {

        // query
        $model = Akses::select('*');
        $filter = [];
        if (!empty($request->input('nama'))) {
            $model->where($this->table.'.nama', 'LIKE','%'.$request->input('nama').'%');
            $filter['nama'] = $request->input('nama');
        }
        // pagination
        $this->pagination = ceil($model->count() / $this->limit);
        if ($this->pagination > 1) {
            if (!empty($request->input('page'))) {
                $this->page = $request->input('page');
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }
       

        // get index
        $model = $model->get();
        
        $response['data'] = $model;
        $response['title'] = $this->title;
        
        $response['page'] = $this->page;
        $response['pagination'] = $this->pagination;
        $response['route'] = $this->route;
        $response['filter'] = (object) $filter;
        
        return view($this->view . '.index', $response);
    }

    public function create()
    {
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;

        $response['data'] = getFieldTable($this->table);
        $response['data']->akses = [];

        $response['akses'] = $this->akses;

        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        $model = Akses::where("id", $id)->first();
        $model->akses = json_decode($model->akses);

        $response['data'] = $model;
        $response['title'] = $this->title . " | Edit Data";
        $response['route'] = $this->route;

        $response['akses'] = $this->akses;
        // print_die($response);

        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => ['required'],
        ]);

        $data['nama'] = $request->post('nama');
        $data['akses'] = [];
        foreach ($this->akses as $key => $value) {
            foreach ($value['detail'] as $k => $v) {
                if (!empty($request->post($key . "_" . $k))) {
                    $data['akses'][] = $key . "_" . $k;
                }
            }
        }
        $data['akses'] = json_encode($data['akses']);

        if (!empty($request->post('id'))) {
            Akses::where('id', $request->post('id'))->update($data);

            // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $request->post('id'),
                'aktivitas' => 'Mengubah Hak Akses dengan nama '. $request->post('nama')
            ];
            saveUserLog($paramUserLog);

            $message = 'Data berhasil diupdate';
            if(session()->get('akses_id') == $request->post('id')){
                $getAkses = Akses::find($request->post('id'));
                $request->session()->put('akses', json_decode($getAkses->akses));
                // dd($request->session()->get('akses'));
            }
        } else {
            Akses::create($data);

            // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $request->post('id'),
                'aktivitas' => 'Menambah Hak Akses dengan nama '. $request->post('nama')
            ];
            saveUserLog($paramUserLog);
            
            $message = 'Data berhasil disimpan';
        }

        $notification = array(
            'message' => $message,
            'alert-type' => 'success',
        );

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            Akses::where('id', $id)->delete();
            $notification = array(
                'message' => 'Data berhasil dihapus',
                'alert-type' => 'info',
            );
        }

        return redirect()->route($this->route . '.index')->with($notification);
    }
}
