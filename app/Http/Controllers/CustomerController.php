<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public $title = 'Customer';
    public $route = 'customer';
    public $table = 'm_customer';
    public $view = 'master.customer';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;

    public function index(Request $request)
    {

        // query
        $model = Customer::select('*')->orderBy('m_customer.nama', 'ASC');

        // pagination
        $this->pagination = ceil($model->count() / $this->limit);
        if ($this->pagination > 1) {
            if (!empty($request->input('page'))) {
                $this->page = $request->input('page');
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }

        // get index
        $model = $model->get();

        $response['data'] = $model;
        $response['title'] = $this->title;

        $response['page'] = $this->page;
        $response['pagination'] = $this->pagination;
        $response['route'] = $this->route;

        return view($this->view . '.index', $response);
    }

    public function create()
    {
        $response['data'] = getFieldTable($this->table);
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;

        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        $model = Customer::where("id", $id)->first();
        $response['data'] = $model;
        $response['title'] = $this->title . " | Edit Data";
        $response['route'] = $this->route;

        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => ['required'],
            'nomer' => ['required'],
            'telp' => ['required'],
            'alamat' => ['required'],
        ]);

        $data['nama'] = $request->post('nama');
        $data['nomer'] = $request->post('nomer');
        $data['telp'] = $request->post('telp');
        $data['alamat'] = $request->post('alamat');
        if (!empty($request->post('id'))) {
            Customer::where('id', $request->post('id'))->update($data);
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );
        } else {
            Customer::create($data);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );
        }

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            Customer::where('id', $id)->delete();
        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );

        return redirect()->route($this->route . '.index')->with($notification);
    }
}
