<?php

namespace App\Http\Controllers;

use App\Models\Member;
use App\Models\TransaksiCetak;
use Illuminate\Http\Request;
use App\Models\TransaksiReferral;

class TransaksiReferralController extends Controller
{
    public $title = 'Referral';
    public $route = 'transaksi-referral';
    public $table = 'transaksi_referral';
    public $view  = 'transaksi.referral';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // query
        $model = TransaksiReferral::select('transaksi_referral.*', 'm_member.name as nm_member', 't_cetak.kode as kd_tiket')
            ->join('m_member', 'm_member.id', '=', 'transaksi_referral.id_member')
            ->join('t_cetak', 't_cetak.id', '=', 'transaksi_referral.id_ticket')
            ->where('transaksi_referral.is_deleted', '=', 0)
            ;
        // filter
        $filter = [];
        if (!empty($request->input('kode'))) {
            $model->where('transaksi_referral.kode_reff', 'LIKE','%'.$request->input('kode').'%');
            $filter['kode'] = $request->input('kode');
        }

        // pagination
        $this->pagination = ceil($model->count() / $this->limit);
        // if ($this->pagination > 1) {
        //     if (!empty($request->input('page'))) {
        //         $this->page = $request->input('page');
        //     }
        //     $this->offset = ($this->page - 1) * $this->limit;
        //     $model->offset($this->offset)->limit($this->limit);
        // }

       
        $response['filter'] = (object) $filter;

        // get index
        $model = $model->get();
        foreach ($model as $key => $value) {
            $value->tanggal = date('d F Y', strtotime($value->created_at));
        }

        $response['data'] = $model;
        $response['title'] = $this->title;

        $response['page'] = $this->page;
        $response['pagination'] = $this->pagination;
        $response['route'] = $this->route;
        return view($this->view . '.index', $response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['reff_pengunjung'] = $request->post('reff_pengunjung');
        $data['total_reward'] = $request->post('total_reward');
        
        $update = TransaksiReferral::where('id', $request->post('id'))->update($data);

        $notification = array(
            'message' => 'Data berhasil diupdate',
            'alert-type' => 'success',
        );

        return redirect()->route($this->route . '.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = TransaksiReferral::where("id", $id)->first();
        $modelticket = TransaksiCetak::where("id", $model->id_ticket)->first();
        $modelmember = Member::where("id", $model->id_member)->first();

        

        $data = [];
        $data['id'] = $id;
        $data['id_member'] = $modelmember->id;
        $data['id_ticket'] = $modelticket->id;
        $data['kode_ticket'] = $modelticket->kode;
        $data['nama'] = $modelmember->name;
        $data['reward'] = $model->reward;
        $data['reff_pengunjung'] = $model->reff_pengunjung;
        $data['total_reward'] = $model->total_reward;
        $data['reward'] = $model->reward;
        $data['kode_reff'] = $model->kode_reff;
        
        $model->nama = $modelmember->name;
        $model->kode_ticket = $modelticket->kode; 
        $response['data'] = $model;
        $response['title'] = $this->title . " | Edit Data ". $model->kode;
        $response['route'] = $this->route;
        
        return view($this->view . '.form', $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            $data['is_deleted'] = 1;
            TransaksiReferral::where('id', $id)->update($data);

            // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $id,
                'aktivitas' => 'Menghapus data referral'
            ];
            saveUserLog($paramUserLog);
        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );

        return redirect()->route($this->route . '.index')->with($notification);
    }
}
