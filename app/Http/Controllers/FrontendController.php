<?php

namespace App\Http\Controllers;

use App\Models\TransaksiPemakaian;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class FrontendController extends Controller
{
    public function cekTagihan(Request $request)
    {
        $response = [];
        $response['nomer'] = null;
        $response['total'] = null;
        $response['listPemakaian'] = [];
        if (!empty($request->input('nomer'))) {
            $model = TransaksiPemakaian::select('t_pemakaian.*', 'm_customer.nama as nama_customer', 'm_customer.nomer as nomer_pam', 'm_biaya.tanggal')
                ->join('m_customer', 'm_customer.id', '=', 't_pemakaian.customer_id')
                ->join('m_biaya', 'm_biaya.id', '=', 't_pemakaian.biaya_id')
                ->where('m_customer.nomer', $request->input('nomer'))
                ->where('status_pembayaran', '=', 'pending')
                ->get();

            $response['nomer'] = $request->input('nomer');
            if (count($model) == 0) {
                Alert::error('Opsss', 'Tagihan tidak ditemukan');
                // $notification = array(
                //     'message' => 'Tagihan tidak ditemukan',
                //     'alert-type' => 'info',
                // );
                return view('frontend.frontend_checkpayments', $response);
            }

            $total = 0;
            foreach ($model as $key => $value) {
                $value->tanggal_formatted = date('F Y', strtotime($value->tanggal));
                $total += $value->total;
                $value->total = number_format($value->total);
            }

            $response['total'] = number_format($total);
            $response['listPemakaian'] = $model;
        }

        return view('frontend.frontend_checkpayments', $response);

    }

    public function bayarTagihan($nomer)
    {
        $response = [];
        $response['nomer'] = null;
        $response['total'] = null;
        $response['listPemakaian'] = [];
        if (!empty($nomer)) {
            $response['nomer'] = $nomer;

            $data['status_pembayaran'] = 'berhasil';
            $data['tgl_pembayaran'] = date('Y-m-d H:i:s');
            $model = TransaksiPemakaian::select('t_pemakaian.*', 'm_customer.nama as nama_customer', 'm_customer.nomer as nomer_pam', 'm_biaya.tanggal')
                ->join('m_customer', 'm_customer.id', '=', 't_pemakaian.customer_id')
                ->join('m_biaya', 'm_biaya.id', '=', 't_pemakaian.biaya_id')
                ->where('m_customer.nomer', $nomer)
                ->where('status_pembayaran', '=', 'pending')
                ->get();

            $total = 0;
            foreach ($model as $key => $value) {
                TransaksiPemakaian::where('id', $value->id)->update($data);
            }
        }

        // $notification = array(
        //     'message' => 'Pembayaran berhasil',
        //     'alert-type' => 'success',
        // );
        Alert::success('Berhasil', 'Tagihan anda berhasil dibayar');
        return redirect()->route('cektagihan');

    }
}
