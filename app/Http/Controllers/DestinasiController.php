<?php

namespace App\Http\Controllers;

use App\Models\Destinasi;
use App\Models\DestinasiDetail;
use Illuminate\Http\Request;

class DestinasiController extends Controller
{
    private $destinasi;
    private $destinasiDetail;

    public $view = 'master.destinasi';
    public $route = 'destinasi';
    protected $table = 'm_destination';

    public function __construct()
    {
        $this->destinasi = new Destinasi();
        $this->destinasiDetail = new DestinasiDetail();   
    }

    /**
     * Mengambil data destinasi dilengkapi dengan pagination
     *
     * @author Wahyu Agung <wahyuagung26@email.com>
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $response = $this->destinasi->getAll($filter, $request->page ?? 1, $request->sort ?? '');
        return view($this->view . '.index', $response);
    }

    public function create()
    {
       
        $response = $this->destinasi->tambah();
        // $response['details'][0 = {[
            
        //         'nama' => null,
        //         'harga' => null,
        //         'id' => null
            

        // ]);

        $response['details'] = [];
        // echo json_encode($response); die;

        // dd($response);
        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        
        $response = $this->destinasi->getData($id);
        $paramsDetail['m_destination_id'] = $id;
        $response['details'] = $this->destinasiDetail->getAll($paramsDetail);
        // dd($response);
        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'nama' => ['required'],
            'harga' => ['required'],
            'details.*.nama' => 'required',
            'details.*.harga' => 'required',
        ]);
        

        

        // print_die($data);
        $id = null;
        if (!empty($request->post('id'))) {
            $model = $this->destinasi->edit($request->all(), $request->post('id'));
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );
            $id = $request->post('id');
              // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $id,
                'aktivitas' => 'Mengubah data Destinasi dengan Nama '. $request->nama
            ];
            saveUserLog($paramUserLog);

        } else {
            $model = $this->destinasi->store($request->all());
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );
            $id = $model->id;

             // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $id,
                'aktivitas' => 'Menambah data Destinasi dengan Nama ' . $request->nama
            ];
            saveUserLog($paramUserLog);

        }
        if($id != null){
            $insertDetail = $this->destinasi->insertUpdateDetail($request['details'] ?? [], $id);
        }

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $this->destinasi->destroy($id);
            $this->destinasiDetail->dropByDestinasiId($id);

        }
          // USER LOG
          $paramUserLog = [
            'reff_type' => $this->table,
            'reff_id' => $id,
            'aktivitas' => 'Menghapus data Destinasi'
        ];
        saveUserLog($paramUserLog);


        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );
        return redirect()->route($this->route . '.index')->with($notification);
    }
}
