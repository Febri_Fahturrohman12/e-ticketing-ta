<?php

namespace App\Http\Controllers;

use App\Models\Member;
use App\Models\Akses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{

    private $member;

    public $view = 'master.member';
    public $route = 'member';
    protected $table = 'm_member';

    public function __construct()
    {
        $this->member = new Member();
    }

    /**
     * Mengambil data member dilengkapi dengan pagination
     *
     * @author Wahyu Agung <wahyuagung26@email.com>
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $response = $this->member->getAll($filter, $request->page ?? 1, $request->sort ?? '');
        return view($this->view . '.index', $response);
    }

    public function create()
    {
       
        $response = $this->member->tambah();
        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        
        $response = $this->member->getData($id);
        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required'],
        ]);
    
        $id = null;
        $data = $request->all();
        if($request->file('foto_url')){
            $path = 'upload/member';
            $file = $request->file('foto_url');
            $filename = strtotime(date('YmdHis')).$file->getClientOriginalName();
            $file->move(public_path($path),$filename);
            $data['foto'] = $path.'/'.$filename;
        }
        // dd($data);
        if (!empty($request->post('id'))) {
            $model = $this->member->edit($data, $request->post('id'));
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );
            $id = $request->post('id');
              // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $id,
                'aktivitas' => 'Mengubah data Member dengan Nama '. $request->name
            ];
            saveUserLog($paramUserLog);

        } else {
            $model = $this->member->store($data);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );
            $id = $model->id;

             // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $id,
                'aktivitas' => 'Menambah data Member dengan Nama ' . $request->name
            ];
            saveUserLog($paramUserLog);

        }

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $this->member->destroy($id);

        }
          // USER LOG
          $paramUserLog = [
            'reff_type' => $this->table,
            'reff_id' => $id,
            'aktivitas' => 'Menghapus data Member'
        ];
        saveUserLog($paramUserLog);


        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );
        return redirect()->route($this->route . '.index')->with($notification);
    }
}
