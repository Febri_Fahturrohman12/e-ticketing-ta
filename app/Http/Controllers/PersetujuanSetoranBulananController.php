<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PersetujuanSetoranBulanan;
use App\Models\SettingPersetujuan;
use App\Models\User;
use App\Models\Destinasi;
use App\Models\TransaksiCetak;
use Illuminate\Support\Facades\DB;


class PersetujuanSetoranBulananController extends Controller
{
    private $setoranBulanan;
    private $setting_persetujuan;
    protected $table = 't_approval_setoran_bulanan';


    public $view = 'transaksi.persetujuan_setoran_bulanan';
    public $route = 'persetujuan_setoran_bulanan';

    public function __construct()
    {
        $this->setoranBulanan = new PersetujuanSetoranBulanan();
        $this->setting_persetujuan = new SettingPersetujuan();

    }

    /**
     * Mengambil data destinasi dilengkapi dengan pagination
     *
     * @author Wahyu Agung <wahyuagung26@email.com>
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $response = $this->setoranBulanan->getAll($filter, $request->page ?? 1, $request->sort ?? '');
        return view($this->view . '.index', $response);
    }

    public function create()
    {

        $response = $this->setoranBulanan->tambah();
        $getPersetujuan = $this->setting_persetujuan->getAll();
        $m_user_approval_bulanan_id = isset($getPersetujuan->m_user_approval_bulanan_id) ? $getPersetujuan->m_user_approval_bulanan_id : null;
        $response['approval'] = $this->getDetailApproval($m_user_approval_bulanan_id);    
        $response['listDestinasi'] = Destinasi::all();
   
        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        $response = $this->setoranBulanan->getData($id);
        $response['approval'] = $this->getDetailApproval($response['data']->m_user_approval_id);
        $response['listDestinasi'] = Destinasi::all();

        return view($this->view . '.form', $response);
    }
    public function approval($id)
    {
        $response = $this->setoranBulanan->getData($id, 'Approve');
        $response['approval'] = $this->getDetailApproval($response['data']->m_user_approval_id);
        $response['listDestinasi'] = Destinasi::all();

        return view($this->view . '.approval', $response);
    }
    public function approvePengajuan(Request $request)
    {   
        $data['status'] = 'disetujui';
        $data['m_user_approval_nama'] = session()->get('user')->name;
        $data['approved_at'] = date('Y-m-d h:i:s');
        $model = $this->setoranBulanan->edit($data, $request->post('id'));

         // USER LOG
         $paramUserLog = [
            'reff_type' => $this->table,
            'reff_id' => $request->post('id'),
            'aktivitas' => 'Mengapprove data Setoran Bulanan dengan kode '. $request->post('kode')
        ];
        saveUserLog($paramUserLog);
        
        $notification = array(
            'message' => 'Data berhasil diupdate',
            'alert-type' => 'success',
        );
        return redirect()->route($this->route . '.index')->with($notification);

    }
    public function store(Request $request)
    {
        // dd($request->all());

        $validatedData = $request->validate([
            'tanggal_mulai' => ['required'],
            'tanggal_selesai' => ['required'],
            'total_pendapatan' => ['required'],
        ]);
        // $data['tanggal_mulai'] = $request->post('tanggal_mulai');
        // $data['tanggal_selesai'] = $request->post('tanggal_selesai');
        // $data['total_pendapatan'] = $request->post('total_pendapatan');
        // $data['keterangan'] = $request->post('keterangan');

        $data = $request->all();
        if($request->file('foto_url')){
            $path = 'upload/setoran_bulanan';
            $file = $request->file('foto_url');
            $filename = strtotime(date('YmdHis')).$file->getClientOriginalName();
            $file->move(public_path($path),$filename);
            $data['foto'] = $path.'/'.$filename;
        }

        if (!empty($request->post('id'))) {
            $model = $this->setoranBulanan->edit($data, $request->post('id'));
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );

            if(isset($payload['total_pendapatan'])){
                $payload['total_pendapatan'] = str_replace(".", "", $payload['total_pendapatan']);
            }

             // USER LOG
             $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $request->post('id'),
                'aktivitas' => 'Mengubah data Setoran Bulanan dengan kode '. $request->post('kode')
            ];
            saveUserLog($paramUserLog);

        } else {
            $data['m_user_approval_id'] = $request->post('m_user_approval_id');
            if(isset($data['m_user_approval_id'])){
                $data['status'] = 'menunggu';
            }else{
                $data['status'] = 'disetujui';
                $data['m_user_approval_nama'] = session()->get('user')->name;
            }
            $data['m_user_pengajuan_id'] =  session()->get('user')->id;

            $model = $this->setoranBulanan->store($data);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );

             // USER LOG
             $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $model->id,
                'aktivitas' => 'Mengubah data Setoran Bulanan dengan kode '.  $model->kode
            ];
            saveUserLog($paramUserLog);
        }
        

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $this->setoranBulanan->destroy($id);

        }
        // USER LOG
        $paramUserLog = [
            'reff_type' => $this->table,
            'reff_id' => $id,
            'aktivitas' => 'Menghapus data Setoran Bulanan'
        ];
        saveUserLog($paramUserLog);

        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );
       
        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function getDetailApproval($userId){
        $response = (object) [];

        if(isset($userId)){
            $response->m_user_approval_id =  $userId;
            $getUser = User::where('id', $userId)->with('akses')->first();
            $response->m_user_approval_nama =  $getUser->name;
            $response->m_user_approval_foto =  isset($getUser->foto) ? 'upload/admin_image'.$getUser->foto : null;
            $response->m_user_approval_akses_nama =  isset($getUser->akses) ? $getUser->akses->nama : '';
        }else{
            $response->m_user_approval_id =  null;
            $response->m_user_approval_nama =  null;
            $response->m_user_approval_foto =  null;
            $response->m_user_approval_akses_nama =  null;
        }
        return $response;
    }
    public function kalkulasiSetoran(Request $request)
    {
        $data = ['test'];
        $m_destinasi_id = $request->m_destinasi_id;
        $tanggal_mulai = $request->tanggal_mulai;
        $tanggal_selesai = $request->tanggal_selesai;
        
        $destinasi = new Destinasi();
        $getDestinasi = $destinasi->getById($m_destinasi_id);
        $perforasi = isset($getDestinasi->perforasi) ? $getDestinasi->perforasi : 0;
        $asuransi = isset($getDestinasi->asuransi) ? $getDestinasi->asuransi : 0;
        $biaya_cetak = isset($getDestinasi->biaya_cetak) ? $getDestinasi->biaya_cetak : 0;
        $persen_profit_perhutani = isset($getDestinasi->persen_profit_perhutani) ? $getDestinasi->persen_profit_perhutani : 0;
        $persen_profit_idn = isset($getDestinasi->persen_profit_idn) ? $getDestinasi->persen_profit_idn : 0;

        $getTiket = TransaksiCetak::select(DB::raw("SUM(t_cetak.total_harga) as total_pendapatan, COUNT(t_cetak.id) as total_tiket, SUM(t_cetak.jumlah_pengunjung) as total_pengunjung"))
        ->where('created_at', '>=', $tanggal_mulai)
        ->where('created_at', '<=', $tanggal_selesai)
        ->where('t_cetak.m_destination_id', '=', $m_destinasi_id)
        ->get();

        $total_pengunjung = isset($getTiket[0]->total_pengunjung) ? round($getTiket[0]->total_pengunjung) : 0;
        $total_pendapatan = isset($getTiket[0]->total_pengunjung) &&  isset( $getDestinasi->harga) ?round($getTiket[0]->total_pengunjung * $getDestinasi->harga) : 0;
        $total_tiket = isset($getTiket[0]->total_tiket) ? $getTiket[0]->total_tiket : 0;

        $total_perforasi = round(($perforasi / 100 ) * $total_pendapatan);
        $total_asuransi = round($asuransi * $total_tiket);
        $total_biaya_cetak = round($biaya_cetak * $total_tiket);

        $total_pendapatan_bersih = round($total_pendapatan - $total_perforasi - $total_asuransi - $total_biaya_cetak);
        
        $total_profit_perhutani = round(($persen_profit_perhutani / 100) * $total_pendapatan_bersih);
        $total_profit_idn = round(($persen_profit_idn / 100) * $total_pendapatan_bersih);

        $response = [
            'total_pendapatan' => $total_pendapatan,
            'total_tiket' => $total_tiket,
            'total_pengunjung' => $total_pengunjung,
            'total_perforasi' => $total_perforasi,
            'total_asuransi' => $total_asuransi,
            'total_biaya_cetak' => $total_biaya_cetak,
            'total_pendapatan_bersih' => $total_pendapatan_bersih,
            'total_profit_perhutani' => $total_profit_perhutani,
            'total_profit_idn' => $total_profit_idn,
            'perforasi' => $perforasi,
            'asuransi' => $asuransi,
            'biaya_cetak' => $biaya_cetak,
            'persen_profit_idn' => $persen_profit_idn,
            'persen_profit_perhutani' => $persen_profit_perhutani,
            'harga_tiket' => isset($getDestinasi->harga),
        ];


        return response()->json($response);
    }
}
