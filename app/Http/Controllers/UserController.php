<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Akses;
use App\Models\Destinasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public $title = 'User';
    public $route = 'user';
    public $table = 'users';
    public $view = 'master.user';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;

    public function index()
    {
        $id = Auth::user()->id;
        $adminData = User::find($id);

        return view('admin.admin_profile_view', compact('adminData'));
    }

    public function index_master(Request $request)
    {
        // query
        $model = User::select('users.*', 'm_akses.nama as nama_akses')
            ->leftJoin('m_akses', 'm_akses.id', '=', 'users.akses_id')
            ->orderBy('users.name', 'ASC');
        $filter = [];
        if (!empty($request->input('name'))) {
            $model->where($this->table.'.name', 'LIKE','%'.$request->input('name').'%');
            $filter['name'] = $request->input('name');
        }
        // pagination
        $this->pagination = ceil($model->count() / $this->limit);
        if ($this->pagination > 1) {
            if (!empty($request->input('page'))) {
                $this->page = $request->input('page');
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }
       

        // get index
        $model = $model->get();

        $response['data'] = $model;
        $response['title'] = $this->title;

        $response['page'] = $this->page;
        $response['pagination'] = $this->pagination;
        $response['route'] = $this->route;
        $response['filter'] = (object) $filter;
        return view($this->view . '.index', $response);
    }

    public function create()
    {
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;

        $response['data'] = getFieldTable($this->table);
        $response['listAkses'] = Akses::all();
        $response['listDestinasi'] = Destinasi::all()->whereNull('deleted_at');

        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        $response['data'] = User::where('id', $id)->first();
        
        $response['listAkses'] = Akses::all();

        $response['title'] = $this->title . " | Edit Data";
        $response['route'] = $this->route;
        // print_die($response);
        $response['listDestinasi'] = Destinasi::all()->whereNull('deleted_at');

        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {   
        
       
        $validatedData = $request->validate([
            'name' => ['required'],
            'akses_id' => ['required'],
            'username' => ['required']
        ]);

        $data['name'] = $request->post('name');
        $data['akses_id'] = $request->post('akses_id');
        $data['type'] = $request->post('type');
        $data['username'] =  isset($request->username) ? $request->username : '';
        

        // $data['email'] =  isset($request->email) ? $request->email : '';

        if(isset($request->password_new)){
            $data['password'] = Hash::make($request->password_new) ;
        }elseif(!isset($request->password)){
            $data['password'] = '';
        }
       
        if($data['type'] == 'operator'){
            $data['m_destination_id'] = $request->post('m_destination_id');
        }else{
            $data['m_destination_id'] = null;
        }
        // print_die($data);
        if (!empty($request->post('id'))) {
            User::where('id', $request->post('id'))->update($data);

            // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $request->post('id'),
                'aktivitas' => 'Mengubah user dengan nama '. $request->post('name')
            ];
            saveUserLog($paramUserLog);
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );
        } else {
            $data['email_verified_at'] = date('Y-m-d h:i:s');
            $getUser = User::where('username', '=', $data['username'])->first();
            if(!isset($getUser)){
                User::create($data);
                // USER LOG
                $paramUserLog = [
                    'reff_type' => $this->table,
                    'reff_id' => $request->post('id'),
                    'aktivitas' => 'Menambah user dengan nama '. $request->post('name')
                ];
                saveUserLog($paramUserLog);
                
                $notification = array(
                    'message' => 'Data berhasil disimpan',
                    'alert-type' => 'success',
                );
            }else{
                return back()->withErrors(['Username', 'sudah dipakai']);
            }
        }

        return redirect()->route($this->route . '.index_master')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            User::where('id', $id)->delete();
        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );
        return redirect()->route($this->route . '.index_master')->with($notification);
    }
}
