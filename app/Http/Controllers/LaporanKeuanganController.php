<?php

namespace App\Http\Controllers;

use App\Models\Biaya;
use App\Models\Customer;
use App\Models\Destinasi;
use App\Models\TransaksiCetak;
use App\Models\PersetujuanSetoranBulanan;
use Illuminate\Http\Request;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LaporanKeuanganExport;
use App\Models\SettingPersetujuan;

class LaporanKeuanganController extends Controller
{
    public $title = 'Laporan Keuangan';
    public $route = 'laporan-keuangan';
    public $view = 'laporan.keuangan';

    public function index(Request $request)
    {
        $filter = $request->all();

        $filter['tanggal_mulai'] = isset($filter['tanggal_mulai']) ? $filter['tanggal_mulai'] : date('Y') . '-01-01';
        $filter['tanggal_selesai'] =  isset($filter['tanggal_selesai']) ? $filter['tanggal_selesai'] : date('Y') . '-12-31';


        $monthStart = date('m', strtotime($filter['tanggal_mulai']));
        $monthEnd = date('m', strtotime($filter['tanggal_selesai']));
        $yearStart = date('Y', strtotime($filter['tanggal_mulai']));
        $yearEnd = date('Y', strtotime($filter['tanggal_selesai']));

        if ($yearEnd != $yearStart) {
            $filter['tanggal_mulai'] = date('Y') . '-01-01';
            $filter['tanggal_selesai'] =  date('Y') . '-12-31';
            $notification = array(
                'message' => 'Mohon pilih range tanggal ditahun yang sama',
                'alert-type' => 'error',
            );
            return redirect()->route($this->route)->with($notification);
        }

        // query
        $model = PersetujuanSetoranBulanan::select('t_approval_setoran_bulanan.*', 'm_destination.nama as nama_destinasi', 'm_destination.harga as harga_tiket')
            ->join('m_destination', 'm_destination.id', '=', 't_approval_setoran_bulanan.m_destination_id')
            ->whereNull('t_approval_setoran_bulanan.deleted_at')
            ->where('t_approval_setoran_bulanan.tanggal_mulai', '>=',  $filter['tanggal_mulai'])
            ->where('t_approval_setoran_bulanan.tanggal_selesai', '<=',  $filter['tanggal_selesai'])
            ->orderBy('t_approval_setoran_bulanan.tanggal_mulai', 'asc');

        if (!empty($request->input('destinasi_id'))) {
            $model->where('t_approval_setoran_bulanan.m_destination_id', $request->input('destinasi_id'));
            $filter['destinasi_id'] = $request->input('destinasi_id');
        }else {
            $filter['destinasi_id'] = "";
        }

        $response['filter'] = (object) $filter;

        // get index
        $model = $model->get();
      
        

        $totalAll = [
            'terjual' => 0,
            'parkir' => 0,
            'bruto' => 0,
            'asuransi' => 0,
            'porporasi' => 0,
            'cetak' => 0,
            'pendapatan_bersih' => 0,
            'lmdh' => 0,
            'pengelola' => 0,
            'pph' => 0,
            'pht' => 0,
            'setoran_kph' => 0
        ];
        foreach ($model as $key => $value) {
            $value->total_penjualan_tiket = isset($value->harga_tiket) && isset($value->total_pengunjung ) ?  $value->harga_tiket * $value->total_pengunjung :  0;
            $value->total_parkir =  isset($value->total_penjualan_tiket) && isset($value->total_pendapatan) ? $value->total_pendapatan - $value->total_penjualan_tiket: 0;
            $value->total_pph =  isset($value->total_profit_idn) || isset($value->total_profit_perhutani) ? ($value->total_profit_idn + $value->total_profit_perhutani) * 4/100 : 0;
            $value->total_setoran_kph = $value->total_asuransi + $value->total_perforasi + $value->total_biaya_cetak + $value->total_pph;
            $value->total_pht = $value->total_pendapatan_bersih - $value->total_profit_idn - $value->total_profit_perhutani;

            $totalAll['terjual'] += $value->total_pengunjung;
            $totalAll['parkir'] += $value->total_parkir;
            $totalAll['bruto'] += $value->total_pendapatan;
            $totalAll['asuransi'] += $value->total_asuransi;
            $totalAll['porporasi'] += $value->total_perforasi;
            $totalAll['cetak'] += $value->total_biaya_cetak;
            $totalAll['pendapatan_bersih'] += $value->total_pendapatan_bersih;
            $totalAll['lmdh'] += $value->total_profit_perhutani;
            $totalAll['pengelola'] += $value->total_profit_idn;
            $totalAll['pph'] += $value->total_pph;
            $totalAll['pht'] += $value->total_pht;
            $totalAll['setoran_kph'] += $value->total_setoran_kph;
            
        }
        // ej($model);
       
        // RESPONSE
        $response['filter'] = (object)[
            'tanggal_mulai' => $filter['tanggal_mulai'],
            'tanggal_selesai' => $filter['tanggal_selesai'],
            'destinasi_id' => $filter['destinasi_id'],
        ];
        $response['listDestinasi'] = Destinasi::all();
        $response['data'] = $model;
        $response['title'] = $this->title;
        $response['periode'] = date('d F Y', strtotime($filter['tanggal_mulai']))." - ".date("d F Y", strtotime($filter['tanggal_selesai']));
        $response['route'] = $this->route;
        $response['total'] = $totalAll;
        // ej($response);
        
        if($request->has('download'))
	    {   
            $settingPersetujuan =   SettingPersetujuan::select('m_setting_persetujuan.*','users.name as nama_penyetuju', 'users.type')
            ->join('users', 'm_setting_persetujuan.m_user_approval_bulanan_id', '=', 'users.id')
            ->first()
            ;
            $response['penyetuju'] = $settingPersetujuan;
    
            $nama_file = 'laporan-keuangan-'.$response['periode'].'.xlsx';
            return Excel::download(new LaporanKeuanganExport($response,'laporan.keuangan.excel-keuangan'),$nama_file);

	        // return $pdf->download('Laporan Ticketing-'.date('d F Y').'.pdf');
	    }

        return view($this->view . '.index', $response);
    }
}
