<?php

namespace App\Http\Controllers;

use App\Models\Biaya;
use App\Models\Customer;
use App\Models\Destinasi;
use App\Models\DestinasiDetail;
use App\Models\TransaksiCetak;
use App\Models\{Country, State, City, Member, TransaksiReferral};
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\Cast\Object_;
use RealRashid\SweetAlert\Facades\Alert;
use stdClass;

class TransaksiCetakController extends Controller
{
    public $title = 'Ticketing';
    public $route = 'transaksi-cetak';
    public $table = 't_cetak';
    public $view = 'transaksi.ticketing';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;

    public function index(Request $request)
    {

        // query
        $model = TransaksiCetak::select('t_cetak.*', 'm_destination.nama as nama_destinasi', 'm_destination.harga as harga_ticket', 'm_destination_detail.nama as nama_kendaraan', 'm_destination_detail.harga as biaya_kendaraan')
            ->join('m_destination', 'm_destination.id', '=', 't_cetak.m_destination_id')
            ->join('m_destination_detail', 'm_destination_detail.id', '=', 't_cetak.m_destination_detail_id')
            ->where('t_cetak.is_deleted', '=', 0)
            ->orderBy('t_cetak.id', 'DESC')
            ;
        // filter
        $filter = [];
        if (!empty($request->input('kode'))) {
            // $model->where('t_cetak.kode', 'LIKE','%'.$request->input('kode').'%');
            $filter['kode'] = $request->input('kode');
        }

        // pagination
        $this->pagination = ceil($model->count() / $this->limit);
        // if ($this->pagination > 1) {
        //     if (!empty($request->input('page'))) {
        //         $this->page = $request->input('page');
        //     }
        //     $this->offset = ($this->page - 1) * $this->limit;
        //     $model->offset($this->offset)->limit($this->limit);
        // }

       
        $response['filter'] = (object) $filter;

        // get index
        $model = $model->get();
        foreach ($model as $key => $value) {
            $value->tanggal = date('d F Y', strtotime($value->created_at));
        }

        $response['data'] = $model;
        $response['title'] = $this->title;

        $response['page'] = $this->page;
        $response['pagination'] = $this->pagination;
        $response['route'] = $this->route;
        return view($this->view . '.index', $response);
    }

    public function create(Request $request)
    {
        $response['data'] = getFieldTable($this->table);
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;
        
        $response['listDestinasi'] = Destinasi::all();
        $response['listDestinasiDetail'] = DestinasiDetail::all();

        $response['data']->pengunjung = 0;
        $response['data']->no_telepon = "";

        $response['countries'] = Destinasi::get(["nama", "id"]);
        return view($this->view . '.form', $response);
    }
    public function fetchState(Request $request)
    {
        $data['states'] = DestinasiDetail::where("m_destination_id",$request->country_id)->get(["nama", "id", "harga"]);
        return response()->json($data);
    }

    public function edit($id)
    {
        $model = TransaksiCetak::where("id", $id)->first();
        $response['data'] = $model;
        $response['title'] = $this->title . " | Edit Data ". $model->kode;
        $response['route'] = $this->route;

        $response['data']->pengunjung = $model->jumlah_pengunjung;
        $response['data']->no_telepon = $model->no_telepon;

        $response['listDestinasi'] = Destinasi::all();
        $response['listDestinasiDetail'] = DestinasiDetail::where('m_destination_id', $model->m_destination_id)->get();

        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'm_destination_id' => ['required'],
            'm_destination_detail_id' => ['required'],
            'jumlah_pengunjung' => ['required'],
        ]);

        $cekKode = TransaksiCetak::orderBy('id', 'desc')->limit(1)->get(); 
        if ($cekKode && count($cekKode) > 0) {
            $kode_terakhir = $cekKode[0]->kode;
        } else {
            $kode_terakhir = 0;
        }
        $tipe = 'TCK';
        $kode_item = (substr($kode_terakhir, -4) + 1);
        $kode = substr('0000'.$kode_item, strlen($kode_item));


        $tiket_price = Destinasi::find($request->post('m_destination_id'));
        $kendaraan_price = DestinasiDetail::find($request->post('m_destination_detail_id'));

        $data['m_destination_id'] = $request->post('m_destination_id');
        $data['m_destination_detail_id'] = $request->post('m_destination_detail_id');
        $data['jumlah_pengunjung'] = $request->post('jumlah_pengunjung');
        $jumlah_pengunjung = (int)$data['jumlah_pengunjung'];
        $data['total_harga'] = ($tiket_price->harga * $jumlah_pengunjung) + $kendaraan_price->harga;
        $data['no_telepon'] = $request->post('no_telepon');
        $data['uniq_ticket'] = strtotime("now").generateRandomString(8);
        $data['kd_referral'] = $request->post('kd_referral');
        
        if (!empty($request->post('id'))) {
            $update = TransaksiCetak::where('id', $request->post('id'))->update($data);

            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );

            $models = TransaksiCetak::find($request->post('id'));

            // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $request->post('id'),
                'aktivitas' => 'Mengubah tiket dengan kode '. $models->kode
            ];
            saveUserLog($paramUserLog);
        } else {
            $data['kode'] = $tipe.$kode;
             
            // VALIDASI REFERRAL CODE
            if(isset($data['kd_referral']) && !empty( $data['kd_referral']) &&  $data['kd_referral'] !== ""){
                $cekReferral = Member::where('kode_referral', $data['kd_referral'])->first();
                if($cekReferral){
                    $models = TransaksiCetak::create($data);

                    $data['kd_referral'] = $cekReferral->kode_referral;
                    $dataref['id_member'] = $cekReferral->id;
                    $dataref['id_ticket'] = $models->id;
                    $dataref['reward'] = $cekReferral->reward_referral;
                    $dataref['reff_pengunjung'] = $data['jumlah_pengunjung'];
                    $dataref['total_reward'] = ($cekReferral->reward_referral * $data['jumlah_pengunjung']);
                    $dataref['kode_reff'] = $cekReferral->kode_referral;
                    $dataref['tipe'] = 1;
                    $modelsreferral = TransaksiReferral::create($dataref);
                }else {
                    Alert::error('Opsss', 'Kode Referral tidak ditemukan');
                    $responseref['title'] = $this->title;
                    $responseref['route'] = $this->route;
                    $responseref['data'] = getFieldTable($this->table);
                    $responseref['title'] = $this->title . " | Tambah Data";
                    $responseref['route'] = $this->route;
                    
                    $responseref['listDestinasi'] = Destinasi::all();
                    $responseref['listDestinasiDetail'] = DestinasiDetail::all();

                    $responseref['data']->pengunjung = 0;
                    $responseref['data']->no_telepon = "";

                    $responseref['countries'] = Destinasi::get(["nama", "id"]);
                    return view($this->view . '.form', $responseref);
                }
            }else {
                $models = TransaksiCetak::create($data);
            }

            // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $request->post('id'),
                'aktivitas' => 'Menambah tiket dengan kode '. $data['kode']
            ];
            saveUserLog($paramUserLog);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );
        }
        
        // query
        $model = TransaksiCetak::select('t_cetak.*', 'm_destination.nama as nama_destinasi', 'm_destination.harga as harga_ticket', 'm_destination.alamat', 'm_destination_detail.nama as nama_kendaraan', 'm_destination_detail.harga as biaya_kendaraan')
            ->join('m_destination', 'm_destination.id', '=', 't_cetak.m_destination_id')
            ->join('m_destination_detail', 'm_destination_detail.id', '=', 't_cetak.m_destination_detail_id')
            ->where('t_cetak.id', '=', $models->id);
        
        $response['data'] = $model->first();
        $response['data']->harga_pengunjung = ($response['data']->jumlah_pengunjung * $response['data']->harga_ticket);
        // ej($response);
        return view($this->view . '.struk', $response);
        // return redirect()->route($this->route . '.cetak')->with($models);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $data['is_deleted'] = 1;
            TransaksiCetak::where('id', $id)->update($data);

            // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $id,
                'aktivitas' => 'Menghapus data tiket'
            ];
            saveUserLog($paramUserLog);
        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );

        return redirect()->route($this->route . '.index')->with($notification);
    }
    public function restore($id)
    {
        if (!empty($id)) {
            $data['is_deleted'] = 0;
            TransaksiCetak::where('id', $id)->update($data);
        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );

        return redirect()->route($this->route . '.index')->with($notification);
    }
    public function cetak(Request $request)
    {
        $response['data'] = "Data Cetak";
        return view($this->view . '.struk', $response);
    }

    public function print(Request $request)
    {
        // query
       $model = TransaksiCetak::select('t_cetak.*', 'm_destination.nama as nama_destinasi', 'm_destination.harga as harga_ticket', 'm_destination.alamat', 'm_destination_detail.nama as nama_kendaraan', 'm_destination_detail.harga as biaya_kendaraan')
       ->join('m_destination', 'm_destination.id', '=', 't_cetak.m_destination_id')
       ->join('m_destination_detail', 'm_destination_detail.id', '=', 't_cetak.m_destination_detail_id')
       ->where('t_cetak.id', '=', $request->id);
   
        $response['data'] = $model->first();
        $response['data']->harga_pengunjung = $response['data']->jumlah_pengunjung * $response['data']->harga_ticket;
        
        return view($this->view . '.nota', $response);
    }
    public function gettiket(Request $request)
    {
        // query
        $model = TransaksiCetak::select('t_cetak.*', 'm_destination.nama as nama_destinasi', 'm_destination.harga as harga_ticket', 'm_destination.alamat', 'm_destination_detail.nama as nama_kendaraan', 'm_destination_detail.harga as biaya_kendaraan')
       ->join('m_destination', 'm_destination.id', '=', 't_cetak.m_destination_id')
       ->join('m_destination_detail', 'm_destination_detail.id', '=', 't_cetak.m_destination_detail_id')
       ->where('t_cetak.uniq_ticket', '=', $request->kodetiket)->first();
        // ej($model);
        if($model){
           $response['data'] = $model;
           $response['data']->ada_data = 1;
           $response['data']->kodetiket = $model->uniq_ticket;
           $response['data']->harga_pengunjung = $response['data']->jumlah_pengunjung * $response['data']->harga_ticket;
        }else {
            $myObj = new stdClass();
            $myObj->ada_data = 0;
            $myObj->nama_destinasi = null;
            $myObj->kode = null;
            $myObj->jumlah_pengunjung = null;
            $myObj->harga_pengunjung = null;
            $myObj->nama_kendaraan = null;
            $myObj->biaya_kendaraan = null;
            $myObj->total_harga = null;
            $myObj->kodetiket = null;

            // $myJSON = json_decode($myObj);

            $response['data'] = $myObj;
        }
        
        return view($this->view . '.scantiket', $response);
    }
    public function scan()
    {
        $myObj = new stdClass();
        $myObj->ada_data = 0;
        $myObj->nama_destinasi = null;
        $myObj->kode = null;
        $myObj->jumlah_pengunjung = null;
        $myObj->harga_pengunjung = null;
        $myObj->nama_kendaraan = null;
        $myObj->biaya_kendaraan = null;
        $myObj->total_harga = null;
        $myObj->kodetiket = null;

        // $myJSON = json_decode($myObj);

        $response['data'] = $myObj;
        // ej($response);
        return view($this->view . '.scantiket', $response);
    }
    
    public function confirm($id)
    {
        if (!empty($id)) {
            $data['status'] = 1;
            TransaksiCetak::where('id', $id)->update($data);
        }
        $notification = array(
            'message' => 'Data berhasil diperbarui',
            'alert-type' => 'info',
        );

        return redirect()->route($this->route . '.scan')->with($notification);
    }
    public function confirmlangsung($id)
    {
        if (!empty($id)) {
            $data['status'] = 1;
            TransaksiCetak::where('id', $id)->update($data);
        }
        $notification = array(
            'message' => 'Data berhasil diperbarui',
            'alert-type' => 'info',
        );

        
        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function noscan()
    {
        $myObj = new stdClass();
        $myObj->ada_data = 0;
        $myObj->nama_destinasi = null;
        $myObj->kode = null;
        $myObj->jumlah_pengunjung = null;
        $myObj->harga_pengunjung = null;
        $myObj->nama_kendaraan = null;
        $myObj->biaya_kendaraan = null;
        $myObj->total_harga = null;
        $myObj->kodetiket = null;

        // $myJSON = json_decode($myObj);

        $response['data'] = $myObj;
        // ej($response);
        return view($this->view . '.noscantiket', $response);
    }
    public function noscangettiket(Request $request)
    {
        $myObj = new stdClass();
        $myObj->ada_data = 0;
        $myObj->nama_destinasi = null;
        $myObj->kode = null;
        $myObj->jumlah_pengunjung = null;
        $myObj->harga_pengunjung = null;
        $myObj->nama_kendaraan = null;
        $myObj->biaya_kendaraan = null;
        $myObj->total_harga = null;
        $myObj->kodetiket = null;

        // $myJSON = json_decode($myObj);

        $response['data'] = $myObj;
        if (!empty($request->input('kode'))) {
            // query
            $model = TransaksiCetak::select('t_cetak.*', 'm_destination.nama as nama_destinasi', 'm_destination.harga as harga_ticket', 'm_destination.alamat', 'm_destination_detail.nama as nama_kendaraan', 'm_destination_detail.harga as biaya_kendaraan')
            ->join('m_destination', 'm_destination.id', '=', 't_cetak.m_destination_id')
            ->join('m_destination_detail', 'm_destination_detail.id', '=', 't_cetak.m_destination_detail_id')
            ->where('t_cetak.kode','=', $request->input('kode'))->first();
            
            if ($model !== null) {
                $response['data'] = $model;
                $response['data']->ada_data = 1;
                $response['data']->kodetiket = $model->uniq_ticket;
                $response['data']->harga_pengunjung = $response['data']->jumlah_pengunjung * $response['data']->harga_ticket;
                return view($this->view . '.noscantiket', $response);
            }else {
                Alert::error('Opsss', 'Kode Tiket tidak ditemukan');
                return view($this->view . '.noscantiket', $response);
            }
        
        }else {
            Alert::error('Opsss', 'Harap isi Kode Tiket');
            return view($this->view . '.noscantiket', $response);
        }
    }
    public function noscanconfirm($id)
    {
        if (!empty($id)) {
            $data['status'] = 1;
            TransaksiCetak::where('id', $id)->update($data);
        }
        Alert::success('Yes!', 'Kode Tiket berhasil dikonfirmasi');

        return redirect()->route($this->route . '.noscan');
    }
}
