<?php

namespace App\Http\Controllers;

use App\Models\SettingPersetujuan;
use App\Models\User;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast\Object_;

class SettingPersetujuanController extends Controller
{
    private $setting_persetujuan;

    public $view = 'master.setting_persetujuan';
    public $route = 'setting_persetujuan';
    protected $table = 'm_setting_persetujuan';
    public $title = 'Setting Persetujuan';

    public function __construct()
    {
        $this->setting_persetujuan = new SettingPersetujuan();
    }

    public function index(Request $request)
    {
        
        $response['data'] = $this->setting_persetujuan->getAll();
        if(!isset($response['data'])){
            $response['data'] = getFieldTable($this->table);
        }
        $response['listUser'] = User::all();
        $response['route'] = $this->route;
        $response['title'] = $this->title;
        
        return view($this->view . '.index', $response);
    }

    public function store(Request $request)
    {
      
        $data['m_user_approval_harian_id'] = $request->post('m_user_approval_harian_id');
        $data['m_user_approval_bulanan_id'] = $request->post('m_user_approval_bulanan_id');


        if (!empty($request->post('id'))) {
            $model = $this->setting_persetujuan->edit($data, $request->post('id'));
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );

        } else {
            $model = $this->setting_persetujuan->store($data);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );
        }

        return redirect()->route($this->route . '.index')->with($notification);
    }

}
