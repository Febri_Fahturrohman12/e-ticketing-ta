<?php

namespace App\Http\Controllers;

use App\Models\Biaya;
use App\Models\Customer;
use App\Models\TransaksiPemakaian;
use Illuminate\Http\Request;

class TransaksiPemakaianController extends Controller
{
    public $title = 'Pemakaian';
    public $route = 'transaksi-pemakaian';
    public $table = 't_pemakaian';
    public $view = 'transaksi.pemakaian';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;

    public function index(Request $request)
    {

        // query
        $model = TransaksiPemakaian::select('t_pemakaian.*', 'm_customer.nama as nama_customer', 'm_customer.nomer as nomer_pam', 'm_biaya.tanggal')
            ->join('m_customer', 'm_customer.id', '=', 't_pemakaian.customer_id')
            ->join('m_biaya', 'm_biaya.id', '=', 't_pemakaian.biaya_id');

        // pagination
        $this->pagination = ceil($model->count() / $this->limit);
        if ($this->pagination > 1) {
            if (!empty($request->input('page'))) {
                $this->page = $request->input('page');
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }

        // get index
        $model = $model->get();
        foreach ($model as $key => $value) {
            $value->tanggal_formatted = date('F Y', strtotime($value->tanggal));
        }

        $response['data'] = $model;
        $response['title'] = $this->title;

        $response['page'] = $this->page;
        $response['pagination'] = $this->pagination;
        $response['route'] = $this->route;

        return view($this->view . '.index', $response);
    }

    public function create(Request $request)
    {
        $response['data'] = getFieldTable($this->table);

        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;

        $response['listCustomer'] = Customer::all();
        $biaya = Biaya::all();
        foreach ($biaya as $key => $value) {
            $value->tanggal = date("F Y", strtotime($value->tanggal));
        }
        $response['listBiaya'] = $biaya;
        $response['data']->biaya_air = $biaya[0]->biaya_air;
        $response['data']->biaya_alat = $biaya[0]->biaya_alat;
        $response['data']->pemakaian = 0;

        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        $model = TransaksiPemakaian::where("id", $id)->first();
        $response['data'] = $model;
        $response['title'] = $this->title . " | Edit Data";
        $response['route'] = $this->route;

        $response['listCustomer'] = Customer::all();
        $biaya = Biaya::all();
        foreach ($biaya as $key => $value) {
            $value->tanggal = date("F Y", strtotime($value->tanggal));
        }
        $response['listBiaya'] = $biaya;

        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'customer_id' => ['required'],
            'biaya_id' => ['required'],
            'biaya_air' => ['required'],
            'pemakaian' => ['required'],
            'total' => ['required'],
            'biaya_alat' => ['required'],
        ]);

        $data['customer_id'] = $request->post('customer_id');
        $data['biaya_id'] = $request->post('biaya_id');
        $data['biaya_air'] = $request->post('biaya_air');
        $data['pemakaian'] = $request->post('pemakaian');
        $data['total'] = $request->post('total');
        $data['biaya_alat'] = $request->post('biaya_alat');
        $data['status_pembayaran'] = 'pending';

        if (!empty($request->post('id'))) {
            $cek = TransaksiPemakaian::where('id', '!=', $request->post('id'))->where('customer_id', $data['customer_id'])->where('biaya_id', $data['biaya_id'])->first();
            if(!empty($cek)) {
                $notification = array(
                    'message' => 'Data sudah ada',
                    'alert-type' => 'error',
                );
                return redirect()->route($this->route . '.edit', $request->post('id'))->with($notification);
            }
            TransaksiPemakaian::where('id', $request->post('id'))->update($data);
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );
        } else {
            $cek = TransaksiPemakaian::where('customer_id', $data['customer_id'])->where('biaya_id', $data['biaya_id'])->first();
            if(!empty($cek)) {
                $notification = array(
                    'message' => 'Data sudah ada',
                    'alert-type' => 'error',
                );
                return redirect()->route($this->route . '.create')->with($notification);
            }
            TransaksiPemakaian::create($data);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );
        }

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            TransaksiPemakaian::where('id', $id)->delete();
        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );

        return redirect()->route($this->route . '.index')->with($notification);
    }
}
