<?php

namespace App\Http\Controllers;

use App\Models\Biaya;
use Illuminate\Http\Request;

class BiayaController extends Controller
{
    public $title = 'Biaya';
    public $route = 'biaya';
    public $table = 'm_biaya';
    public $view = 'master.biaya';

    public $page = 1;
    public $offset = 0;
    public $limit = 10;
    public $pagination = 0;

    public function index(Request $request)
    {

        // query
        $model = Biaya::select('*');

        // pagination
        $this->pagination = ceil($model->count() / $this->limit);
        if ($this->pagination > 1) {
            if (!empty($request->input('page'))) {
                $this->page = $request->input('page');
            }
            $this->offset = ($this->page - 1) * $this->limit;
            $model->offset($this->offset)->limit($this->limit);
        }

        // get index
        $model = $model->get();
        foreach ($model as $key => $value) {
            $value->tanggal = date("Y-m", strtotime($value->tanggal));
            $value->tanggal_formatted = date("F Y", strtotime($value->tanggal));
        }

        $response['data'] = $model;
        $response['title'] = $this->title;

        $response['page'] = $this->page;
        $response['pagination'] = $this->pagination;
        $response['route'] = $this->route;

        return view($this->view . '.index', $response);
    }

    public function create()
    {
        $response['title'] = $this->title . " | Tambah Data";
        $response['route'] = $this->route;

        $response['data'] = getFieldTable($this->table);
        $response['data']->tanggal = date("Y-m");

        return view($this->view . '.form', $response);
    }

    public function edit(Biaya $biaya)
    {
        $response['data'] = compact('biaya')['biaya'];
        $response['data']->tanggal = date("Y-m", strtotime($response['data']->tanggal));
        $response['title'] = $this->title . " | Edit Data";
        $response['route'] = $this->route;
        // print_die($response);

        return view($this->view . '.form', $response);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tanggal' => ['required'],
            'biaya_air' => ['required'],
            'biaya_alat' => ['required'],
        ]);

        $data['tanggal'] = date("Y-m-01", strtotime($request->post('tanggal') . "-01"));
        $data['biaya_air'] = $request->post('biaya_air');
        $data['biaya_alat'] = $request->post('biaya_alat');

        // print_die($data);
        if (!empty($request->post('id'))) {
            Biaya::where('id', $request->post('id'))->update($data);
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success'
            );
        } else {
            Biaya::create($data);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success'
            );
        }

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            Biaya::where('id', $id)->delete();
        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info'
        );
        return redirect()->route($this->route . '.index')->with($notification);
    }
}
