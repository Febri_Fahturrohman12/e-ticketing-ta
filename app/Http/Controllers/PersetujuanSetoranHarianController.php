<?php

namespace App\Http\Controllers;

use App\Models\PersetujuanSetoranHarian;
use App\Models\SettingPersetujuan;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Destinasi;

class PersetujuanSetoranHarianController extends Controller
{
    private $setoranHarian;
    private $setting_persetujuan;


    public $view = 'transaksi.persetujuan_setoran_harian';
    public $route = 'persetujuan_setoran_harian';
    protected $table = 't_approval_setoran_harian';

    public function __construct()
    {
        $this->setoranHarian = new PersetujuanSetoranHarian();
        $this->setting_persetujuan = new SettingPersetujuan();

    }

    /**
     * Mengambil data destinasi dilengkapi dengan pagination
     *
     * @author Wahyu Agung <wahyuagung26@email.com>
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $response = $this->setoranHarian->getAll($filter, $request->page ?? 1, $request->sort ?? '');
        return view($this->view . '.index', $response);
    }

    public function create()
    {
        $response = $this->setoranHarian->tambah();
        $getPersetujuan = $this->setting_persetujuan->getAll();
        $m_user_approval_harian_id = isset($getPersetujuan->m_user_approval_harian_id) ? $getPersetujuan->m_user_approval_harian_id : null;
        $response['approval'] = $this->getDetailApproval($m_user_approval_harian_id);
        $response['listDestinasi'] = Destinasi::all();

        return view($this->view . '.form', $response);
    }

    public function edit($id)
    {
        $response = $this->setoranHarian->getData($id);
        $response['approval'] = $this->getDetailApproval($response['data']->m_user_approval_id);
        $response['listDestinasi'] = Destinasi::all();

        return view($this->view . '.form', $response);
    }
    public function approval($id)
    {
        $response = $this->setoranHarian->getData($id);
        $response['approval'] = $this->getDetailApproval($response['data']->m_user_approval_id);
        $response['listDestinasi'] = Destinasi::all();

        return view($this->view . '.approval', $response);
    }
    public function approvePengajuan(Request $request)
    {   
        $data['status'] = 'disetujui';
        $data['m_user_approval_nama'] = session()->get('user')->name;
        $data['approved_at'] = date('Y-m-d h:i:s');

        $model = $this->setoranHarian->edit($data, $request->post('id'));
        // USER LOG
        $paramUserLog = [
            'reff_type' => $this->table,
            'reff_id' => $request->post('id'),
            'aktivitas' => 'Mengapprove data Setoran Harian dengan kode '. $request->post('kode')
        ];
        saveUserLog($paramUserLog);

        $notification = array(
            'message' => 'Data berhasil diupdate',
            'alert-type' => 'success',
        );
        return redirect()->route($this->route . '.index')->with($notification);

    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tanggal_mulai' => ['required'],
            'tanggal_selesai' => ['required'],
            'total_pendapatan' => ['required'],
        ]);
        $data['tanggal_mulai'] = $request->post('tanggal_mulai');
        $data['tanggal_selesai'] = $request->post('tanggal_selesai');
        $data['m_destination_id'] = $request->post('m_destination_id');
        $data['total_pendapatan'] = $request->post('total_pendapatan');
        $data['keterangan'] = $request->post('keterangan');

        if($request->file('foto_url')){
            $path = 'upload/setoran_harian';
            $file = $request->file('foto_url');
            $filename = strtotime(date('YmdHis')).$file->getClientOriginalName();
            $file->move(public_path($path),$filename);
            $data['foto'] = $path.'/'.$filename;
        }

        if (!empty($request->post('id'))) {
            $model = $this->setoranHarian->edit($data, $request->post('id'));
            $notification = array(
                'message' => 'Data berhasil diupdate',
                'alert-type' => 'success',
            );

             // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $request->post('id'),
                'aktivitas' => 'Mengubah data Setoran Harian dengan kode '. $request->post('kode')
            ];
            saveUserLog($paramUserLog);

        } else {
            $data['m_user_approval_id'] = $request->post('m_user_approval_id');
            if(isset($data['m_user_approval_id'])){
                $data['status'] = 'menunggu';
            }else{
                $data['status'] = 'disetujui';
                $data['m_user_approval_nama'] = session()->get('user')->name;
            }
            $data['m_user_pengajuan_id'] =  session()->get('user')->id;

            $model = $this->setoranHarian->store($data);
            $notification = array(
                'message' => 'Data berhasil disimpan',
                'alert-type' => 'success',
            );

             // USER LOG
            $paramUserLog = [
                'reff_type' => $this->table,
                'reff_id' => $model->id,
                'aktivitas' => 'Menambah data Setoran Harian dengan kode '. $model->kode
            ];
            saveUserLog($paramUserLog);

        }

        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $this->setoranHarian->destroy($id);

        }
        
         // USER LOG
         $paramUserLog = [
            'reff_type' => $this->table,
            'reff_id' => $id,
            'aktivitas' => 'Menghapus data Setoran Harian'
        ];
        saveUserLog($paramUserLog);


        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );
        return redirect()->route($this->route . '.index')->with($notification);
    }

    public function getDetailApproval($userId){
        $response = (object) [];

        if(isset($userId)){
            $response->m_user_approval_id =  $userId;
            $getUser = User::where('id', $userId)->with('akses')->first();
            $response->m_user_approval_nama =  $getUser->name;
            $response->m_user_approval_foto =  isset($getUser->foto) ? 'upload/admin_image'.$getUser->foto : null;
            $response->m_user_approval_akses_nama =  isset($getUser->akses) ? $getUser->akses->nama : '';
        }else{
            $response->m_user_approval_id =  null;
            $response->m_user_approval_nama =  null;
            $response->m_user_approval_foto =  null;
            $response->m_user_approval_akses_nama =  null;
        }
        return $response;
    }
}
