<?php

namespace App\Http\Controllers;

use App\Models\Biaya;
use App\Models\Customer;
use App\Models\Destinasi;
use App\Models\TransaksiCetak;
use App\Models\TransaksiPemakaian;
use Illuminate\Http\Request;
use PDF;

class LaporanPemakaianController extends Controller
{
    public $title = 'Laporan Ticketing';
    public $route = 'laporan-pemakaian';
    public $view = 'laporan.pemakaian';

    public function index(Request $request)
    {
        $filter = $request->all();

        $filter['tanggal_mulai'] = isset($filter['tanggal_mulai']) ? $filter['tanggal_mulai'] : date('Y') . '-01-01';
        $filter['tanggal_selesai'] =  isset($filter['tanggal_selesai']) ? $filter['tanggal_selesai'] : date('Y') . '-12-31';


        $monthStart = date('m', strtotime($filter['tanggal_mulai']));
        $monthEnd = date('m', strtotime($filter['tanggal_selesai']));
        $yearStart = date('Y', strtotime($filter['tanggal_mulai']));
        $yearEnd = date('Y', strtotime($filter['tanggal_selesai']));

        if ($yearEnd != $yearStart) {
            $filter['tanggal_mulai'] = date('Y') . '-01-01';
            $filter['tanggal_selesai'] =  date('Y') . '-12-31';
            $notification = array(
                'message' => 'Mohon pilih range tanggal ditahun yang sama',
                'alert-type' => 'error',
            );
            return redirect()->route($this->route)->with($notification);
        }

        // query
        $model = TransaksiCetak::select('t_cetak.*', 'm_destination.nama as nama_destinasi', 'm_destination.harga as harga_ticket', 'm_destination_detail.nama as nama_kendaraan', 'm_destination_detail.harga as biaya_kendaraan')
            ->join('m_destination', 'm_destination.id', '=', 't_cetak.m_destination_id')
            ->join('m_destination_detail', 'm_destination_detail.id', '=', 't_cetak.m_destination_detail_id')
            ->where('t_cetak.is_deleted', '=', 0)
            ->where('t_cetak.created_at', '>=',  $filter['tanggal_mulai'])
            ->where('t_cetak.created_at', '<=',  $filter['tanggal_selesai'])
            ->orderBy('t_cetak.created_at', 'asc');

        if (!empty($request->input('destinasi_id'))) {
            $model->where('t_cetak.m_destination_id', $request->input('destinasi_id'));
            $filter['destinasi_id'] = $request->input('destinasi_id');
        }else {
            $filter['destinasi_id'] = "";
        }

        $response['filter'] = (object) $filter;

        // get index
        $model = $model->get();
        $totalpengunjung = 0;
        $totalharga = 0;
        foreach ($model as $key => $value) {
            $value->tanggal_formatted = date('d F Y', strtotime($value->created_at));
            if($value->jumlah_pengunjung == null || $value->jumlah_pengunjung == ""){
                $value->jumlah_pengunjung = 0;
            }
            if($value->total_harga == null || $value->total_harga == ""){
                $value->total_harga = 0;
            }
            $totalpengunjung += $value->jumlah_pengunjung;
            $totalharga += $value->total_harga;


        }
        
        // RESPONSE
        $response['filter'] = (object)[
            'tanggal_mulai' => $filter['tanggal_mulai'],
            'tanggal_selesai' => $filter['tanggal_selesai'],
            'destinasi_id' => $filter['destinasi_id'],
        ];
        $response['listDestinasi'] = Destinasi::all();
        $response['data'] = $model;
        $response['title'] = $this->title;
        $response['periode'] = date('d F Y', strtotime($filter['tanggal_mulai']))." - ".date("d F Y", strtotime($filter['tanggal_selesai']));
        $response['route'] = $this->route;
        $response['totalpengunjung'] = $totalpengunjung;
        $response['totalharga'] = $totalharga;
        
        if($request->has('download'))
	    {   
            // return view($this->view . '.laporan', $response);
	        $pdf = PDF::loadView($this->view . '.laporan', $response)->setOptions(['defaultFont' => 'sans-serif']);
	        return $pdf->download('Laporan Ticketing-'.date('d F Y').'.pdf');
	    }

        return view($this->view . '.index', $response);
    }
}
