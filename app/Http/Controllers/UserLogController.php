<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PersetujuanSetoranBulanan;
use App\Models\SettingPersetujuan;
use App\Models\User;
use App\Models\UserLog;

class UserLogController extends Controller
{
    private $userLog;


    public $view = 'laporan.user_log';
    public $route = 'laporan-user-log';

    public function __construct()
    {
        $this->userLog = new UserLog();

    }

    /**
     * Mengambil data destinasi dilengkapi dengan pagination
     *
     * @author Wahyu Agung <wahyuagung26@email.com>
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $response = $this->userLog->getAll($filter, $request->page ?? 1, $request->sort ?? '');
        return view($this->view . '.index', $response);
    }
    public function store($data)
    {
        try {
            $model = $this->userLog->store($data);
            return [
                'status' => true,
                'message' => 'Berhasil'
            ];
        } catch (\Throwable $th) {
            return [
                'status' => false,
                'message' => $th
            ];
            
        }
        
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $this->userLog->destroy($id);

        }
        $notification = array(
            'message' => 'Data berhasil dihapus',
            'alert-type' => 'info',
        );
        return redirect()->route($this->route . '.index')->with($notification);
    }
}
