<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $arrAkses = [
            'master_user',
            'master_member',
            'master_akses',
            'master_destinasi',
            'master_setting_persetujuan',
            'transaksi_ticketing',
            'transaksi_persetujuan_setoran_harian',
            'transaksi_persetujuan_setoran_bulanan',
            'laporan_ticketing',
            'laporan_user_log',
            'laporan_keuangan'
        ];
        $nama_menu = '';
        foreach ($arrAkses as $key => $value) {
            /* define a admin user role */
            Gate::define($value, function() use ($value) {
                return  $this->setAkses($value);
            });
        }
    }

    public function setAkses($akses){
        $arrAkses = session()->get('akses');
        if(!empty($arrAkses) && isset($arrAkses)){

            foreach ($arrAkses as $key => $value) {
                if($value == $akses){
                    return true;
                    break;
                }
            }
        }
        return false;
    }
}
