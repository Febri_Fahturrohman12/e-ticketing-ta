<?php

use App\Http\Controllers\UserLogController;
use App\Models\UserLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;

function print_die($data)
{
    echo json_encode($data);die;
}
function ej($data)
{
    echo json_encode($data);die;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}
function getFieldTable($tableName)
{
    $arrIgnore = [
        'created_at', 'created_by', 'updated_at', 'updated_by',
    ];
    $columns = Schema::getColumnListing($tableName);
    $data = [];
    foreach ($columns as $key => $value) {
        if (!in_array($value, $arrIgnore)) {
            $data[$value] = null;
        }
    }
    return (object) $data;
}

function generateKode($table){
    
    $cekKode = DB::table($table)
    ->selectRaw($table.'.*')
    ->orderBy('id','DESC')
    ->first();
    try {
        if ($cekKode) {
            $kode_terakhir = $cekKode->id;
        } else {
            $kode_terakhir = 0;
        }
        $tahun = date('y');
        $bulan = date('m');
        $tipe = 'PSH';
        $kode_item = (substr($kode_terakhir, -5) + 1);
        $kode = substr('00000'.$kode_item, strlen($kode_item));
        $kode = $tipe.$tahun.$bulan.$kode;

        return [
            'status' => true,
            'data' => $kode,
        ];
    } catch (Exception $e) {
        return [
            'status' => false,
            'error' => 'Gagal Generate Kode',
        ];
    }
}

function saveUserLog($params){

    $userLog = new UserLog();

    $data = [
        'ip_address' => getIp(),
        'users_id' => isset(session()->get('user')->id) ? session()->get('user')->id : null,
        'users_name' => isset(session()->get('user')->name) ? session()->get('user')->name : null,
        'reff_type' => isset($params['reff_type']) ? $params['reff_type'] : null,
        'reff_id' => isset($params['reff_id']) ? $params['reff_id'] : null,
        'aktivitas' => isset($params['aktivitas']) ? $params['aktivitas'] : null,
    ];
    // dd($data);
    $userLog->store($data);
}
function getIp(){
    return( \Request::getClientIp(true));
}
function getMonthName($idMonth){
    $arrMonth = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    ];
    return $arrMonth[$idMonth];
}