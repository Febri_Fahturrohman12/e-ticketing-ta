<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Activity\BeneficiariesActivityModel;
use App\Models\Activity\KegiatanModel;
use Maatwebsite\Excel\Concerns\WithTitle;


class LaporanKeuanganExport implements FromView, WithTitle, ShouldAutoSize, WithStyles
{
    private $reports;
    private $files;

    public function __construct(array $data, $files)
    {
        $this->reports = $data;
        $this->files = $files;
    }

    public function title(): string
    {
        return 'Data Laporan Keungan';
    }

    public function view(): View
    {
        return view($this->files, $this->reports);

    }
    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:L1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:L1')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A1:A8')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:P3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A5:P5')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

    }

}
